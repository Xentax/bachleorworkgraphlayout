{
    Nodes: [A, {SUB_2: [B, F]}, {SUB_1: [C, D]}, E, G, H, I, X, Y, Z, C, D, F],
    Edges: [A->B, SUB_2.B->SUB_1.C, SUB_1.C->SUB_1.D, SUB_1.D->E, SUB_2.F->G, A->SUB_2.F, G->H, B->I, I->E, X->Y, C->D, D->F],
    Left: [A],
    Right: [E, H, Z],
    Top: [F]
}