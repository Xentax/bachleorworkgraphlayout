Bachleor work Auto Node Placement --- Aplication DAGio
======================================================
Author: Jan Juda

Licence
-------
Copyright (c) 2019, Jan Juda
All rights reserved.
BSD Licence.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Jan Juda ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Jan Juda BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

How to run DAGio
----------------
Run requirements:
- Oracle Java 1.8

Depending on your platform run scripts for Windows (.bat) or for Linux (.sh).

To start with GUI:
- In the ./binary directory run script "Launch_DAGio_with_GUI.bat" or "Launch_DAGio_with_GUI.sh"
- or alternatively in the ./binary directory run command: "java -jar DAGio.jar --gui 2>log.txt"
  You can strip the "2>log.txt" which redirects error standard output to log file.
  
To start with CLI:
- In the ./binary directory from console run "DAGio.bat <ARGUMENTS>" or "DAGio.sh <ARGUMENTS>", you can directly supply arguments to it like this:
  DAGio.bat --help
- or alternatively in the ./binary directory run command "java -jar DAGio.jar <ARGUMENTS>", example:
  java -jar DAGio.jar --help
  
Usage examples
--------------
All of these examples should be executed in the ./binary directory.

java -jar DAGio.jar --help  = displays help
java -jar DAGio.jar --gui   = starts GUI
java -jar DAGio.jar -i ../inputs/input.txt -o ../output --no-show -x 600 -y 600 -a ISOM -I 1000
    = starts layout of 1000 iterations with input file "input.txt" in directory ./inputs with graph space 600x600px and saves output to folder ./output without showing grafical results
java -jar DAGio.jar -i ../inputs/input-MK64.txt -o ../output -t ../inputs/diagram-MK64.dsn -p MK64_FRLLSLON --force-deterministic-behaviour -x 3000 -y 1500 -N 10 -a FR -I 10000 --lock-other-nodes --limit-layout-space --override-pin
    = layouts clock source graph of micro-controller MK64 with best configuration, performs 10000 iterations, shows 10 best results and saves them into ./output directory

How to build from sources
-------------------------
Build requirements:
- Oracle Java JDK 1.8
- Maven (version 3 or higher)

Steps:
1. Install requirement software, you might need to set your environmental variable JAVA_HOME to the location of JDK.
2. Open command line in root directory of this project.
3. Execute command "mvn package". Thic command will download all required dependencies and create directory ./target with compiled "DAGio-1.0.jar" jar file.
4. To run the application, move the "DAGio-1.0.jar" file into the directory ./binary, where "DAGio_lib" folder is located.
   The generated jar must be located in the same directory as folder "DAGio_lib", so it can access all of it's dependencies.

Platform
--------
Application was developed and tested on Windows 10, but since it is in Java, it should work on other platforms as well.
