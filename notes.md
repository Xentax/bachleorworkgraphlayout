Bakalářka

Java grafy
https://issues.apache.org/jira/browse/SANDBOX-458
https://stackoverflow.com/questions/51574/good-java-graph-algorithm-library

http://jung.sourceforge.net/doc/index.html
http://www.grotto-networking.com/JUNG/JUNG2-Tutorial.pdf

Knihovny:
JUNG: http://jung.sourceforge.net/doc/index.html
    https://mvnrepository.com/artifact/net.sf.jung
	DAG...
Picoly: https://picocli.info/
GSON: http://tutorials.jenkov.com/java-json/gson-jsonreader.html


Algoritmy:
DAG: https://www.youtube.com/watch?v=N0rWr8c1CeY
Spring electric a FR: https://www.youtube.com/watch?v=HQBMfozLa4Y&t=167s
Kamada-Kawai - Tomihisa Kamada and Satoru Kawai
https://pdfs.semanticscholar.org/b8d3/bca50ccc573c5cb99f7d201e8acce6618f04.pdf
Fruchterman-Reingold force-directed algorithm http://citeseer.ist.psu.edu/viewdoc/download;jsessionid=19A8857540E8C9C26397650BBACD5311?doi=10.1.1.13.8444&rep=rep1&type=pdf
https://en.wikipedia.org/wiki/Force-directed_graph_drawing
ISOM - Meyer's self-organizing graph methods
https://link.springer.com/content/pdf/10.1007/3-540-37623-2_19.pdf
DAG

ITT
==========
TODO doplňovat keywordy
TODO přeložit keywordy

TODO uvozovky v caption u obrázků?
TODO Vyrobit obrázky pro výstupy jednotlivých algoritmů

BP
==========
TODO dokončit Abstrakt
TODO dokončit EN abstrakt
TODO poděkování
TODO závěr pro celou práci

Vstup:
---
Nodes: [A, B, C, D, E, F, G, H, I, X, Y, Z]            
Edges: [A->B, B->C, C->D, D->E, F->G, A->F, G->H, B->I, I->E, X->Y]
Left: [A]
Right: [E, H, Z]
Top: [F]
---

PIN TYPE:
0 = nic
1 = elipsa
2 = bílá šipka
    Direction:
    0 = nahoru
    1 = dolu
    2 = doleva
    3 = doprava
3 = kombinace 1 a 2
4 = nic
5 = nic
6 = křížek
7 = nic
8 = nic
9 = nic
10 = nic