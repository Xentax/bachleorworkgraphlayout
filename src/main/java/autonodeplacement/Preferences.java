/**
 * Copyright 2020 Jan Juda
 * Created: 15. 2. 2020
 */
package autonodeplacement;

import java.io.File;
import java.nio.file.Paths;
import java.util.Map;
import java.util.logging.Logger;

import autonodeplacement.algorithms.Algorithms;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParameterException;
import picocli.CommandLine.UnmatchedArgumentException;

/**
 * Singleton class for storing user preferences.
 * @author Jan Juda (xjudaj00)
 */
@Command(name = "java -jar DAGio.jar", description = "Tool for layouting DAGs (directed acyclic graphs).", sortOptions = true)
public class Preferences {

	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(Preferences.class.toString());

	/** Return code everything is OK */
	public static final int RET_CODE_OK = 0;
	/** Return code bad command line parameters. */
	public static final int RET_CODE_BAD_PARAMS = -1;

	/** Instance of singleton class */
	private static Preferences preferencesInstance = null;

	/** Name of main graph input file */
	@Option(names = { "-i", "--input" }, paramLabel = "FILE", defaultValue = "", description = "the name of main input file.")
	private String inputFileName;

	/** Name of the output path. */
	@Option(names = { "-o", "--output" }, paramLabel = "DIR", defaultValue = ".", description = "path to output directory. Default is current directory.")
	private String outputPath;

	/** Name of the output directory. */
	@Option(names = { "-p", "--output-prefix" }, paramLabel = "OUTPUT_PREFIX", defaultValue = "graph",
			description = "the name of output file. Default is '${DEFAULT-VALUE}'.")
	private String outputFilePrefix;

	/** Add time stamp to output file names? */
	@Option(names = { "--timestamp" }, description = "add timestamp to the generated output file names.")
	private boolean timeStamp = false;

	/** Name of TinyCad input file. Typically .dsn */
	@Option(names = { "-t", "--tinycad" }, paramLabel = "TINYCAD", description = "the name of TinyCad input file.")
	private String tinycadInputFileName = null;

	/** Whether user specified -h or --help for help */
	@Option(names = { "-h", "--help" }, usageHelp = true, description = "display a help message.")
	private boolean helpRequested = false;

	/** Whether user specified -g or --gui for GUI */
	@Option(names = { "--gui", "--GUI" }, description = "start application with GUI.")
	private boolean guiRequested = false;

	/** Selected layouting algorithm */
	@Option(names = { "-a", "--algorithm" }, paramLabel = "ALGORITHM", defaultValue = "FR",
			description = "algorithm for layouting the graph. One of ${COMPLETION-CANDIDATES}. Default is ${DEFAULT-VALUE}. See 'Supported layout algorithms'.")
	private Algorithms selectedAlgorithm;
	
	/** Selected score rule */
	@Option(names = { "-s",
			"--score" }, paramLabel = "SCORE_RULE=WEIGHT", split = ";",
			defaultValue = "LtR=1000;CEN=40;CEE=2",
			description = "selected score rules for evaluating the results with their weights. Format is SCORE_RULE=WEIGHT and there can be more active values separated by \";\"."
					+ " Default rule is LtR=1000;CEN=40;CEE=2 (Left to right * 1000, Crossings edge X nodes * 40, Crossings edge X edge * 2). See 'Supported score rules'.")
	private Map<Score, Integer> selectedScores;

	/** Width of the graph */
	@Option(names = { "-x", "--width" }, paramLabel = "WIDTH", defaultValue = "1280", description = "preferred width of graph. Default is ${DEFAULT-VALUE}.")
	private int sizeX;

	/** Height of the graph */
	@Option(names = { "-y", "--height" }, paramLabel = "HEIGHT", defaultValue = "720", description = "preferred height of graph. Default is ${DEFAULT-VALUE}.")
	private int sizeY;
	
	/** Iterations of layout algorithm executions */
	@Option(names = { "-I", "--iterations" }, paramLabel = "ITER_COUNT", defaultValue = "100",
			description = "number of iterative execution of the selected layout algorithm. Default is ${DEFAULT-VALUE}. Minimal value is 1.")
	private int iterations;
	
	/** Number of different initializations */
	@Option(names = { "-S", "--start-inits" }, paramLabel = "START_INITS", defaultValue = "1",
			description = "number of different start initializations. For each start will be performed ITER_COUNT iterations. Default is ${DEFAULT-VALUE}. Minimal value is 1.")
	private int startInits;
	
	/** Select N best */
	@Option(names = { "-N", "--number-of-results" }, paramLabel = "RESULTS", defaultValue = "1",
			description = "number of best results to save/show. Cannot be higher than ITER_COUNT * INITS. Default is ${DEFAULT-VALUE}. Minimal value is 1.")
	private int bestBufferSize;

	/** Whether user requires orthogonal edges */  // FIXME future version - Support orthogonal edges - this is possible improvement.
	@Option(names = { "--orthogonal" }, description = "requires edges to be orthogonal.", hidden = true)
	private boolean orthogonalOnly = false;

	/** Whether to limit layout space to part space only */
	@Option(names = { "--limit-layout-space" },
			description = "if specified, then layout algorithm won't layout in whole graph space, but only in subgraph part's space.")
	private boolean limitLayoutSpace = false;
	
	/** Whether to limit layout space to part space only */
	@Option(names = { "--lock-other-nodes" },
			description = "if specified, then nodes not belonging to the currently being-layouted part will be locked for the algorithm iteration.")
	private boolean lockOtherNodes = false;

	/** Whether arrows are allowed */
	@Option(names = { "--no-arrows" }, negatable = true, description = "enables/disables creating arrow pins at the end of edges. Enabled by default.")
	private boolean arrowsAllowed = true;
	
	/** Whether arrows are allowed */
	@Option(names = { "--override-pin" },
			description = "overrides pin types (according to edges direction) loaded from input TinyCAD. Only makes sense if \"arrows\" are allowed and TinyCAD input file is selected.")
	private boolean overridePinTypes = false;

	/** Whether size post processing is allowed */
	@Option(names = { "--no-sizepostprocess" }, negatable = true,
			description = "enables/disables post processing of node sizes and positions. Disabling can result in graph not fitting completely into graph space or nodes overlapping. Enabled by default.")
	private boolean sizePostprocessAllowed = true;

	/** Whether user wants to show the result */
	@Option(names = { "--no-show" }, negatable = true, description = "enables/disables showing of window with the final graph layout. Enabled by default.")
	private boolean showResult = true;
	
	/** Maximum iterations for overlay & overlap post processing */
	@Option(names = { "--max-iterations-postprocess" }, defaultValue = "100", paramLabel = "MAX_ITER",
			description = "sets maximum number of iterations for overlay & overlap post processing. Negative values are forbidden. Default and recommended value is ${DEFAULT-VALUE}.")
	private int maxIterationsPostprocess;

	/** Border around a node */
	@Option(names = { "--node-border" }, defaultValue = "10", paramLabel = "BORDER",
			description = "sets size of border around each node. This border is added to node's bounding box, which is considered during size post processing. Negative values are not allowed. Default is ${DEFAULT-VALUE}.")
	private int nodeBorder;
	
	/** Border around a subgraph */
	@Option(names = { "--subgraph-border" }, defaultValue = "-2147483648", paramLabel = "BORDER",
			description = "sets size of border around each subgraph. This border is added to subgraph's bounding box, which is considered during size post processing. Negative values are not allowed. Default is value for --node-border.")
	private int subgraphBorder;

	/** Initialize algorithm randomly? */
	@Option(names = { "--no-random" }, negatable = true,
			description = "enables/disables random initialization of layout algorithms. Enabled by default. Even if diabled, results for same graphs using same algorithms can still differ, because algorithms use randomized values. If you need deterministic output, use '--force-deterministic-behaviour'.")
	private boolean random = true;
	
	/** Parse positional nodes? */
	@Option(names = { "--no-nodes-preferred-position" }, negatable = true,
			description = "enables/disables processing of nodes with preferred position. If disabled 'Top', 'Left', 'Right' and 'Bottom' lists are ignored. Enabled by default.")
	private boolean parsePositional = true;
	
	/** Generate TinyCad output? */
	@Option(names = { "--no-generate-output" }, negatable = true, description = "enables/disables generating TinyCad output. Enabled by default.")
	private boolean generateOutput = true;

	/** Whether user forces algorithms to behave deterministically */
	@Option(names = {
			"--force-deterministic-behaviour" },
			description = "force algorithms to have deterministic behaviour, causing their output to be always the same for same input. Automatically disables random initialization.")
	private boolean forceDeterministicBehaviour = false;

	/**
	 * Private constructor
	 */
	private Preferences() {
		// Nothing to be done here
	}

	/**
	 * @return instance of singleton class Preferences
	 */
	public static Preferences getInstance() {
		if (preferencesInstance == null) {
			preferencesInstance = new Preferences();
		}
		return preferencesInstance;
	}

	/**
	 * This method will parse arguments of the application and retrieve user preferences from them
	 * @param args to parse
	 */
	public void parseArgs(String[] args) {
		CommandLine cmdLine = new CommandLine(this);
		try {
			cmdLine.parseArgs(args);
			if (cmdLine.isUsageHelpRequested()) {
				cmdLine.usage(System.out);
				StringBuilder additionalHelp = new StringBuilder();
				additionalHelp.append("\n  Supported layout algorithms:");
				for (Algorithms algorithm : Algorithms.values()) {
					additionalHelp.append("\n    ");
					additionalHelp.append(algorithm.toString());
					additionalHelp.append(" = ");
					additionalHelp.append(algorithm.getDescription());
				}
				additionalHelp.append("\n\n  Supported score rules:");
				for (Score score : Score.values()) {
					additionalHelp.append("\n    ");
					additionalHelp.append(score.toString());
					additionalHelp.append(" = ");
					additionalHelp.append(score.getDescription());
				}
				additionalHelp.append("\n\n");
				additionalHelp.append("Example usage:\n");
				additionalHelp.append("java -jar DAGio.jar --help  = displays help\n");
				additionalHelp.append("java -jar DAGio.jar --gui   = starts GUI\n");
				additionalHelp.append("java -jar DAGio.jar -i input.txt -o ./output --no-show -x 600 -y 600 -a ISOM -I 1000\n");
				additionalHelp.append("    = starts layout of 1000 iterations with input file \"input.txt\" with graph space 600x600px and saves output to folder ./output without showing grafical results\n");
				additionalHelp.append("java -jar DAGio.jar -i input.txt -o ./output -a BEST -I 100 -N 10 --lock-other-nodes --limit-layout-space\n");
				additionalHelp.append("    = layouts graph in file \"input.txt\", performs 100 iterations with each algorithm, lock-other-nodes and limit-layout-space function will be enabled,\n"
									+ "      10 best results will be shown and saved into ./output folder\n");
				
				System.out.print(additionalHelp.toString());
				System.exit(RET_CODE_OK);
			}
		} catch (UnmatchedArgumentException e) {
			LOGGER.warning(e.getLocalizedMessage() + " This arguments are ignored.");
		} catch (ParameterException e) {
			LOGGER.severe(e.getLocalizedMessage() + "\nCheck for help with -h option.");
			System.exit(RET_CODE_BAD_PARAMS);
		}
		if (maxIterationsPostprocess < 0) {
			LOGGER.severe("Maximum number of post processing iterations cannot be negative!\nCheck for help with -h option.");
			System.exit(RET_CODE_BAD_PARAMS);
		}
		if (forceDeterministicBehaviour) {
			random = false;
		}
		if (iterations < 1) {
			iterations = 1;
		}
		if (startInits < 1) {
			startInits = 1;
		}
		if (bestBufferSize < 1) {
			bestBufferSize = 1;
		}
		if (nodeBorder < 0) {
			nodeBorder = 0;
		}
		if (subgraphBorder == Integer.MIN_VALUE) {
			subgraphBorder = nodeBorder;
		}
		if (subgraphBorder < 0) {
			subgraphBorder = 0;
		}
		if (sizeX < 0) {
			sizeX = 0;
		}
		if (sizeY < 0) {
			sizeY = 0;
		}
		if (maxIterationsPostprocess < 1) {
			maxIterationsPostprocess = 1;
		}
		if (generateOutput) {
			String path = outputPath;
			if (!(path.endsWith("/") || path.endsWith(":"))) {
				path += "/";
			}
			path += outputFilePrefix;
			try {
				// Testing for correctness of a path is a bit catchy, but this should do in most cases.
				Paths.get(path).toAbsolutePath();
				File testfile = new File(path);
				testfile.getCanonicalPath();
			} catch (Exception ex) {
				LOGGER.severe("Incorrect output path! Details: " + ex.getLocalizedMessage());
				System.exit(RET_CODE_BAD_PARAMS);
			}
		}
	}

	/**
	 * @return name of main input file specified by user
	 */
	public String getInputFileName() {
		return inputFileName;
	}

	/**
	 * @return name of the output directory
	 */
	public String getOutputPath() {
		return outputPath;
	}

	/**
	 * @return prefix for the output files
	 */
	public String getOutputFilePrefix() {
		return outputFilePrefix;
	}

	/**
	 * @return name of TinyCad input file
	 */
	public String getTinycadInputFileName() {
		return tinycadInputFileName;
	}

	/**
	 * @return layout algorithm selected by user
	 */
	public Algorithms getSelectedAlgorithm() {
		return selectedAlgorithm;
	}

	/**
	 * @return width of the graph
	 */
	public int getWidth() {
		return sizeX;
	}

	/**
	 * @return height of the graph
	 */
	public int getHeight() {
		return sizeY;
	}

	/**
	 * @return true if all edges must be orthogonal
	 */
	public boolean getOrthogonalOnly() {
		return orthogonalOnly;
	}

	/**
	 * @return true if arrows are allowed by user
	 */
	public boolean getArrowsAllowed() {
		return arrowsAllowed;
	}

	/**
	 * @return true if size post processing is allowed by user
	 */
	public boolean getSizePostprocessAllowed() {
		return sizePostprocessAllowed;
	}

	/**
	 * @return true if the result should be shown
	 */
	public boolean showResult() {
		return showResult;
	}

	/**
	 * @return maximum number of iterations for iterative post processing
	 */
	public int getMaxIterationsPostprocess() {
		return maxIterationsPostprocess;
	}

	/**
	 * @return border for node bounding box
	 */
	public int getNodeBorder() {
		return nodeBorder;
	}

	/**
	 * @return border for subgraph bounding box
	 */
	public int getSubgraphBorder() {
		return subgraphBorder;
	}

	/**
	 * @return true if algorithms should be initialized randomly
	 */
	public boolean getRandomInit() {
		return random;
	}

	/**
	 * @return true if user wants deterministic behaviour of algorithms
	 */
	public boolean getForceDeterministicBehaviour() {
		return forceDeterministicBehaviour;
	}

	/**
	 * @return true if positional nodes should be parsed
	 */
	public boolean getParsePositional() {
		return parsePositional;
	}

	/**
	 * @return true if TinyCad output should be generated
	 */
	public boolean getGenerateOutput() {
		return generateOutput;
	}

	/**
	 * @return true if GUI is requested by the user
	 */
	public boolean isGuiRequested() {
		return guiRequested;
	}

	/**
	 * @return number of algorithm iterations
	 */
	public int getIterations() {
		return iterations;
	}

	/**
	 * @return from how many initializations should we start with?
	 */
	public int getStartInitializations() {
		return startInits;
	}

	/**
	 * @return size of best buffer
	 */
	public int getBestBufferSize() {
		return bestBufferSize;
	}

	/**
	 * @return true if layout algorithm is to be limited to subgraph part space only
	 */
	public boolean getLimitLayoutSpace() {
		return limitLayoutSpace;
	}

	/**
	 * @return true if other nodes (not belonging to the part) in layouting subgraph part should be locked
	 */
	public boolean getLockOtherNodes() {
		return lockOtherNodes;
	}

	/**
	 * @return the selected score rules and their weights
	 */
	public Map<Score, Integer> getSelectedScores() {
		return selectedScores;
	}

	/**
	 * @return true, if time stamp should be added to output filenames
	 */
	public boolean timeStamp() {
		return timeStamp;
	}

	/**
	 * @return true, if loaded pin types should be overriden
	 */
	public boolean getOverridePinTypes() {
		return overridePinTypes;
	}

	/**
	 * Sets parsing of positional nodes
	 * @param parsePositional
	 */
	public void setParsePositional(boolean parsePositional) {
		this.parsePositional = parsePositional;
	}

	/**
	 * Sets whether arrow pins are allowed
	 * @param arrowsAllowed
	 */
	public void setArrowsAllowed(boolean arrowsAllowed) {
		this.arrowsAllowed = arrowsAllowed;
	}

	/**
	 * Sets size of buffer for results
	 * @param bestBufferSize
	 */
	public void setBestBufferSize(int bestBufferSize) {
		this.bestBufferSize = bestBufferSize;
	}

	/**
	 * Sets whether algorithm will be force to behave deterministically
	 * @param forceDeterministicBehaviour
	 */
	public void setForceDeterministicBehaviour(boolean forceDeterministicBehaviour) {
		this.forceDeterministicBehaviour = forceDeterministicBehaviour;
	}

	/**
	 * Sets whether application should create output
	 * @param generateOutput
	 */
	public void setGenerateOutput(boolean generateOutput) {
		this.generateOutput = generateOutput;
	}

	/**
	 * Sets input file name
	 * @param inputFileName
	 */
	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}

	/**
	 * Sets number of iterations
	 * @param iterations
	 */
	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	/**
	 * Sets whether layout space should be limited
	 * @param limitLayoutSpace
	 */
	public void setLimitLayoutSpace(boolean limitLayoutSpace) {
		this.limitLayoutSpace = limitLayoutSpace;
	}

	/**
	 * Sets whether other nodes should be locked during layout
	 * @param lockOtherNodes
	 */
	public void setLockOtherNodes(boolean lockOtherNodes) {
		this.lockOtherNodes = lockOtherNodes;
	}

	/**
	 * Sets maximum number of iterations of size post processing
	 * @param maxIterationsPostprocess
	 */
	public void setMaxIterationsPostprocess(int maxIterationsPostprocess) {
		this.maxIterationsPostprocess = maxIterationsPostprocess;
	}

	/**
	 * Sets width of node border
	 * @param nodeBorder
	 */
	public void setNodeBorder(int nodeBorder) {
		this.nodeBorder = nodeBorder;
	}

	/**
	 * Sets number of start initializations
	 * @param numberOfStartInits
	 */
	public void setStartInits(int numberOfStartInits) {
		this.startInits = numberOfStartInits;
	}

	/**
	 * Sets prefix of output file
	 * @param outputFilePrefix
	 */
	public void setOutputFilePrefix(String outputFilePrefix) {
		this.outputFilePrefix = outputFilePrefix;
	}

	/**
	 * Sets output path
	 * @param outputPath
	 */
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	/**
	 * Sets whether initialization should be random
	 * @param random
	 */
	public void setRandom(boolean random) {
		this.random = random;
	}

	/**
	 * Sets selected algorithm
	 * @param selectedAlgorithm
	 */
	public void setSelectedAlgorithm(Algorithms selectedAlgorithm) {
		this.selectedAlgorithm = selectedAlgorithm;
	}

	/**
	 * Sets whether results should be shown
	 * @param showResult
	 */
	public void setShowResult(boolean showResult) {
		this.showResult = showResult;
	}

	/**
	 * Sets whether size post processing is allowed
	 * @param sizePostprocessAllowed
	 */
	public void setSizePostprocessAllowed(boolean sizePostprocessAllowed) {
		this.sizePostprocessAllowed = sizePostprocessAllowed;
	}

	/**
	 * Sets width of graph space
	 * @param sizeX
	 */
	public void setSizeX(int sizeX) {
		this.sizeX = sizeX;
	}

	/**
	 * Sets height of graph space
	 * @param sizeY
	 */
	public void setSizeY(int sizeY) {
		this.sizeY = sizeY;
	}

	/**
	 * Sets width of subgraph border
	 * @param subgraphBorder
	 */
	public void setSubgraphBorder(int subgraphBorder) {
		this.subgraphBorder = subgraphBorder;
	}

	/**
	 * Sets whether timestamp should be appended to generated file's name
	 * @param timeStamp
	 */
	public void setTimeStamp(boolean timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * Sets name of TinyCAD input file
	 * @param tinycadInputFileName
	 */
	public void setTinycadInputFileName(String tinycadInputFileName) {
		this.tinycadInputFileName = tinycadInputFileName;
	}

	/**
	 * Sets whether to override pin types
	 * @param overridePinTypes
	 */
	public void setOverridePinTypes(boolean overridePinTypes) {
		this.overridePinTypes = overridePinTypes;
	}
}
