/**
 * Copyright 2020 Jan Juda
 * Created: 15. 2. 2020
 */
package autonodeplacement;

import java.awt.geom.Point2D;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import autonodeplacement.representation.Edge;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import autonodeplacement.representation.Node.Location;
import autonodeplacement.representation.Pin;
import autonodeplacement.representation.Polygon;
import autonodeplacement.representation.Rectangle;
import autonodeplacement.representation.SubGraph;
import autonodeplacement.representation.SymbolDefinition;

/**
 * Class for parsing input files (both main and secondary input)
 * @author Jan Juda (xjudaj00)
 */
public class InputParser {

	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(InputParser.class.toString());

	/** Constant for Nodes element of input file */
	public static final String ELEMENT_NODES = "nodes";
	/** Constant for Edges element of input file */
	public static final String ELEMENT_EDGES = "edges";
	/** Constant for Left nodes element of input file */
	public static final String ELEMENT_LEFT = "left";
	/** Constant for Right nodes element of input file */
	public static final String ELEMENT_RIGHT = "right";
	/** Constant for Top nodes element of input file */
	public static final String ELEMENT_TOP = "top";
	/** Constant for Bottom nodes element of input file */
	public static final String ELEMENT_BOTTOM = "bottom";

	/** Arrow sign used in IDs of edges */
	public static final String EDGE_SIGN = "->";
	/** Dot. Used as separator in subgraph hierarchy */
	public static final String DOT = ".";

	/** Message for occurrence of unexpected token */
	public static final String MESSAGE_UNEXPECTED_TOKEN = "Unexpected token {0} while parsing main input.";
	/** Message for occurrence of invalid node */
	public static final String MESSAGE_INVALID_NODE = "Invalid node ID {0}. It is forbidden for node ID to contain either '->' or '.', those are reserved to edges. This node will be ignored.";
	/** Message */
	public static final String MESSAGE_TOO_MANY_PREFERRED_LOCS = "Node {0} is both in {1} and {2} lists, while only one preferred location per node is allowed. The latter will be used.";

	/**
	 * Intentionally empty constructor
	 */
	private InputParser() {
	}

	/**
	 * Parses diagram into DiagramRepresentation from main input file
	 * @param preferences parser gets input file name and graph size from preferences
	 * @return diagram in DiagramRepresentation, null if file not found or IOException occurred
	 */
	public static GraphRepresentation parseMainInput(Preferences preferences) {
		Map<String, Node> parsedNodes = new LinkedHashMap<>(); // key = path.ID , value = Node
		List<String> edgeIds = new ArrayList<>();
		List<String> leftIds = new ArrayList<>();
		List<String> rightIds = new ArrayList<>();
		List<String> topIds = new ArrayList<>();
		List<String> bottomIds = new ArrayList<>();
		Map<String, SubGraph> parsedSubGraphs = new LinkedHashMap<>(); // key = path.ID , value = SubGraph

		// Open file with jsonReader and parse it
		boolean parseSuccess = parseJson(preferences.getInputFileName(), parsedNodes, edgeIds, leftIds, rightIds, topIds, bottomIds, parsedSubGraphs);
		if (!parseSuccess) {
			return null;
		}
		Map<String, Edge> parsedEdges = parseEdges(parsedNodes, edgeIds);
		parseLocationalNodes(parsedNodes, leftIds, Location.LEFT);
		parseLocationalNodes(parsedNodes, rightIds, Location.RIGHT);
		parseLocationalNodes(parsedNodes, topIds, Location.TOP);
		parseLocationalNodes(parsedNodes, bottomIds, Location.BOTTOM);

		return new GraphRepresentation(parsedNodes, parsedEdges, parsedSubGraphs, preferences.getWidth(), preferences.getHeight());
	}

	/**
	 * Parses nodes with preferred location from node id list
	 * @param parsedNodes map of all parsed nodes
	 * @param nodeIds list of nodes IDs
	 * @param location preferred location for these IDs
	 */
	private static void parseLocationalNodes(Map<String, Node> parsedNodes, List<String> nodeIds, Location location) {
		for (String id : nodeIds) {
			Node node = getNodeWithId(id, parsedNodes);
			if (node == null) {
				LOGGER.warning(MessageFormat.format(
						"Node {0} with preferred location {1} is not uniquely identifiable (either missing in the Nodes list or if you are using subgraphs, try to use whole path instead of simple id - ex. subgraph.subsubgraph.node).",
						id, location));
			} else if (node.getPreferredLocation() != Location.NONE) {
				LOGGER.warning(MessageFormat.format(
						"Node {0} with preferred location {1} already has set a preferred location {2}. Node can have only 1 preferred location and duplicite entries are not allowed",
						node.toString(), location, node.getPreferredLocation()));
			} else {
				node.setPreferredLocation(location);
			}
		}
	}

	/**
	 * Takes list of edgeIds and parses them into Edge objects
	 * @param parsedNodes map of parsed nodes
	 * @param edgeIds IDs to parse
	 * @return parsed edges map, key = ID of edge, value = Edge itself
	 */
	private static Map<String, Edge> parseEdges(Map<String, Node> parsedNodes, List<String> edgeIds) {
		Map<String, Edge> parsedEdges = new LinkedHashMap<>(); // key = ID of edge, value = Edge itself
		if (parsedNodes.isEmpty()) {
			return parsedEdges;
		}
		for (String edgeId : edgeIds) {
			String[] edgeParts = edgeId.split(EDGE_SIGN);
			if (edgeParts.length == 2) {
				String startId = edgeParts[0].trim();
				String endId = edgeParts[1].trim();

				Node startNode = getNodeWithId(startId, parsedNodes);
				Node endNode = getNodeWithId(endId, parsedNodes);

				if (startNode == null || endNode == null) {
					LOGGER.warning(MessageFormat.format(
							"Unable to create edge {0}, because node {1} is not uniquely identifiable (either missing in the Nodes list or if you are using subgraphs, try to use whole path instead of simple id - ex. subgraph.subsubgraph.node)! This edge will be ignored.",
							edgeId, startNode == null ? startId : endId));
					continue;
				}

				if (startNode.equals(endNode)) {
					LOGGER.warning(MessageFormat.format("Edge {0} is single-node loop. Single-node loops are not allowed!", edgeId));
					continue;
				}

				// Unified ID without any white spaces with full path
				String newId = startNode.toString() + EDGE_SIGN + endNode.toString();

				if (parsedEdges.containsKey(newId)) {
					LOGGER.warning(MessageFormat.format("Duplicated edge with ID {0}!", edgeId));
				} else {
					parsedEdges.put(newId, new Edge(newId, startNode, endNode));
				}
			} else {
				LOGGER.warning(MessageFormat.format("Invalid edge ID {0}. This edge will be ignored.", edgeId));
			}
		}
		return parsedEdges;
	}

	/**
	 * @param idPath id of a node or complete path to the node
	 * @param parsedNodes
	 * @return node matching argument idPath or null
	 */
	private static Node getNodeWithId(String idPath, Map<String, Node> parsedNodes) {
		if (idPath.contains(DOT)) {
			return parsedNodes.get(idPath);
		} else {
			List<Node> filtered = parsedNodes.values().stream().filter(x -> x.getId().equals(idPath)).collect(Collectors.toList());
			if (filtered.size() == 1) {
				return filtered.get(0);
			} else if (filtered.size() >= 2) {
				return parsedNodes.get(idPath); // there might be 'A' and 'SUB.A'
			}
			return null;
		}
	}

	/**
	 * Parses main input file and fills parsedNodes and all the other ID lists
	 * @param fileName name of main input file
	 * @param parsedNodes map of parsed nodes to be filled, key = ID of node, value = Node itself
	 * @param edgeIds IDs of all edges, unaltered
	 * @param leftIds IDs of left nodes
	 * @param rightIds IDs of right nodes
	 * @param topIds IDs of top nodes
	 * @param bottomIds IDs of bottom nodes
	 * @param parsedSubGraphs
	 * @return true if parsing was successful, false if an error occurred.
	 */
	private static boolean parseJson(String fileName, Map<String, Node> parsedNodes, List<String> edgeIds,
			List<String> leftIds, List<String> rightIds, List<String> topIds, List<String> bottomIds, Map<String, SubGraph> parsedSubGraphs) {
		// Defaultly parsing edges to accept minimal input.
		boolean result = true;
		Parse parsing = Parse.EDGES;
		boolean endReached = false;
		int objectDepth = 0;
		List<SubGraph> hierarchyStack = new ArrayList<>();
		SubGraph rootGraph = new SubGraph("");
		parsedSubGraphs.put(rootGraph.toString(), rootGraph);
		hierarchyStack.add(rootGraph);

		try (JsonReader jsonReader = new JsonReader(new FileReader(fileName))){
			// Benevolent parsing
			jsonReader.setLenient(true);
			while (!endReached) {
				SubGraph currentParent = hierarchyStack.get(0);
				JsonToken nextToken = jsonReader.peek();
				if (JsonToken.BEGIN_OBJECT.equals(nextToken)) {
					objectDepth++;
					jsonReader.beginObject();
				} else if (JsonToken.END_OBJECT.equals(nextToken)) {
					if (objectDepth > 1) {
						hierarchyStack.remove(0);
					}
					objectDepth--;
					jsonReader.endObject();
				} else if (JsonToken.END_DOCUMENT.equals(nextToken)) {
					endReached = true;
				} else if (JsonToken.BEGIN_ARRAY.equals(nextToken)) {
					jsonReader.beginArray();
				} else if (JsonToken.END_ARRAY.equals(nextToken)) {
					jsonReader.endArray();
					if (objectDepth == 1) {
						parsing = Parse.NOTHING;
					}
				} else if (JsonToken.NULL.equals(nextToken)) {
					LOGGER.warning("NULL token detected while parsing main input file -> probably unexpected ',' at " + jsonReader.getPath());
					jsonReader.nextNull();
				} else if (JsonToken.NAME.equals(nextToken)) {
					String name = jsonReader.nextName();
					// Controls what part is now being parsed
					if (objectDepth == 1) {
						parsing = switchParsing(name, parsing);
					} else {
						if (parsedSubGraphs.containsKey(name) && parsedSubGraphs.get(name).getParent() == currentParent) {
							String message = "Fatal error. Two sub-graphs with same parent can't have same ID. Skipping the rest of parsing...";
							LOGGER.severe(message);
							Application.errorMessageForController(message);
							result = false;
							break;
						} else if (name.contains(DOT)) {
							String message = "Fatal error. ID of sub-graf can't contain dot '.', because it is used as sub-graph hierarchy separator. Skipping the rest of parsing...";
							LOGGER.severe(message);
							Application.errorMessageForController(message);
							result = false;
							break;
						} else if (name.equals("")) {
							String message = "Fatal error. ID of sub-graf cannot be empty string. Skipping the rest of parsing...";
							LOGGER.severe(message);
							Application.errorMessageForController(message);
							result = false;
							break;
						} else {
							SubGraph newSubGraph = new SubGraph(name);
							newSubGraph.setParent(currentParent);
							parsedSubGraphs.put(newSubGraph.toString(), newSubGraph);
							hierarchyStack.add(0, newSubGraph);
						}
					}
				} else if (JsonToken.STRING.equals(nextToken)) {
					String name = jsonReader.nextString().trim();
					switch (parsing) {
						case NOTHING:
							LOGGER.warning(MessageFormat.format(MESSAGE_UNEXPECTED_TOKEN, name));
							break;
						case NODES:
							if (name.contains(EDGE_SIGN) || name.contains(DOT)) {
								LOGGER.warning(MessageFormat.format(MESSAGE_INVALID_NODE, name));
							} else if (currentParent.getNodeWithId(name) != null) {
								LOGGER.warning(MessageFormat.format("Duplicated node ID {0} with parent {1}!", name, currentParent));
							} else {
								Node newNode = new Node(name);
								newNode.setParent(currentParent);
								parsedNodes.put(newNode.toString(), newNode);
							}
							break;
						case EDGES:
							edgeIds.add(name);
							break;
						// No need to check for '->' here, because if node contains '->' it will match neither
						// parsedNodes nor nodes created from edges (with minimal input)
						case LEFT:
							leftIds.add(name);
							break;
						case RIGHT:
							rightIds.add(name);
							break;
						case TOP:
							topIds.add(name);
							break;
						case BOTTOM:
							bottomIds.add(name);
							break;
						default:
							LOGGER.warning(MessageFormat.format(MESSAGE_UNEXPECTED_TOKEN, name));
							break;
					}
				}
			}
		} catch (IOException e) {
			String message = "Input file could not be parsed. " + e.getLocalizedMessage();
			LOGGER.severe(message);
			Application.errorMessageForController(message);
			return false;
		}
		return result;
	}

	/**
	 * Mapping of ELEMENT_X constants (strings) to enum Parse, case-insensitive.
	 * @param name string compared to ELEMENT_X constants
	 * @param previousParse returned if string doesn't match
	 * @return item of Parse enum that matched given name string, or previousParse if it doesn't match any constant
	 */
	private static Parse switchParsing(String name, Parse previousParse) {
		String nameLowerCase = name.toLowerCase();
		if (ELEMENT_NODES.equals(nameLowerCase)) {
			return Parse.NODES;
		} else if (ELEMENT_EDGES.equals(nameLowerCase)) {
			return Parse.EDGES;
		} else if (ELEMENT_LEFT.equals(nameLowerCase)) {
			return Parse.LEFT;
		} else if (ELEMENT_RIGHT.equals(nameLowerCase)) {
			return Parse.RIGHT;
		} else if (ELEMENT_TOP.equals(nameLowerCase)) {
			return Parse.TOP;
		} else if (ELEMENT_BOTTOM.equals(nameLowerCase)) {
			return Parse.BOTTOM;
		} else {
			LOGGER.warning(MessageFormat.format(MESSAGE_UNEXPECTED_TOKEN, name));
			return previousParse;
		}
	}
	

	/**
	 * Loads symbol definitions from TinyCad file and assigns them to nodes
	 * @param graph
	 * @param nodes
	 * @return next free ID of symbol definition
	 */
	public static int loadSymbolDefinitionsFromTinycad(GraphRepresentation graph) {
		int symbolDefID = 1;
		String filename = Preferences.getInstance().getTinycadInputFileName();
		if (filename == null) {
			return symbolDefID;
		}
		Map<String, String> nodesToSymbols = getNodesToSymbols(graph);
		if (nodesToSymbols.isEmpty()) {
			return symbolDefID;
		}
		// When we have a mapping, we will search through the file again to find all matching symbol definitions
		try (FileInputStream file = new FileInputStream(filename)) {
			XMLInputFactory xmlFactory = XMLInputFactory.newInstance();
			xmlFactory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
			XMLEventReader reader = xmlFactory.createXMLEventReader(file);
			String name;
			String id = null;
			String defName = "";
			String description = "";
			String loadedCharacters = null;
			Point2D size = null;
			List<Pin> pins = new ArrayList<>();
			List<Rectangle> rectangles = new ArrayList<>();
			List<Polygon> polygons = new ArrayList<>();
			Point2D pinPosition = null;
			String pinFlow;
			int pinDirection = 0;
			int pinType = 0;
			boolean pinOverride = Preferences.getInstance().getOverridePinTypes();
			Polygon polygon = null;
			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				switch (event.getEventType()) {
					case XMLStreamConstants.START_ELEMENT:
						StartElement startElement = event.asStartElement();
						name = startElement.getName().getLocalPart();
						Iterator<?> attributes = startElement.getAttributes();
						if (name.equalsIgnoreCase("SYMBOLDEF")) {
							while (attributes.hasNext()) {
								Attribute attribute = (Attribute) attributes.next();
								String attrName = attribute.getName().getLocalPart();
								if (attrName.equalsIgnoreCase("id")) {
									id = attribute.getValue();
									if (!nodesToSymbols.values().contains(id)) {
										id = null; // skip this symbol def, because we doen't have any matching node
									}
								}
							}
						} else if (id != null) {
							if (name.equalsIgnoreCase("REF_POINT")) {
								while (attributes.hasNext()) {
									Attribute attribute = (Attribute) attributes.next();
									String attrName = attribute.getName().getLocalPart();
									if (attrName.equalsIgnoreCase("pos")) {
										size = parsePointFromTinyCad(attribute.getValue(), true);
									}
								}
							} else if (name.equalsIgnoreCase("RECTANGLE")) {
								Point2D a = null;
								Point2D b = null;
								while (attributes.hasNext()) {
									Attribute attribute = (Attribute) attributes.next();
									String attrName = attribute.getName().getLocalPart();
									if (attrName.equalsIgnoreCase("a")) {
										a = parsePointFromTinyCad(attribute.getValue());
									} else if (attrName.equalsIgnoreCase("b")) {
										b = parsePointFromTinyCad(attribute.getValue());
									}
								}
								if (a != null && b != null) {
									rectangles.add(new Rectangle(a, b));
								}
							} else if (name.equalsIgnoreCase("PIN")) {
								while (attributes.hasNext()) {
									Attribute attribute = (Attribute) attributes.next();
									String attrName = attribute.getName().getLocalPart();
									if (attrName.equalsIgnoreCase("pos")) {
										pinPosition = parsePointFromTinyCad(attribute.getValue());
									} else if (attrName.equalsIgnoreCase("which")) {
										pinType = parseInteger(attribute.getValue());
									} else if (attrName.equalsIgnoreCase("direction")) {
										pinDirection = parseInteger(attribute.getValue());
									}
								}
							} else if (name.equalsIgnoreCase("POLYGON")) {
								while (attributes.hasNext()) {
									Attribute attribute = (Attribute) attributes.next();
									String attrName = attribute.getName().getLocalPart();
									if (attrName.equalsIgnoreCase("pos")) {
										Point2D polygonPosition = parsePointFromTinyCad(attribute.getValue());
										if (polygonPosition != null) {
											polygon = new Polygon(polygonPosition);
											polygons.add(polygon);
										}
									}
								}
							} else if (name.equalsIgnoreCase("POINT") && polygon != null) {
								while (attributes.hasNext()) {
									Attribute attribute = (Attribute) attributes.next();
									String attrName = attribute.getName().getLocalPart();
									if (attrName.equalsIgnoreCase("pos")) {
										Point2D point = parsePointFromTinyCad(attribute.getValue());
										polygon.addPoint(point);
									}
								}
							}
						}
						break;

					case XMLStreamConstants.CHARACTERS:
						Characters characters = event.asCharacters();
						loadedCharacters = characters.getData(); // save loaded characters, we will use them at the end of element
						break;

					case XMLStreamConstants.END_ELEMENT:
						EndElement endElement = event.asEndElement();
						name = endElement.getName().getLocalPart();

						if (id != null) {
							if (name.equalsIgnoreCase("PIN") && pinPosition != null) {
								if (loadedCharacters != null) {
									pinFlow = (loadedCharacters.toUpperCase().contains("OUT") || loadedCharacters.toUpperCase().contains("IN"))
											? loadedCharacters : "IN";
								} else {
									pinFlow = "IN";
								}
								if (pinFlow.toUpperCase().contains("IN") && pinOverride) {
									pinType = OutputGenerator.PIN_ARROW;
								}
								pins.add(new Pin(pinPosition.getX(), pinPosition.getY(), pinType, pinDirection, pinFlow, null));
								pinPosition = null;
							} else if (name.equalsIgnoreCase("POLYGON")) {
								polygon = null;
							} else if (name.equalsIgnoreCase("SYMBOLDEF")) {
								for (Entry<String, String> entry : nodesToSymbols.entrySet()) {
									Node node = graph.getNodeByID(entry.getKey());
									if (id.equals(entry.getValue()) && node != null) {
										SymbolDefinition symbolDef = new SymbolDefinition(symbolDefID, node, defName, description);
										symbolDefID++;
										pins.forEach(x -> x.getCopy(symbolDef));
										rectangles.stream().map(Rectangle::getCopy).forEach(symbolDef::addRectangle);
										polygons.stream().map(Polygon::getCopy).forEach(symbolDef::addPolygon);
										if (size != null) {
											node.setSize(size);
										}
									}
								}
								size = null;
								id = null;
								pins = new ArrayList<>();
								rectangles = new ArrayList<>();
								polygons = new ArrayList<>();
								defName = "";
								description = "";
							} else if (loadedCharacters != null) {
								if (name.equalsIgnoreCase("NAME")) {
									defName = loadedCharacters;
								} else if (name.equalsIgnoreCase("DESCRIPTION")) {
									description = loadedCharacters;
								}
							}
						}
						loadedCharacters = null; // forget loaded characters
						break;

					default:
						break;
				}
			}
		} catch (@SuppressWarnings("unused") FileNotFoundException e) {
			LOGGER.severe(MessageFormat.format("TinyCad file ''{0}'' not found! TinyCad input will not be parsed.", filename));
		} catch (@SuppressWarnings("unused") IOException | XMLStreamException e) {
			LOGGER.severe("An error occured during parsing of TinyCad input. TinyCad input may not be parsed completely.");
		}
		return symbolDefID;
	}

	/**
	 * @param posString
	 * @return position/size as Point2D parsed from symbol definition or null, if format doesn't match
	 */
	private static Point2D parsePointFromTinyCad(String posString) {
		return parsePointFromTinyCad(posString, false);
	}

	/**
	 * @param posString
	 * @param multiply whether to multiply the size by 5 (to convert from TinyCad values to pixels)
	 * @return position/size as Point2D parsed from symbol definition or null, if format doesn't match
	 */
	private static Point2D parsePointFromTinyCad(String posString, boolean multiply) {
		String[] parts = posString.split(",");
		if(parts.length == 2) {
			try {
				double x = Double.parseDouble(parts[0]);
				double y = Double.parseDouble(parts[1]);
				if (multiply) {
					x *= 5;
					y *= 5;
				}
				return new Point2D.Double(x, y);
			} catch (@SuppressWarnings("unused") NumberFormatException e) {
				return null;
			}
		}
		return null;
	}

	/**
	 * @param intString
	 * @return integer parsed from string or 0 if string has bad format
	 */
	private static int parseInteger(String intString) {
		try {
			return Integer.parseInt(intString);
		} catch (@SuppressWarnings("unused") NumberFormatException e) {
			return 0;
		}
	}

	/**
	 * Searches for symbols matching nodes in TinyCad file.<br>
	 * Adds each match to result mapping and saves position of a name of node to the node.
	 * @param graph
	 * @return mapping, where key is id of a node and value is ID of a symbol definition from TinyCad input file
	 */
	private static Map<String, String> getNodesToSymbols(GraphRepresentation graph) {
		Map<String, String> nodesToSymbols = new HashMap<>();
		String filename = Preferences.getInstance().getTinycadInputFileName();
		if (filename == null) {
			return nodesToSymbols;
		}
		try (FileInputStream file = new FileInputStream(filename)) {
			XMLInputFactory xmlFactory = XMLInputFactory.newInstance();
			xmlFactory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
			XMLEventReader reader = xmlFactory.createXMLEventReader(file);
			String id = null;
			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				String name;
				switch (event.getEventType()) {
					case XMLStreamConstants.START_ELEMENT:
						StartElement startElement = event.asStartElement();
						name = startElement.getName().getLocalPart();
						if (name.equalsIgnoreCase("SYMBOL")) {
							Iterator<?> attributes = startElement.getAttributes();
							while(attributes.hasNext()) {
								Attribute attribute = (Attribute) attributes.next();
								String attrName = attribute.getName().getLocalPart();
								if (attrName.equalsIgnoreCase("id")) {
									id = attribute.getValue();
								}
							}
						} else if (name.equalsIgnoreCase("FIELD") && id != null) {
							Iterator<?> attributes = startElement.getAttributes();
							boolean isRefField = false;
							String nodeId = null;
							String namePosition = null;
							while (attributes.hasNext()) {
								Attribute attribute = (Attribute) attributes.next();
								String attrName = attribute.getName().getLocalPart();
								if (attrName.equalsIgnoreCase("description") && attribute.getValue().equalsIgnoreCase("Ref")) {
									isRefField = true;
								} else if (attrName.equalsIgnoreCase("value")) {
									nodeId = attribute.getValue();
								} else if (attrName.equalsIgnoreCase("pos")) {
									namePosition = attribute.getValue();
								}
							}
							Node node = graph.getNodeByID(nodeId);
							if (isRefField && node != null && namePosition != null) {
								Point2D position = parsePointFromTinyCad(namePosition);
								if (position != null) {
									node.setNamePosition(position);
								}
								nodesToSymbols.put(node.getId(), id);
							}
						}
						break;

					default:
					case XMLStreamConstants.CHARACTERS:
						break;

					case XMLStreamConstants.END_ELEMENT:
						EndElement endElement = event.asEndElement();
						name = endElement.getName().getLocalPart();
						if (name.equalsIgnoreCase("SYMBOL")) {
							id = null;
						}
						break;
				}
			}
		} catch (@SuppressWarnings("unused") FileNotFoundException e) {
			LOGGER.severe(MessageFormat.format("TinyCad file ''{0}'' not found! TinyCad input will not be parsed.", filename));
		} catch (@SuppressWarnings("unused") IOException | XMLStreamException e) {
			LOGGER.severe("An error occured during parsing of TinyCad input. TinyCad input may not be parsed completely.");
		}
		return nodesToSymbols;
	}
	
	
	/**
	 * Enum describing what part of input file is InputParser currently parsing
	 * @author Jan Juda (xjudaj00)
	 */
	public enum Parse {
		/** Nothing is being parsed */
		NOTHING,
		/** Nodes are being parsed */
		NODES,
		/** Edges are being parsed */
		EDGES,
		/** Left nodes are being parsed */
		LEFT,
		/** Right nodes are being parsed */
		RIGHT,
		/** Top nodes are being parsed */
		TOP,
		/** Bottom nodes are being parsed */
		BOTTOM
	}
}
