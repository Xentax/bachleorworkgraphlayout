/**
 * Copyright (c) 2019, Jan Juda
	All rights reserved.
	BSD Licence.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY Jan Juda ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL Jan Juda BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 * Created: 12. 10. 2019
 */
package autonodeplacement;

import static autonodeplacement.representation.GraphRepresentation.debugView;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import autonodeplacement.algorithms.AbstractAdapter;
import autonodeplacement.algorithms.Algorithms;
import autonodeplacement.representation.GraphRepresentation;

/**
 * Main Application entry point
 * @author Jan Juda (xjudaj00)
 */
public class Application {
	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(Application.class.toString());

	/** Name of the application */
	public static final String NAME = "DAGio";

	/** Holds instance of GUI controller */
	private static GuiController guiController = null;

	/**
	 * Method main
	 * @param args
	 */
	public static void main(String[] args) {
		// Parse preferences
		Preferences preferences = Preferences.getInstance();
		preferences.parseArgs(args);
		// Start GUI if desired
		if (preferences.isGuiRequested()) {
			guiController = new GuiController();
			guiController.openGui();
		} else {
			applicationRun();
		}
	}

	/**
	 * This is the main program body. It contains whole process of laying out a graph - from loading input to saving output.
	 */
	public static void applicationRun() {
		try {
			Preferences preferences = Preferences.getInstance();
			// Check if configuration is valid
			if (!preferences.getSizePostprocessAllowed()) {
				String message;
				if (preferences.getSelectedAlgorithm().equals(Algorithms.BEST)) {
					message = "Configuration error: Size post-processing must be enabled when BEST algorithm mode is selected.";
					LOGGER.severe(message);
					errorMessageForController(message);
					return;
				} else if (!preferences.getLimitLayoutSpace()) {
					message = "Configuration error: Either size post-processing (--sizepostprocess) or Limit layout space"
							+ "(--limit-layout-space) option must be enabled.";
					LOGGER.severe(message);
					errorMessageForController(message);
					return;
				}
			}
			if (!preferences.getArrowsAllowed()) {
				preferences.setOverridePinTypes(false);
			}
			// Obtain diagram from input file
			GraphRepresentation graph = InputParser.parseMainInput(preferences);
			if (graph == null || graph.getRootGraph() == null) {
				return;
			}
			// Parse information from TinyCad file if present
			int nextSymbolDefID = 1;
			if (preferences.getTinycadInputFileName() != null) {
				nextSymbolDefID = InputParser.loadSymbolDefinitionsFromTinycad(graph);
			}
			// If no positional nodes parsed, disable positional nodes parsing
			if (preferences.getParsePositional() && !graph.hasPositionalNodes()) {
				preferences.setParsePositional(false);
			}
			// Force deterministic if user insists
			if (Preferences.getInstance().getForceDeterministicBehaviour()) {
				PreProcessing.forceDeterministicBehaviour();
			}
			// Apply pre-processing
			PreProcessing.checkGraphSize(graph);
			// Do the heavy work!
			List<GraphRepresentation> results = layout(graph);
			if (results.isEmpty()) {
				errorMessageForController("No results were generated. Error occurred in preprocessing or in iteration loop."
						+ "See standard error output (or log) for more info.");
				return;
			}
			// Show result and generate output
			if (preferences.showResult()) {
				int i = 0;
				for (GraphRepresentation result : results) {
					i++;
					debugView(result, MessageFormat.format("Result number {0}, score {1}, algorithm {2}", Integer.valueOf(i),
							Long.valueOf(result.getSavedScore()), result.getLaidOutBy()));
				}
			}
			if (preferences.getGenerateOutput()) {
				String path = preferences.getOutputPath();
				String prefix = preferences.getOutputFilePrefix();
				String suffix = ".dsn";
				if (preferences.timeStamp()) {
					suffix = "_" + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()) + suffix;
				}
				if (!(path.endsWith("/") || path.endsWith(":"))) {
					path += "/";
				}
				int i = 0;
				for (GraphRepresentation result : results) {
					i++;
					OutputGenerator generator = new OutputGenerator(nextSymbolDefID);
					// File name format: path/prefix_N_SCORE_ALGORITHM.dsn or path/prefix_N_SCORE_ALGORITHM_TIMESTAMP.dsn
					generator.generateOutput(result, path + prefix + "_" + i + "_" + result.getSavedScore() + "_" + result.getLaidOutBy() + suffix);
				}
			}
			LOGGER.info("Successfully completed.");
		} catch (Exception e) {
			errorMessageForController("Unexpected error. See out error stream/logs for more info");
			LOGGER.severe("Unexpected error: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Performs the main part of laying out a graph
	 * @param graph
	 * @return best results of laying out a graph or null if severe error occurred
	 */
	private static List<GraphRepresentation> layout(GraphRepresentation graph) {
		if (PreProcessing.prepareSubGraphs(graph.getRootGraph())) {
			return new ArrayList<>();
		}
		List<AbstractAdapter> initialLayouts = PreProcessing.initialLayoutAndCreateAdapters(graph);
		return PreProcessing.runAlgorithmIterations(initialLayouts);
	}

	/**
	 * Notifies GUI controller, that a step (1 iteration) in laying out a graph was taken
	 */
	public static void notifyController() {
		if (guiController != null) {
			guiController.stepTaken();
		}
	}

	/**
	 * Notifies GUI controller, that error occurred.
	 * @param message error message
	 */
	public static void errorMessageForController(String message) {
		if (guiController != null) {
			guiController.errorOccurred(message);
		}
	}
}
