/**
 * Copyright 2020 Jan Juda
 * Created: 17. 3. 2020
 */
package autonodeplacement;

import java.awt.Component;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.DefaultFormatter;

import autonodeplacement.algorithms.Algorithms;

/**
 * Class holding main GUI
 * @author Jan Juda (xjudaj00)
 */
@SuppressWarnings("javadoc") // Because this file is mainly generated.
public class GUI extends javax.swing.JFrame {
    
	private static final long serialVersionUID = 1L;

	/** Hold all score check boxes. */
	private Map<Score, JCheckBox> scoreBoxes = new LinkedHashMap<>();
	/** Hold all score weight spinners. */
	private Map<Score, JSpinner> scoreSpinners = new LinkedHashMap<>();

	/**
	 * Creates new form GUI
	 * @param guiController
	 */
	public GUI(GuiController guiController) {
        initComponents();
        setLocationRelativeTo(null);
		// Buttons & Comboboxes
        layoutButton.addActionListener(guiController.getActionListener());
        mainInputButton.addActionListener(guiController.getActionListener());
        tinycadInputButton.addActionListener(guiController.getActionListener());
        tinycadOutputButton.addActionListener(guiController.getActionListener());
		algorithmCombobox.addActionListener(guiController.getActionListener());
		// Checkboxes
        tinycadCheckbox.addItemListener(guiController.getItemListener());
		arrowsCheckBox.addItemListener(guiController.getItemListener());
		pinOverrideCheckBox.addItemListener(guiController.getItemListener());
		timestampCheckbox.addItemListener(guiController.getItemListener());
		randomCheckBox.addItemListener(guiController.getItemListener());
		limitLayoutSpaceCheckBox.addItemListener(guiController.getItemListener());
		lockOtherCheckBox.addItemListener(guiController.getItemListener());
		prefPosCheckBox.addItemListener(guiController.getItemListener());
		postprocessCheckBox.addItemListener(guiController.getItemListener());
		// TextFields
		guiController.addTextListener(mainInputField);
		guiController.addTextListener(tinycadInputField);
		guiController.addTextListener(tinycadOutputField);
		guiController.addTextListener(tinycadOutputPrefixField);
		// Spinners
		setCommitsForSpinner(iterationsSpinner);
		setCommitsForSpinner(initSpinner);
		setCommitsForSpinner(resultsSpinner);
		setCommitsForSpinner(maxiterationsSpinner);
		setCommitsForSpinner(widthSpinner);
		setCommitsForSpinner(heightSpinner);
		setCommitsForSpinner(nodeborderSpinner);
		setCommitsForSpinner(subgraphborderSpinner);
		iterationsSpinner.addChangeListener(guiController.getChangeListener());
		initSpinner.addChangeListener(guiController.getChangeListener());
		resultsSpinner.addChangeListener(guiController.getChangeListener());
		maxiterationsSpinner.addChangeListener(guiController.getChangeListener());
		widthSpinner.addChangeListener(guiController.getChangeListener());
		heightSpinner.addChangeListener(guiController.getChangeListener());
		nodeborderSpinner.addChangeListener(guiController.getChangeListener());
		subgraphborderSpinner.addChangeListener(guiController.getChangeListener());
		// Score
		Map<Score, Integer> selectedScored = Preferences.getInstance().getSelectedScores();
		scorePanel.setLayout(new GridLayout(Score.values().length + 1, 2, 6, 6));
		scorePanel.add(new JLabel("Score rules:"));
		scorePanel.add(new JLabel("Rule weights:"));
		for (Score score : Arrays.asList(Score.values())) {
			JCheckBox checkbox = new JCheckBox(score.toString(), selectedScored.keySet().contains(score));
			checkbox.setToolTipText(descriptionToTooltip(score.getDescription()));
			checkbox.setActionCommand(score.toString());
			checkbox.addItemListener(guiController.getItemListener());
			scoreBoxes.put(score, checkbox);
			Integer weight = selectedScored.containsKey(score) ? selectedScored.get(score) : Integer.valueOf(100);
			JSpinner spinner = new JSpinner(new SpinnerNumberModel(weight, Integer.valueOf(0), null, Integer.valueOf(1)));
			spinner.setToolTipText("Weight of the score rule " + score.toString());
			spinner.setName(score.toString());
			scoreSpinners.put(score, spinner);
			scorePanel.add(checkbox);
			scorePanel.add(spinner);
			spinner.addChangeListener(guiController.getChangeListener());
		}
		// Algorithm tooltips
		algorithmCombobox.setRenderer(new DefaultListCellRenderer() {

			/** Generated serial UID */
			private static final long serialVersionUID = -3775471105015128884L;

			@Override
			public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				if (value instanceof Algorithms) {
					Algorithms focusedAlgorithm = (Algorithms) value;
					list.setToolTipText(descriptionToTooltip(focusedAlgorithm.getDescription()));
				}
				return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			}

		});
    }

	public static String descriptionToTooltip(String description) {
		return "<html>" + description.replaceAll("\\n", "<br>");
	}

	/**
	 * @return all output components which are dependent on "Generate output" checkbox
	 */
	public List<JComponent> getOutputComponents() {
		List<JComponent> list = new ArrayList<>();
		list.add(arrowsCheckBox);
		list.add(pinOverrideCheckBox);
		list.add(timestampCheckbox);
		list.add(tinycadOutputButton);
		list.add(tinycadOutputField);
		list.add(tinycadOutputPrefixField);
		return list;
	}

	/**
	 * @return map of scores and their weight spinners
	 */
	public Map<Score, JSpinner> getScoreWeightSpinners() {
		return scoreSpinners;
	}

	/**
	 * Sets that spinner commits (sends events for) each correct edit.
	 * @param spinner
	 */
	private static void setCommitsForSpinner(JSpinner spinner) {
		if (spinner.getEditor() instanceof JSpinner.DefaultEditor) {
			JSpinner.DefaultEditor editor = (DefaultEditor) spinner.getEditor();
			if (editor.getTextField().getFormatter() instanceof DefaultFormatter) {
				DefaultFormatter formater = (DefaultFormatter) editor.getTextField().getFormatter();
				formater.setCommitsOnValidEdit(true);
			}
		}
	}

	public JCheckBox getRandomCheckBox() {
		return randomCheckBox;
	}

	public JCheckBox getLimitLayoutSpaceCheckBox() {
		return limitLayoutSpaceCheckBox;
	}

	public JCheckBox getLockOtherCheckBox() {
		return lockOtherCheckBox;
	}

	public JTextField getMainInputField() {
		return mainInputField;
	}

	public JTextField getTinycadInputField() {
		return tinycadInputField;
	}

	public JTextField getTinycadOutputField() {
		return tinycadOutputField;
	}

	public JCheckBox getTinycadCheckbox() {
		return tinycadCheckbox;
	}

	public JTextArea getInfoTextArea() {
		return infoTextArea;
	}

	public JButton getLayoutButton() {
		return layoutButton;
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public JComboBox<Algorithms> getAlgorithmCombobox() {
		return algorithmCombobox;
	}

	public JSpinner getIterationsSpinner() {
		return iterationsSpinner;
	}

	public JSpinner getInitSpinner() {
		return initSpinner;
	}

	public JSpinner getResultsSpinner() {
		return resultsSpinner;
	}

	public JSpinner getMaxiterationsSpinner() {
		return maxiterationsSpinner;
	}

	public JSpinner getWidthSpinner() {
		return widthSpinner;
	}

	public JSpinner getHeightSpinner() {
		return heightSpinner;
	}

	public JSpinner getNodeborderSpinner() {
		return nodeborderSpinner;
	}

	public JSpinner getSubgraphborderSpinner() {
		return subgraphborderSpinner;
	}

	public JTextField getTinycadOutputPrefixField() {
		return tinycadOutputPrefixField;
	}

	public JCheckBox getPrefPosCheckBox() {
		return prefPosCheckBox;
	}

	public JCheckBox getPostprocessCheckBox() {
		return postprocessCheckBox;
	}

	public JCheckBox getArrowsCheckBox() {
		return arrowsCheckBox;
	}

	public JCheckBox getPinOverrideCheckBox() {
		return pinOverrideCheckBox;
	}

	public JCheckBox getTimestampCheckbox() {
		return timestampCheckbox;
	}

	public JCheckBox getShowresultCheckbox() {
		return showresultCheckbox;
	}

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        tinycadOutputField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        mainInputField = new javax.swing.JTextField();
        mainInputButton = new javax.swing.JButton();
        tinycadInputButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        tinycadInputField = new javax.swing.JTextField();
        tinycadCheckbox = new javax.swing.JCheckBox();
        tinycadOutputButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        algorithmCombobox = new javax.swing.JComboBox<>();
        randomCheckBox = new javax.swing.JCheckBox();
        prefPosCheckBox = new javax.swing.JCheckBox();
        postprocessCheckBox = new javax.swing.JCheckBox();
        jLabel10 = new javax.swing.JLabel();
        maxiterationsSpinner = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        iterationsSpinner = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        initSpinner = new javax.swing.JSpinner();
        jLabel7 = new javax.swing.JLabel();
        resultsSpinner = new javax.swing.JSpinner();
        limitLayoutSpaceCheckBox = new javax.swing.JCheckBox();
        lockOtherCheckBox = new javax.swing.JCheckBox();
        scorePanel = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        widthSpinner = new javax.swing.JSpinner();
        nodeborderSpinner = new javax.swing.JSpinner();
        heightSpinner = new javax.swing.JSpinner();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        subgraphborderSpinner = new javax.swing.JSpinner();
        jLabel21 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        arrowsCheckBox = new javax.swing.JCheckBox();
        pinOverrideCheckBox = new javax.swing.JCheckBox();
        timestampCheckbox = new javax.swing.JCheckBox();
        jLabel12 = new javax.swing.JLabel();
        tinycadOutputPrefixField = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        layoutButton = new javax.swing.JButton();
        showresultCheckbox = new javax.swing.JCheckBox();
        jLabel20 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        infoTextArea = new javax.swing.JTextArea();
        progressBar = new javax.swing.JProgressBar();
        jLabel19 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("DAGio");
        setMinimumSize(new java.awt.Dimension(665, 800));

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setText("TinyCAD output path:");
        jLabel3.setMaximumSize(new java.awt.Dimension(97, 16));
        jLabel3.setMinimumSize(new java.awt.Dimension(97, 16));
        jLabel3.setPreferredSize(new java.awt.Dimension(97, 16));

        tinycadOutputField.setToolTipText("<html>Path to the folder, where all output files will be saved to.<br>\nFull and relative paths supported.<br>\nDisabled, when generating TinyCAD output is disabled.");
        tinycadOutputField.setName("tinycadOutput"); // NOI18N

        jLabel1.setText("Main graph input:");

        mainInputField.setToolTipText("<html>Enter the name of a main input file.<br>\nFull and relative paths supported.");
        mainInputField.setName("mainInput"); // NOI18N

        mainInputButton.setText("Select...");
        mainInputButton.setToolTipText("Select main input file.");
        mainInputButton.setActionCommand("selectMainInput");

        tinycadInputButton.setText("Select...");
        tinycadInputButton.setToolTipText("Select TinyCAD input file.");
        tinycadInputButton.setActionCommand("selectTinycadInput");

        jLabel2.setText("TinyCAD input:");

        tinycadInputField.setToolTipText("<html>Enter the name of a TinyCAD input file.<br>\nFull and relative paths supported.<br>\n<b>Leave empty if you don't want to use secondary input.</b>");
        tinycadInputField.setName("tinycadInput"); // NOI18N

        tinycadCheckbox.setSelected(true);
        tinycadCheckbox.setText("Generate TinyCAD output?");
        tinycadCheckbox.setToolTipText("Do you want to save the resullts into files?");
        tinycadCheckbox.setActionCommand("generateOutput");

        tinycadOutputButton.setText("Select...");
        tinycadOutputButton.setToolTipText("<html>Selects path to the output folder.<br>\nDisabled, when generating TinyCAD output is disabled.");
        tinycadOutputButton.setActionCommand("selectOutputPath");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tinycadCheckbox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tinycadInputField)
                            .addComponent(mainInputField))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mainInputButton, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(tinycadInputButton, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tinycadOutputField)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tinycadOutputButton)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mainInputButton)
                    .addComponent(mainInputField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tinycadInputButton)
                    .addComponent(jLabel2)
                    .addComponent(tinycadInputField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tinycadCheckbox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tinycadOutputButton)
                    .addComponent(tinycadOutputField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setToolTipText("");
        jPanel3.setName(""); // NOI18N

        jLabel4.setText("Selected layout algorithm:");

        algorithmCombobox.setModel(new DefaultComboBoxModel<>(Algorithms.values()));
        algorithmCombobox.setToolTipText("Select layout algorithm");
        algorithmCombobox.setActionCommand("algorithmChanged");

        randomCheckBox.setSelected(true);
        randomCheckBox.setText("Random initialization of nodes position");
        randomCheckBox.setToolTipText("<html>If enabled, initial layout will be random.<br>\nIf disabled, initial layout will be performed deterministically.<br>\n<b>To get a best result, random initializations should be enabled.</b>");
        randomCheckBox.setActionCommand("randomInit");

        prefPosCheckBox.setText("Parse nodes with preferred position");
        prefPosCheckBox.setToolTipText("<html>This option allows you to disable parsing preferred position of nodes<br>\nwithout having to edit the input file.");
        prefPosCheckBox.setActionCommand("prefPos");

        postprocessCheckBox.setText("Enable size post-processing");
        postprocessCheckBox.setToolTipText("<html>With this option you can disable size post-processing.<br>\nThat will result in no overlap or overflow correction.<br>\nThis can only be disabled, if Limit layout space option is enabled.<br>\n<b>To get a best results, keep this enabled.</b>");
        postprocessCheckBox.setActionCommand("postProcess");

        jLabel10.setText("Max iterations of post-processing:");

        maxiterationsSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(100), Integer.valueOf(1), null, Integer.valueOf(1)));
        maxiterationsSpinner.setToolTipText("<html>Maximum iterations for post-processing.<br>\nYou should increase this value, if you see info saying, that this limit<br>\nwas reached and you can see overlapping or overflowing nodes in the result.");
        maxiterationsSpinner.setName("maxiterations"); // NOI18N

        jLabel5.setText("Number of iterations:");

        iterationsSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(100), Integer.valueOf(1), null, Integer.valueOf(1)));
        iterationsSpinner.setToolTipText("<html>Select number of iterations to be performed for each initial<br>\nMore iterations means better results, but also more time.");
        iterationsSpinner.setName("iterations"); // NOI18N

        jLabel6.setText("Number of start initializations:");

        initSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
        initSpinner.setToolTipText("<html>Select number of initial layouts.<br>\nThis option is set to 1, if random initialization is disabled.");
        initSpinner.setName("init"); // NOI18N

        jLabel7.setText("Number of best results:");

        resultsSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
        resultsSpinner.setToolTipText("<html>Number of best results to be stored.<br>\nAll of these results will be saved (if generating output is enabled) and<br>\nshowed in separate windows (if showing result is enabled).<br>\nIf you set this to bigger value, you should consider disabling showing grafical output.");
        resultsSpinner.setName("results"); // NOI18N

        limitLayoutSpaceCheckBox.setText("Limit layout space");
        limitLayoutSpaceCheckBox.setToolTipText("<html>Limit layout space to the space of the part, which is currently being processed.<br>\nThis feature is currently supported only by FR and ISOM algorithm.<br>\n<b>To get a best results, enable this feature for ISOM algorithm, and<br>\nfor FR algorithm, enable this only, if Lock other nodes is also enabled.</b>\n");
        limitLayoutSpaceCheckBox.setActionCommand("limitLayoutSpace");

        lockOtherCheckBox.setText("Lock other nodes");
        lockOtherCheckBox.setToolTipText("<html>Lock nodes not belonging to the part, which is currently being processed.<br>\nThis feature is currently supported only by FR algorithm.<br>\n<b>To get a best results, enable this feature for FR algorithm.</b>\n");
        lockOtherCheckBox.setActionCommand("lockOtherNodes");

        scorePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout scorePanelLayout = new javax.swing.GroupLayout(scorePanel);
        scorePanel.setLayout(scorePanelLayout);
        scorePanelLayout.setHorizontalGroup(
            scorePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        scorePanelLayout.setVerticalGroup(
            scorePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabel17.setText("Score options:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scorePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(resultsSpinner)
                            .addComponent(iterationsSpinner)
                            .addComponent(initSpinner)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(maxiterationsSpinner))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(postprocessCheckBox)
                            .addComponent(prefPosCheckBox)
                            .addComponent(randomCheckBox)
                            .addComponent(lockOtherCheckBox)
                            .addComponent(limitLayoutSpaceCheckBox)
                            .addComponent(jLabel17))
                        .addGap(0, 23, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(28, 28, 28)
                        .addComponent(algorithmCombobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(algorithmCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(iterationsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(initSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(resultsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(randomCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(limitLayoutSpaceCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lockOtherCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prefPosCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(postprocessCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(maxiterationsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17)
                .addGap(2, 2, 2)
                .addComponent(scorePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel13.setText("Algorithm options:");

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel8.setText("Graph width:");

        widthSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1280), Integer.valueOf(0), null, Integer.valueOf(1)));
        widthSpinner.setToolTipText("Specify width of the graph space.");
        widthSpinner.setName("width"); // NOI18N

        nodeborderSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(10), Integer.valueOf(0), null, Integer.valueOf(1)));
        nodeborderSpinner.setToolTipText("<html>Specify size of additional border around nodes.<br>\nDefault value is 10px, meaning the node will have border of clear space around it and<br>\nthe bounding box of the node will be enlarged by 10px in each direction.\n");
        nodeborderSpinner.setName("nodeborder"); // NOI18N

        heightSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(720), Integer.valueOf(0), null, Integer.valueOf(1)));
        heightSpinner.setToolTipText("Specify height of the graph space.");
        heightSpinner.setName("height"); // NOI18N

        jLabel9.setText("Graph height:");

        jLabel11.setText("Border for nodes:");

        jLabel15.setText("Border for subgraphs:");

        subgraphborderSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(10), Integer.valueOf(0), null, Integer.valueOf(1)));
        subgraphborderSpinner.setToolTipText("<html>Specify size of additional outer border around subgraphs.<br>\nDefault value is 10px, meaning the subgraph will have border of clear space around it and<br>\nthe bounding box of the subgraph will be enlarged by 10px in each direction.\n");
        subgraphborderSpinner.setName("subgraphborder"); // NOI18N

        jLabel21.setText("values in pixels (px)");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(heightSpinner, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                            .addComponent(nodeborderSpinner)
                            .addComponent(widthSpinner)
                            .addComponent(subgraphborderSpinner)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel21)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(widthSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(heightSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(nodeborderSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(subgraphborderSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel14.setText("TinyCAD output options:");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        arrowsCheckBox.setText("Draw arrows into output file");
        arrowsCheckBox.setToolTipText("<html>Should edges end with arrow-type pins?<br>\nDo not apply to pins loaded from TinyCAD input.<br>\nDisabled, when generating TinyCAD output is disabled.");
        arrowsCheckBox.setActionCommand("arrows");

        pinOverrideCheckBox.setText("Override pin type");
        pinOverrideCheckBox.setToolTipText("<html>Overrides input pin (ends of edges) types loaded from TinyCAD input with arrow type.<br>\nDisabled, when generating TinyCAD output is disabled. <br>\nWorks only, if Drawing arrows is allowed.");
        pinOverrideCheckBox.setActionCommand("overridePin");

        timestampCheckbox.setText("Add timestamp to generated files");
        timestampCheckbox.setToolTipText("<html>Should names of output files contain timestamp?<br>\nThis can prevent overriding files already existing in output folder.<br>\nDisabled, when generating TinyCAD output is disabled.");
        timestampCheckbox.setActionCommand("timestamp");

        jLabel12.setText("Output file prefix:");

        tinycadOutputPrefixField.setToolTipText("<html>Prefix of name of generated files.<br>\nDisabled, when generating TinyCAD output is disabled.");
        tinycadOutputPrefixField.setName("tinycadOutputPrefix"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(timestampCheckbox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(arrowsCheckBox)
                            .addComponent(pinOverrideCheckBox)
                            .addComponent(jLabel12))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(tinycadOutputPrefixField))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tinycadOutputPrefixField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(arrowsCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pinOverrideCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(timestampCheckbox)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel16.setText("Graph options:");

        jLabel18.setText("Main input & output options:");

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        layoutButton.setText("Layout the graph!");
        layoutButton.setToolTipText("<html>Press this button to start layout operation.<br>\nThe operation can be cancelled at any time.<br>\nThis button is disabled, if serious error in configuration is detected.");
        layoutButton.setActionCommand("layout");

        showresultCheckbox.setSelected(true);
        showresultCheckbox.setText("Show result?");
        showresultCheckbox.setToolTipText("<html>If enabled, window with grafical output will be showned for each of the best results.<br>\nIf you specify high number of best results, you should consider disabling this option.");

        jLabel20.setText("Warnings and information:");

        infoTextArea.setEditable(false);
        infoTextArea.setColumns(20);
        infoTextArea.setLineWrap(true);
        infoTextArea.setRows(5);
        infoTextArea.setToolTipText("<html>Here are shown information, warnings and errors in configuration.<br>\nIf error happens during layout operation, the error message will also appear here.");
        infoTextArea.setWrapStyleWord(true);
        jScrollPane1.setViewportView(infoTextArea);

        progressBar.setToolTipText("Shows progress of the layout operation.");
        progressBar.setString("Progress of layouting will be showned here");
        progressBar.setStringPainted(true);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(layoutButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(showresultCheckbox, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(layoutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(showresultCheckbox, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel19.setText("Result:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel19)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel18)
                .addGap(2, 2, 2)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel16))
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel14)
                        .addGap(2, 2, 2)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel19)
                        .addGap(2, 2, 2)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<Algorithms> algorithmCombobox;
    private javax.swing.JCheckBox arrowsCheckBox;
    private javax.swing.JSpinner heightSpinner;
    private javax.swing.JTextArea infoTextArea;
    private javax.swing.JSpinner initSpinner;
    private javax.swing.JSpinner iterationsSpinner;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton layoutButton;
    private javax.swing.JCheckBox limitLayoutSpaceCheckBox;
    private javax.swing.JCheckBox lockOtherCheckBox;
    private javax.swing.JButton mainInputButton;
    private javax.swing.JTextField mainInputField;
    private javax.swing.JSpinner maxiterationsSpinner;
    private javax.swing.JSpinner nodeborderSpinner;
    private javax.swing.JCheckBox pinOverrideCheckBox;
    private javax.swing.JCheckBox postprocessCheckBox;
    private javax.swing.JCheckBox prefPosCheckBox;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JCheckBox randomCheckBox;
    private javax.swing.JSpinner resultsSpinner;
    private javax.swing.JPanel scorePanel;
    private javax.swing.JCheckBox showresultCheckbox;
    private javax.swing.JSpinner subgraphborderSpinner;
    private javax.swing.JCheckBox timestampCheckbox;
    private javax.swing.JCheckBox tinycadCheckbox;
    private javax.swing.JButton tinycadInputButton;
    private javax.swing.JTextField tinycadInputField;
    private javax.swing.JButton tinycadOutputButton;
    private javax.swing.JTextField tinycadOutputField;
    private javax.swing.JTextField tinycadOutputPrefixField;
    private javax.swing.JSpinner widthSpinner;
    // End of variables declaration//GEN-END:variables

}
