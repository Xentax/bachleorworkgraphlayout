/**
 * Copyright 2020 Jan Juda
 * Created: 9. 3. 2020
 */
package autonodeplacement;

/**
 * Simply a direction with ability to get it complementary part. <br>
 * Can be interpreted as: <br>
 * Direction from where an edge is connected to a node. <br>
 * Direction of an arrow pin. <br>
 * Direction in which to move a node and more... <br>
 * @author Jan Juda (xjudaj00)
 */
public enum Direction {
	/** Direction DOWN. Meaning edge is connected to the bottom side of a Node */
	DOWN,
	/** Direction UP. Meaning edge is connected to the upper side of a Node */
	UP,
	/** Direction RIGHT. Meaning edge is connected to the right side of a Node */
	RIGHT,
	/** Direction LEFT. Meaning edge is connected to the left side of a Node */
	LEFT;

	/**
	 * @return complementary direction. For UP returns DOWN, for LEFT returns RIGHT...
	 */
	public Direction complementary() {
		switch (this) {
			case DOWN:
				return UP;
			case LEFT:
				return RIGHT;
			case RIGHT:
				return LEFT;
			case UP:
			default:
				return DOWN;
		}
	}
}