/**
 * Copyright 2020 Jan Juda
 * Created: 4. 3. 2020
 */
package autonodeplacement;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import autonodeplacement.representation.Edge;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import autonodeplacement.representation.Pin;
import autonodeplacement.representation.Rectangle;
import autonodeplacement.representation.SubGraph;
import autonodeplacement.representation.SymbolDefinition;
import javafx.geometry.BoundingBox;

/**
 * Class for generating TinyCad output
 * @author Jan Juda (xjudaj00)
 */
public class OutputGenerator {

	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(OutputGenerator.class.toString());

	/** Pin type none */
	public static final int PIN_NONE = 0;
	/** Pin type arrow */
	public static final int PIN_ARROW = 2;
	/** Subgraphs whose ID starts with this string won't be generated in the output. */
	private static final String NO_SHOW = "NO_SHOW";

	/** User configured border for nodes */
	private final int border;
	/** Current symbolDef ID to use */
	private int symbolDefID = 1;
	/** Index of output pins for a certain node, used while creating output */
	private int outIndex = 1;
	/** Index of output pins for a certain node, used while creating output */
	private int inIndex = 1;

	/**
	 * A constructor
	 * @param nextSymbolDefID ID of next symbol definition
	 */
	public OutputGenerator(int nextSymbolDefID) {
		symbolDefID = nextSymbolDefID;
		border = Preferences.getInstance().getNodeBorder() / 5;
	}

	/**
	 * Generates TinyCad output for given graph and writes it into file specified in preferences
	 * @param graph to generate output for
	 * @param outputFileName name of the output file
	 * @return true whether a generation was successful, false on error
	 */
	public boolean generateOutput(GraphRepresentation graph, String outputFileName) {
		StringBuilder output = new StringBuilder();
		// Here we want to use size BEFORE we shrink the graph
		output.append(generateStart(graph.getSize()));
		// For output, make the graph 5 times smaller.
		graph.resize(0.2);
		// First generate symbol definitions
		List<SubGraph> subgraphs = graph.getSubgraphs().values().stream().filter(x -> !x.getId().startsWith(NO_SHOW)).collect(Collectors.toList());
		for (Node node : subgraphs) {
			output.append(generateSymbolDefinition(node));
		}
		for (Node node : graph.getNodes()) {
			output.append(generateSymbolDefinition(node));
		}
		output.append(TinyCadConstants.OPTIONS);

		// For every node, shift it half size right and down, so that when we generate TinyCad, we still have same
		// distances between nodes
		for (SubGraph subgraph : subgraphs) {
			BoundingBox nodeBox = subgraph.getBoundingBox(Preferences.getInstance().getSubgraphBorder(), true);
			subgraph.shiftNotRecursive(new Point2D.Double(nodeBox.getWidth() / 2, nodeBox.getHeight() / 2));
		}
		for (Node node : graph.getNodes()) {
			BoundingBox nodeBox = node.getBoundingBox(Preferences.getInstance().getNodeBorder(), true);
			node.shift(new Point2D.Double(nodeBox.getWidth() / 2, nodeBox.getHeight() / 2));
		}
		// Make sure the result will fit into TinyCad graph space
		PostProcessing.shiftForOutput(graph);

		// Then generate symbols
		for (Node node : subgraphs) {
			output.append(generateSymbol(node));
		}
		for (Node node : graph.getNodes()) {
			output.append(generateSymbol(node));
		}
		// Now generate all wires
		for (Edge edge : graph.getEdges()) {
			output.append(generateWires(edge, Preferences.getInstance().getOrthogonalOnly()));
		}
		output.append(generateEnd());
		File outputFile = new File(outputFileName);
		File parent = outputFile.getParentFile();
		if (parent != null) {
			parent.mkdirs();
		}
		try (FileWriter writer = new FileWriter(outputFileName, false)) {
			writer.write(output.toString());
		} catch (IOException e) {
			LOGGER.severe("Error occured during writing to output file: " + e.getLocalizedMessage());
			return false;
		}
		return true;
	}

	/**
	 * @param edge
	 * @param orthogonalOnly if edges must be orthogonal
	 * @return generates all wire/s for given edge
	 */
	private static String generateWires(Edge edge, boolean orthogonalOnly) {
		Pin startPin = edge.getStartPin();
		Pin endPin = edge.getEndPin();
		if (startPin == null || endPin == null) {
			LOGGER.warning(MessageFormat.format("No {0} pin found for edge {1}. Wires for this edge won''t be generated. This is output generator error.",
					startPin == null ? "start" : "end", edge.getId()));
			return "";
		}
		Point2D startPos = startPin.getRealPosition();
		Point2D endPos = endPin.getRealPosition();
		if(orthogonalOnly) {
			LOGGER.severe("Orthogonal edges not supported yet.");
		}
		return MessageFormat.format(TinyCadConstants.WIRE, formatDouble(startPos.getX()), formatDouble(startPos.getY()), formatDouble(endPos.getX()),
				formatDouble(endPos.getY()));
	}

	/**
	 * @param node
	 * @return generated symbol for given node
	 */
	private static String generateSymbol(Node node) {
		SymbolDefinition symbolDef = node.getSymbolDefinition();
		if (symbolDef == null) {
			LOGGER.severe(MessageFormat.format(
					"No symbol definition found for node {0}. This probably means error in output generator code. This will lead to errors in output file.",
					node.getId()));
			return "";
		}
		double width = node.getSize().getX();
		double height = node.getSize().getY();
		Point2D namePosition = node.getNamePosition();
		double namePosX = namePosition != null ? namePosition.getX() : width * -0.85;
		double namePosY = namePosition != null ? namePosition.getY() : height * -0.2;
		return MessageFormat.format(TinyCadConstants.SYMBOL, formatInteger(symbolDef.getId()), formatDouble(node.getPosition().getX()),
				formatDouble(node.getPosition().getY()),
				node.getId(), formatDouble(namePosX), formatDouble(namePosY));
	}

	/**
	 * @param node
	 * @return generated symbol definition for given node
	 */
	private String generateSymbolDefinition(Node node) {
		SymbolDefinition symbolDef = node.getSymbolDefinition();
		if (symbolDef == null) {
			symbolDef = new SymbolDefinition(symbolDefID, node);
			symbolDef.addRectangle(new Rectangle(new Point2D.Double(0, 0), new Point2D.Double(node.getSize().getX(), node.getSize().getY()),
					node instanceof SubGraph));
			symbolDefID++;
		}
		generatePinsFor(node);
		return symbolDef.toString();
	}

	/**
	 * Generates pins for a node and add them to its symbol definition
	 * @param node
	 */
	private void generatePinsFor(Node node) {
		// At first, sort all node edges into 4 groups, depending on what side of a node they will be connected
		List<Edge> topEdges = getEdgesInDirection(node, Direction.UP);
		List<Edge> bottomEdges = getEdgesInDirection(node, Direction.DOWN);
		List<Edge> leftEdges = getEdgesInDirection(node, Direction.LEFT);
		List<Edge> rightEdges = getEdgesInDirection(node, Direction.RIGHT);

		outIndex = 1;
		inIndex = 1;
		SymbolDefinition symbolDef = node.getSymbolDefinition();
		List<Pin> pins = new ArrayList<>(symbolDef.getPins()); // Create copy of the collection, so we can remove nodes
		List<Pin> topPins = distributePinsIntoDirections(node, pins, Direction.UP);
		List<Pin> bottomPins = distributePinsIntoDirections(node, pins, Direction.DOWN);
		List<Pin> leftPins = distributePinsIntoDirections(node, pins, Direction.LEFT);
		List<Pin> rightPins = distributePinsIntoDirections(node, pins, Direction.RIGHT);
		// Match pins and edges with corresponding directions
		matchPinsAndEdges(node, topEdges, topPins, pins);
		matchPinsAndEdges(node, bottomEdges, bottomPins, pins);
		matchPinsAndEdges(node, leftEdges, leftPins, pins);
		matchPinsAndEdges(node, rightEdges, rightPins, pins);
		// Just match edges with any pin with equal flow direction
		matchPinsAndEdges(node, topEdges, pins, pins);
		matchPinsAndEdges(node, bottomEdges, pins, pins);
		matchPinsAndEdges(node, leftEdges, pins, pins);
		matchPinsAndEdges(node, rightEdges, pins, pins);
		// Either all pins or all edges should be matched now...
		// If there some left unmatched edges, just generate pins for them
		generatePinsInDirection(node, topEdges, Direction.UP);
		generatePinsInDirection(node, bottomEdges, Direction.DOWN);
		generatePinsInDirection(node, leftEdges, Direction.LEFT);
		generatePinsInDirection(node, rightEdges, Direction.RIGHT);
	}

	/**
	 * Matches pins with edges depending on equal flow direction of the pin and the edge
	 * @param node used to get flow direction of edge (output/input)
	 * @param edges list of edges to match, matched edges are removes from this list, leaving only unmatched edges
	 * @param pins list of pins to match, matched pins are removed from this list, leaving only unmatched pins
	 * @param allPins matched pins are removed from this collection, leaving only unmatched pins, can be same as pins
	 */
	private static void matchPinsAndEdges(Node node, List<Edge> edges, List<Pin> pins, List<Pin> allPins) {
		// Iterate over new arraylist, so we can safely remove from argument lists
		for (Edge edge : new ArrayList<>(edges)) {
			List<Pin> currentPinsList = new ArrayList<>(pins);
			for (Pin pin : currentPinsList) {
				if (edge.getStartNode().equals(node) == pin.isOutput()) {
					if (pin.isOutput()) {
						edge.setStartPin(pin);
					} else {
						edge.setEndPin(pin);
					}
					edges.remove(edge);
					pins.remove(pin);
					allPins.remove(pin);
					break;
				}
			}
		}
	}

	/**
	 * @param node
	 * @param pins of the node
	 * @param direction
	 * @return pins of the given node in given direction
	 */
	private static List<Pin> distributePinsIntoDirections(Node node, List<Pin> pins, Direction direction) {
		double referenceX = node.getSize().getX() / 2;
		double referenceY = node.getSize().getY() / 2;
		List<Pin> pinsInDirection = new ArrayList<>();
		for (Pin pin : pins) {
			// How far from middle as percentage of size
			// X = pin position
			// Left   Middle   Right = xPercentage
			//  |        X        |  = 0
			//  X        |        |  = -1.0
			//  |        |        X  = 1.0
			//  |        |   X    |  = 0.5
			double xPercentage = (pin.getX() - referenceX) / referenceX;
			double yPercentage = (pin.getY() - referenceY) / referenceY;
			Direction pinSide;
			if (Math.abs(xPercentage) > Math.abs(yPercentage)) {
				pinSide = xPercentage < 0 ? Direction.LEFT : Direction.RIGHT;
			} else {
				pinSide = yPercentage < 0 ? Direction.UP : Direction.DOWN;
			}
			if (pinSide == direction) {
				pinsInDirection.add(pin);
			}
		}
		return orderPinsByDirection(pinsInDirection, direction);
	}

	/**
	 * @param pins
	 * @param direction
	 * @return orders pin by realPositionCoord for given direction. So if direction is UP or DOWN, then pins are sorted by X-coord.
	 */
	private static List<Pin> orderPinsByDirection(List<Pin> pins, Direction direction) {
		if (direction == Direction.UP || direction == Direction.DOWN) {
			return pins.stream().sorted((x, y) -> (int) (x.getRealPosition().getX() - y.getRealPosition().getX())).collect(Collectors.toList());
		} else {
			return pins.stream().sorted((x, y) -> (int) (x.getRealPosition().getY() - y.getRealPosition().getY())).collect(Collectors.toList());
		}
	}

	/**
	 * Generates Pins for a side of the given node, that matches the given direction (UP = upper side..)
	 * @param node
	 * @param edges to create pins for, sorted by other end
	 * @param direction
	 */
	private void generatePinsInDirection(Node node, List<Edge> edges, Direction direction) {
		SymbolDefinition symbolDef = node.getSymbolDefinition();
		int numberOfEdges = edges.size();
		if (numberOfEdges == 0) {
			return;
		}
		double x;
		double y;
		double stepX;
		double stepY;
		if (direction.ordinal() <= Direction.UP.ordinal()) { // Top or bottom
			y = (direction == Direction.DOWN) ? node.getSize().getY() : 0; // DOWN height, UP 0
			stepY = 0;
			stepX = node.getSize().getX() / (numberOfEdges + 1);
			x = stepX;
		} else {
			x = (direction == Direction.RIGHT) ? node.getSize().getX() : 0; // RIGHT width, LEFT 0
			stepX = 0;
			stepY = node.getSize().getY() / (numberOfEdges + 1);
			y = stepY;
		}
		for (Edge edge : edges) {
			// true if this is output edge for a node. In other words this node is startNode of the edge
			boolean out = node.getOutputEdges().contains(edge);
			int type = PIN_NONE;
			if (!out && Preferences.getInstance().getArrowsAllowed()) {
				type = PIN_ARROW;
			}
			int directionOfPin = getEdgeDirection(edge, node).ordinal();
			String flow = out ? "OUT"+outIndex : "IN"+inIndex;
			Pin pin = new Pin(x, y, type, directionOfPin, flow, symbolDef);
			if (out) {
				outIndex++;
				edge.setStartPin(pin);
			} else {
				inIndex++;
				edge.setEndPin(pin);
			}
			x += stepX;
			y += stepY;
		}
	}

	/**
	 * @param node
	 * @param edges
	 * @param direction
	 * @return sorted list of edges. Edges are sorted by position (x or y) orthogonal to given direction
	 * 		   of the other end of an edge than the given node.
	 */
	private static List<Edge> sortEdgesByOtherEnd(Node node, List<Edge> edges, Direction direction){
		return edges.stream().sorted((firstEdge, secondEdge) -> {
			Point2D position;
			if (node.getOutputEdges().contains(firstEdge)) { // node is start of firstEdge, so we want position of end node
				position = firstEdge.getEndNode().getPosition();
			} else { // node is end of firstEdge, so we want position of start node
				position = firstEdge.getStartNode().getPosition();
			}
			// We have a position of the right node, so we take as comparison value X or Y from it depending on given direction
			double firstEdgeValue = (direction.ordinal() <= Direction.UP.ordinal()) ? position.getX() : position.getY();
			// And we do the same for the other edge
			if (node.getOutputEdges().contains(secondEdge)) { // node is start of secondEdge, so we want position of end node
				position = secondEdge.getEndNode().getPosition();
			} else { // node is end of secondEdge, so we want position of start node
				position = secondEdge.getStartNode().getPosition();
			}
			double secondEdgeValue = (direction.ordinal() <= Direction.UP.ordinal()) ? position.getX() : position.getY();
			// Now just compare the two values and convert result to int with precision to 3 decimal points (should be enough)
			return (int) (1000 * (firstEdgeValue - secondEdgeValue));
		}).collect(Collectors.toList());
	}

	/**
	 * @param node
	 * @param direction
	 * @return list of all edges that contains given node and are connected to it in given direction
	 */
	private List<Edge> getEdgesInDirection(Node node, Direction direction) {
		List<Edge> edges = new ArrayList<>();
		List<Edge> nodeEdges = new ArrayList<>(node.getInputEdges());
		nodeEdges.addAll(node.getOutputEdges());
		for (Edge edge : nodeEdges) {
			if (getEdgeDirection(edge, node) == direction) {
				edges.add(edge);
			}
		}
		return sortEdgesByOtherEnd(node, edges, direction);
	}

	/**
	 * @param edge
	 * @param node either start or end node of the edge
	 * @return Direction in which is the edge connected to the given node.
	 */
	private Direction getEdgeDirection(Edge edge, Node node) {
		if (!edge.contains(node)) {
			LOGGER.warning(MessageFormat.format("Edge {0} doesn''t contain Node {1}! Not possible to determine edge direction for this node!",
					edge.getId(), node.getId()));
			return Direction.DOWN;
		}
		Node theOther = edge.getStartNode() == node ? edge.getEndNode() : edge.getStartNode();
		Point2D nodePos = node.getPosition();
		Point2D theOtherPos = theOther.getPosition();
		// Difference of node centers
		double xDiffCenter = nodePos.getX() - theOtherPos.getX();
		double yDiffCenter = nodePos.getY() - theOtherPos.getY();
		Direction directionX = xDiffCenter < 0 ? Direction.RIGHT : Direction.LEFT;
		Direction directionY = yDiffCenter < 0 ? Direction.DOWN : Direction.UP;
		BoundingBox nodeBox = node.getBoundingBox(border, false);
		BoundingBox theOtherBox = theOther.getBoundingBox(border, false);
		// Difference of node sides
		double xDiffSide = directionX == Direction.RIGHT ? nodeBox.getMaxX() - theOtherBox.getMinX() :
			nodeBox.getMinX() - theOtherBox.getMaxX();
		double yDiffSide = directionY == Direction.DOWN ? nodeBox.getMaxY() - theOtherBox.getMinY() :
			nodeBox.getMinY() - theOtherBox.getMaxY();
		if (Math.signum(xDiffSide) == Math.signum(xDiffCenter) && Math.signum(yDiffSide) == Math.signum(yDiffCenter)) {
			// The case when the distance in both axis is bigger than sum of half size of nodes in that axis
			// ███ = first node, ▓▓▓ = second node
			// ---------
			// ███
			//
			//      ▓▓▓
			// Don't consider creating X-Y edge here.
			return Math.abs(xDiffSide) > Math.abs(yDiffSide) ? directionX : directionY;
		} else if (Math.signum(xDiffSide) != Math.signum(xDiffCenter)) {
			// The case when the distance in X axis is smaller than sum of half width of nodes
			// Doesn't make sense to draw edge from left side to right side or from right side to left side
			// ███ = first node, ▓▓▓ = second node
			// -------
			// ███
			//
			//   ▓▓▓
			return directionY;
		} else {
			// The opposite case to the previous. The distance in Y axis is smaller than sum of half height of nodes
			// Doesn't make sense to draw edge from upper side to bottom side or from bottom side to upper side
			// ██ = first node, ▓▓ = second node
			// -------
			// ██
			// ██ ▓▓
			//    ▓▓
			return directionX;
		}
	}

	/**
	 * @param number
	 * @return double as string formated with dot and 5 decimals
	 */
	public static String formatDouble(double number) {
		NumberFormat decimalFormat = NumberFormat.getInstance(Locale.US);
		decimalFormat.setGroupingUsed(false);
		decimalFormat.setMinimumFractionDigits(5);
		return decimalFormat.format(number);
	}

	/**
	 * @param number
	 * @return integer as string formated without any separators
	 */
	public static String formatInteger(int number) {
		NumberFormat format = new DecimalFormat("#0");
		return format.format(number);
	}

	/**
	 * @param graphSize for generating details
	 * @return generates and returns beginning of the output TinyCad file
	 */
	private static String generateStart(Dimension graphSize) {
		StringBuilder output = new StringBuilder();
		output.append(TinyCadConstants.XML_START);
		output.append(TinyCadConstants.GENERATED_BY);
		output.append(TinyCadConstants.TINYCAD_SHEETS_START);
		output.append(MessageFormat.format(TinyCadConstants.DETAILS, formatInteger(graphSize.width), formatInteger(graphSize.height)));
		output.append(TinyCadConstants.STYLE_DEFAULT);
		return output.toString();
	}

	/**
	 * @return generates and returns end of the output TinyCad file
	 */
	private static String generateEnd() {
		StringBuilder output = new StringBuilder();
		output.append(TinyCadConstants.TINYCAD_SHEETS_END);
		return output.toString();
	}

}
