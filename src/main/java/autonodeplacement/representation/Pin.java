/**
 * Copyright 2020 Jan Juda
 * Created: 11. 3. 2020
 */
package autonodeplacement.representation;

import static autonodeplacement.OutputGenerator.*;

import java.awt.geom.Point2D;
import java.text.MessageFormat;

import autonodeplacement.TinyCadConstants;

/**
 * Class for holding information of a pin for symbol definition
 * @author Jan Juda (xjudaj00)
 */
public class Pin {
	/** X-coord of relative position to the symbol definition */
	private double x;
	/** Y-coord of relative position to the symbol definition */
	private double y;
	/** Type of the Pin */
	private int type;
	/** Direction of the Pin (only matters if type is ARROW) */
	private int direction;
	/** Flow direction of a pin. IN or OUT string with number */
	private String flow;
	/** Symbol definition to which this pins belongs to */
	private SymbolDefinition parentSymbolDef;

	/**
	 * Constructor of a Pin
	 * @param x
	 * @param y
	 * @param type
	 * @param direction
	 * @param flow
	 * @param parentSymbolDef
	 */
	public Pin(double x, double y, int type, int direction, String flow, SymbolDefinition parentSymbolDef) {
		this.x = x;
		this.y = y;
		this.type = type;
		this.direction = direction;
		this.flow = flow;
		setSymbolDefinition(parentSymbolDef);
	}

	/**
	 * @return real absolute position in graph space for this pin. Useful for creating edges.
	 */
	public Point2D getRealPosition() {
		Node node = parentSymbolDef.getParentNode();
		return new Point2D.Double(node.getPosition().getX() - (node.getSize().getX() - x), node.getPosition().getY() - (node.getSize().getY() - y));

	}

	/**
	 * @param newSymbolDef
	 * @return copy of this pin for new symbol definition
	 */
	public Pin getCopy(SymbolDefinition newSymbolDef) {
		return new Pin(x, y, type, direction, flow, newSymbolDef);
	}

	/**
	 * Sets parent symbol definition of this Pin
	 * @param parentSymbolDef
	 */
	public void setSymbolDefinition(SymbolDefinition parentSymbolDef) {
		this.parentSymbolDef = parentSymbolDef;
		if (parentSymbolDef != null) {
			parentSymbolDef.addPin(this);
		}
	}

	/**
	 * @return X-coord of relative position to the symbol definition
	 */
	public double getX() {
		return x;
	}

	/**
	 * @return Y-coord of relative position to the symbol definition
	 */
	public double getY() {
		return y;
	}

	/**
	 * @return true, whether this is an output pin, false otherwise
	 */
	public boolean isOutput() {
		return flow.toUpperCase().contains("OUT");
	}

	@Override
	public String toString() {
		return MessageFormat.format(TinyCadConstants.PIN, formatDouble(x), formatDouble(y), formatInteger(type), formatInteger(direction), flow);
	}
}
