/**
 * Copyright 2020 Jan Juda
 * Created: 15. 3. 2020
 */
package autonodeplacement.representation;


import static autonodeplacement.OutputGenerator.formatDouble;

import java.awt.Shape;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import autonodeplacement.TinyCadConstants;

/**
 * Class representing Polygon for Symbol definition
 * @author Jan Juda (xjudaj00)
 */
public class Polygon {
	/** Position of this polygon */
	Point2D position;
	/** List of all points from which is this polygon made */
	List<Point2D> points = new ArrayList<>();

	/**
	 * Constructs a polygon with given position and empty list of points
	 * @param position
	 */
	public Polygon(Point2D position) {
		this.position = position;
	}

	/**
	 * Adds new point to this polygon
	 * @param point
	 */
	public void addPoint(Point2D point) {
		points.add(point);
	}

	/**
	 * @return copy of this rectangle
	 */
	public Polygon getCopy() {
		Polygon newPolygon = new Polygon(position);
		points.forEach(newPolygon::addPoint);
		return newPolygon;
	}

	/**
	 * @return this rectangle as Shape for graph view, no offset
	 */
	Shape getShape() {
		Path2D path = new Path2D.Double();
		double[] xcoords = points.stream().mapToDouble(x -> x.getX() * 5).toArray();
		double[] ycoords = points.stream().mapToDouble(x -> x.getY() * 5).toArray();
		path.moveTo(xcoords[0], ycoords[0]);
		for (int i = 1; i < ycoords.length; ++i) {
			path.lineTo(xcoords[i], ycoords[i]);
		}
		path.closePath();
		return path;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(MessageFormat.format(TinyCadConstants.POLYGON_START, formatDouble(position.getX()), formatDouble(position.getY())));
		for (Point2D point : points) {
			result.append(MessageFormat.format(TinyCadConstants.POINT, formatDouble(point.getX()), formatDouble(point.getY())));
		}
		result.append(TinyCadConstants.POLYGON_END);
		return result.toString();
	}
}
