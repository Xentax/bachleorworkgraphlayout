/**
 * Copyright 2020 Jan Juda
 * Created: 11. 3. 2020
 */
package autonodeplacement.representation;

import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import autonodeplacement.OutputGenerator;
import autonodeplacement.TinyCadConstants;

/**
 * Class for generating symbol definition for a node. <br>
 * Symbol definition is describing how the node looks like, but not it's position.
 * @author Jan Juda (xjudaj00)
 */
public class SymbolDefinition {
	/** Id of this symbol definition, should */
	private int id;
	/** Node that this symbol definition belongs to */
	private Node parentNode;
	/** List of all pins in this symbol definition */
	private List<Pin> pins = new ArrayList<>();
	/** List of all rectangles in this symbol definition */
	private List<Rectangle> rectangles = new ArrayList<>();
	/** List of all polygons in this symbol definition */
	private List<Polygon> polygons = new ArrayList<>();
	/** Name of this symbol definition */
	String name;
	/** Description of this symbol definition */
	String description;

	/**
	 * Constructs symbol definition for parent node
	 * @param id of new symbol definition, all IDs must be unique
	 * @param parentNode
	 */
	public SymbolDefinition(int id, Node parentNode) {
		this(id, parentNode, TinyCadConstants.GENERIC_SYMBOL_DEF + parentNode.getId());
	}

	/**
	 * Constructs symbol definition for parent node with given name
	 * @param id of new symbol definition, all IDs must be unique
	 * @param parentNode
	 * @param name
	 */
	public SymbolDefinition(int id, Node parentNode, String name) {
		this(id, parentNode, name, name);
	}

	/**
	 * Constructs symbol definition for parent node with given name and description
	 * @param id of new symbol definition, all IDs must be unique
	 * @param parentNode
	 * @param name
	 * @param description
	 */
	public SymbolDefinition(int id, Node parentNode, String name, String description) {
		this.id = id;
		this.parentNode = parentNode;
		this.name = name;
		this.description = description;
		parentNode.setSymbolDefinition(this);
	}

	/**
	 * Adds new pin to this symbol definition
	 * @param pin
	 */
	public void addPin(Pin pin) {
		pins.add(pin);
	}

	/**
	 * Adds new rectangle to this symbol definition
	 * @param rectangle
	 */
	public void addRectangle(Rectangle rectangle) {
		rectangles.add(rectangle);
	}

	/**
	 * Adds new polygon to this symbol definition
	 * @param polygon
	 */
	public void addPolygon(Polygon polygon) {
		polygons.add(polygon);
	}

	/**
	 * @return id of this symbol definition
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return parent node for this symbol definition
	 */
	public Node getParentNode() {
		return parentNode;
	}

	/**
	 * @return list of all pins in this symbol definition
	 */
	public List<Pin> getPins() {
		return pins;
	}

	/**
	 * @return list of all polygons in this symbol definition
	 */
	public List<Polygon> getPolygons() {
		return polygons;
	}

	/**
	 * @return list of all rectangles in this symbol definition
	 */
	public List<Rectangle> getRectangles() {
		return rectangles;
	}

	@Override
	public String toString() {
		StringBuilder definition = new StringBuilder();
		String width = OutputGenerator.formatDouble(parentNode.getSize().getX());
		String height = OutputGenerator.formatDouble(parentNode.getSize().getY());
		definition.append(MessageFormat.format(TinyCadConstants.SYMBOL_DEF_START, OutputGenerator.formatInteger(id),
				name, description, width, height));
		rectangles.forEach(x -> definition.append(x.toString()));
		polygons.forEach(x -> definition.append(x.toString()));
		pins.forEach(x -> definition.append(x.toString()));
		definition.append(TinyCadConstants.SYMBOL_DEF_END);
		return definition.toString();
	}

	/**
	 * @param newNode
	 * @return deep copy of this symbol definition for new node
	 */
	public SymbolDefinition getCopy(Node newNode) {
		SymbolDefinition newSymbolDef = new SymbolDefinition(id, newNode);
		for (Pin pin : pins) {
			pin.getCopy(newSymbolDef);
		}
		polygons.forEach(newSymbolDef::addPolygon);
		rectangles.forEach(newSymbolDef::addRectangle);
		return newSymbolDef;
	}

	/**
	 * @return this symbol definition as shape Area for graph view, offset = -1 * half a it's size
	 */
	public Area getShape() {
		double offsetX = parentNode.getSize().getX() / -2;
		double offsetY = parentNode.getSize().getY() / -2;
		// add minimal 1x1px rectangle, so all nodes are correctly displayed if symbol definition has no rectangles or polygons
		Area area = new Area(new Rectangle2D.Double(-1 * offsetX - 0.5, -1 * offsetY - 0.5, 1, 1));
		rectangles.forEach(x -> area.add(new Area(x.getShape())));
		polygons.forEach(x -> area.add(new Area(x.getShape())));
		// Shapes were created without offset, so we need to offset it now to the center
		area.transform(AffineTransform.getTranslateInstance(offsetX, offsetY));
		return area;
	}
}
