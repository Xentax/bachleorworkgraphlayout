/**
 * Copyright 2020 Jan Juda
 * Created: 15. 2. 2020
 */
package autonodeplacement.representation;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import autonodeplacement.Application;
import autonodeplacement.algorithms.Algorithms;
import autonodeplacement.representation.Node.Location;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.AbstractVertexShapeTransformer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;
import javafx.geometry.BoundingBox;

/**
 * Class for internal representation of graph. <br>
 * It should contain all the required information for whole node placement.
 * @author Jan Juda (xjudaj00)
 */

public class GraphRepresentation {

	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(GraphRepresentation.class.toString());

	/** Default graph size */
	public static final int DEFAULT_SIZE = 600;

	/** Border width for graph layout */
	public static final int BORDER_WIDTH = 0;
	/** Max window width */
	public static final int MAX_WINDOW_WIDTH = 1700;
	/** Max window height */
	public static final int MAX_WINDOW_HEIGHT = 1000;

	/** All nodes in this diagram. Key = path of that node, Value = Node */
	private Map<String, Node> nodes;

	/** All edges in this diagram. Key = ID, Value = Edge */
	private Map<String, Edge> edges;

	/** Size of the graph */
	private Dimension size;

	/** Holds all subgraphs in this graph. Key = ID, Value = SubGraph */
	private Map<String, SubGraph> subgraphs;

	/** The root/main subgraph. Holds entire graph. */
	private SubGraph rootGraph;

	/** Holds saved graph score */
	private long savedScore = Long.MAX_VALUE;

	/** Algorithm used to lay out this graph. */
	private Algorithms laidOutBy = null;

	/**
	 * Creates EMPTY graph. <br>
	 * Use as DUMMY object or for TESTS ONLY!!! <br>
	 * For storing actual graphs use {@link GraphRepresentation#GraphRepresentation(Map, Map, Map, int, int)}!.
	 */
	public GraphRepresentation() {
		this.nodes = new LinkedHashMap<>();
		this.edges = new LinkedHashMap<>();
		this.subgraphs = new LinkedHashMap<>();
		this.rootGraph = null;
		setSize(0, 0);
	}

	/**
	 * Constructor for full graph representation
	 * @param nodes
	 * @param edges
	 * @param subgraphs map of all subgraphs in this graph. Must contain rootGraph (subgraph with ID Node.ROOT_ID - empty string "")
	 * @param width of the graph
	 * @param height of the graph
	 */
	public GraphRepresentation(Map<String, Node> nodes, Map<String, Edge> edges, Map<String, SubGraph> subgraphs, int width, int height) {
		this.nodes = nodes;
		this.edges = edges;
		this.subgraphs = subgraphs;
		this.rootGraph = subgraphs.get(Node.ROOT_ID);
		if (rootGraph == null) {
			LOGGER.severe("Error while creating GraphRepresentation. Root sub-graph not present.");
		}
		setSize(width, height);
	}

	/**
	 * @return list of graph nodes
	 */
	public Collection<Node> getNodes() {
		return nodes.values();
	}

	/**
	 * @return list of graph edges
	 */
	public Collection<Edge> getEdges() {
		return edges.values();
	}

	/**
	 * @param nodePath path of searched node
	 * @return node with given path or null if no node with given path found in this graph
	 */
	public Node getNode(String nodePath) {
		return nodes.get(nodePath);
	}

	/**
	 * @param nodeId
	 * @return node with given ID or null, if no node found or the ID can't uniquely identify the node
	 */
	public Node getNodeByID(String nodeId) {
		if (nodeId.contains(".")) {
			return getNode(nodeId);
		} else {
			List<Node> filtered = nodes.values().stream().filter(x -> x.getId().equals(nodeId)).collect(Collectors.toList());
			if (filtered.size() == 1) {
				return filtered.get(0);
			} else if (filtered.size() >= 2) {
				return nodes.get(nodeId); // there might be 'A' and 'SUB.A'
			}
			return null;
		}
	}

	/**
	 * Opens window with this graph.
	 * @param graph to show
	 * @param windowName additional name of shown window/frame
	 */
	public static void debugView(GraphRepresentation graph, String windowName) {
		debugView(graph.createCopy().getStaticLayout(), windowName);
	}

	/**
	 * Opens window with this layout of a graph.
	 * @param layout to show, generally use {@link getStaticLayout}
	 * @param windowName additional name of shown window/frame
	 */
	public static void debugView(Layout<Node, Edge> layout, String windowName) {
		VisualizationViewer<Node, Edge> visual = new VisualizationViewer<>(layout);
		visual.setPreferredSize(getLayoutSize(layout));
		visual.getRenderContext().setVertexLabelTransformer(Node::getId);
		visual.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<>());
		visual.getRenderContext().setVertexShapeTransformer(new NodeToShapeTransformer());
		visual.getRenderContext().setVertexFillPaintTransformer(node -> node instanceof SubGraph ? new Color(0, 0, 0, 0) : Color.RED.brighter());
		visual.getRenderContext().setVertexDrawPaintTransformer(node -> Color.BLACK);
		float[] dash = { 10.0f };
		visual.getRenderContext().setVertexStrokeTransformer(node -> node instanceof SubGraph ?
				new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1, dash, 0) :
				new BasicStroke());
		visual.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);
		String name = Application.NAME + " - Graph View";
		if (!windowName.isEmpty()) {
			name += ": " + windowName;
		}
		DefaultModalGraphMouse<Node, Edge> mouse = new DefaultModalGraphMouse<>();
		mouse.setMode(ModalGraphMouse.Mode.TRANSFORMING);
		visual.setGraphMouse(mouse);
		JFrame frame = new JFrame(name);
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.getContentPane().add(visual);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/**
	 * @param layout
	 * @return size of layout limited by max window size
	 */
	public static Dimension getLayoutSize(Layout<Node, Edge> layout) {
		double width = Math.min(layout.getSize().width + BORDER_WIDTH * 2, MAX_WINDOW_WIDTH);
		double height = Math.min(layout.getSize().height + BORDER_WIDTH * 2, MAX_WINDOW_HEIGHT);
		return new Dimension((int) width, (int) height);
	}

	/**
	 * @return graph compatible with JUNG library, with all subgraphs and not modified edges
	 */
	public Graph<Node, Edge> toJUNGGraph() {
		List<Node> allSubgraphs = new ArrayList<>();
		allSubgraphs.addAll(getSubgraphs().values());
		return toJUNGGraph(allSubgraphs, false);
	}

	/**
	 * @param allowedNodes list of subgraphs to be added to the graph as vertexes
	 * @param modifyEdges if true, then edges are modified so that they contain subgraphs from allowedNodes list. If false, not modified edges are passed to the result.
	 * @return graph compatible with JUNG library
	 */
	public Graph<Node, Edge> toJUNGGraph(Collection<Node> allowedNodes, boolean modifyEdges) {
		Graph<Node, Edge> graph = new DirectedSparseMultigraph<>();
		// Always add all nodes
		this.getNodes().forEach(graph::addVertex);
		// Only add subgraphs from allowed nodes
		List<SubGraph> allowedSubgraphs = allowedNodes.stream().filter(node -> node instanceof SubGraph).map(x -> (SubGraph) x).collect(Collectors.toList());
		allowedSubgraphs.forEach(graph::addVertex);
		if (modifyEdges) {
			// Copy the edges and change them, so the edges contain selected subgraphs instead of their child nodes
			for (Edge edge : this.getEdges()) {
				Node start = edge.getStartNode();
				Node end = edge.getEndNode();
				for (SubGraph subgraph : allowedSubgraphs) {
					if (subgraph.contains(start.toString())) {
						start = subgraph;
					}
					if (subgraph.contains(end.toString())) {
						end = subgraph;
					}
				}
				graph.addEdge(edge, start, end);
			}
		} else {
			this.getEdges().forEach(edge -> graph.addEdge(edge, edge.getStartNode(), edge.getEndNode()));
		}
		return graph;
	}

	/**
	 * @return static layout for debugView
	 */
	public StaticLayout<Node, Edge> getStaticLayout() {
		StaticLayout<Node, Edge> layout = new StaticLayout<>(toJUNGGraph(), Node::getPosition);
		layout.setSize(size);
		return layout;
	}

	/**
	 * @return size of the graph
	 */
	public Dimension getSize() {
		return size;
	}

	/**
	 * Set size of the graph with x and y
	 * @param x width
	 * @param y height
	 */
	public void setSize(int x, int y) {
		size = new Dimension(x, y);
		if (rootGraph != null) {
			rootGraph.setPosition(x / 2.0, y / 2.0);
			rootGraph.setSize(x, y);
		}
	}

	/**
	 * Set size of the graph with dimension size
	 * @param size graph size
	 */
	public void setSize(Dimension size) {
		this.size = size;
		if (rootGraph != null) {
			rootGraph.setPosition(size.getWidth() / 2, size.getHeight() / 2);
			rootGraph.setSize(size.getWidth(), size.getHeight());
		}
	}

	/**
	 * Resizes entire graph by given aspect
	 * @param aspect
	 */
	public void resize(double aspect) {
		getRootGraph().resize(aspect);
	}

	/**
	 * @return deep copy of this graph representation. Useful for viewing purposes. Edge pins are not copied!
	 */
	public GraphRepresentation createCopy() {
		Map<String, Node> newNodes = new LinkedHashMap<>();
		Map<String, Edge> newEdges = new LinkedHashMap<>();
		Map<String, SubGraph> newSubgraphs = new LinkedHashMap<>();
		// Copy nodes
		for (Node node : getNodes()) {
			Node newNode = new Node(node.getId(), node.getSize().getX(), node.getSize().getY(), node.getPosition().getX(), node.getPosition().getY());
			newNode.setPreferredLocation(node.getPreferredLocation());
			SymbolDefinition symbolDef = node.getSymbolDefinition();
			if (symbolDef != null) {
				symbolDef.getCopy(newNode);
			}
			newNodes.put(node.toString(), newNode);
		}
		// Copy subgraphs
		for (SubGraph subgraph : subgraphs.values()) {
			SubGraph newSubgraph = new SubGraph(subgraph.getId(), subgraph.getSize().getX(), subgraph.getSize().getY(), subgraph.getPosition().getX(),
					subgraph.getPosition().getY());
			newSubgraphs.put(subgraph.toString(), newSubgraph);
		}
		// Copy subgraphs hierarchy and parts
		for (SubGraph subgraph : subgraphs.values()) {
			SubGraph newSubgraph = newSubgraphs.get(subgraph.toString());
			for (Node child : subgraph.getChildren()) {
				Node newChild;
				if (child instanceof SubGraph) {
					newChild = newSubgraphs.get(child.toString());
				} else {
					newChild = newNodes.get(child.toString());
				}
				newChild.setParent(newSubgraph);
			}
			Map<BoundingBox, List<Node>> newSubgraphParts = new LinkedHashMap<>();
			for (Entry<BoundingBox, List<Node>> entry : subgraph.getSubgraphParts().entrySet()) {
				newSubgraphParts.put(entry.getKey(), entry.getValue().stream()
						.map(x -> x instanceof SubGraph ? newSubgraphs.get(x.toString()) : newNodes.get(x.toString())).collect(Collectors.toList()));
			}
			newSubgraph.setSubgraphParts(newSubgraphParts);
		}
		// Copy edges
		for (Edge edge : getEdges()) {
			Edge newEdge = new Edge(edge.getId(), newNodes.get(edge.getStartNode().toString()), newNodes.get(edge.getEndNode().toString()));
			newEdges.put(newEdge.getId(), newEdge);
		}
		GraphRepresentation copy = new GraphRepresentation(newNodes, newEdges, newSubgraphs, size.width, size.height);
		copy.laidOutBy = laidOutBy;
		copy.savedScore = savedScore;
		return copy;
	}

	/**
	 * @return true, if there are any positional nodes in the graph
	 */
	public boolean hasPositionalNodes() {
		for (Node node : getNodes()) {
			if (node.getPreferredLocation() != Location.NONE) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return root subgraph of this graph
	 */
	public SubGraph getRootGraph() {
		return rootGraph;
	}

	/**
	 * @return return map of all subgraphs. Key is path, value is subgraph.
	 */
	public Map<String, SubGraph> getSubgraphs() {
		return subgraphs;
	}

	/**
	 * @return saved score or Long.MAX_VALUE if no score was saved
	 */
	public long getSavedScore() {
		return savedScore;
	}

	/**
	 * Saves score so it doesn't have to be evaluated all over again
	 * @param score to save
	 */
	public void setSavedScore(long score) {
		this.savedScore = score;
	}

	/**
	 * @return Algorithm which laid out this graph
	 */
	public Algorithms getLaidOutBy() {
		return laidOutBy;
	}

	/**
	 * @param laidOutBy Algorithm used to laid out this graph
	 */
	public void setLaidOutBy(Algorithms laidOutBy) {
		this.laidOutBy = laidOutBy;
	}



	/**
	 * Default node shape transformer for debug view
	 * @author Jan Juda (xjudaj00)
	 */
	static class NodeToShapeTransformer extends AbstractVertexShapeTransformer<Node> {

		/**
		 * Utility method, calculates aspect ratio of a node
		 * @param node
		 * @return aspect ration of a node
		 */
		public static float getNodeAspectRatio(Node node) {
			Point2D size = node.getSize();
			if (size.getX() == 0.0) {
				return (float) 1.0;
			}
			return (float) (size.getY() / size.getX());
		}

		/**
		 * Constructs default node to shape transformer
		 */
		public NodeToShapeTransformer() {
			super(x -> Integer.valueOf(Math.max((int) x.getSize().getX(), 1)), NodeToShapeTransformer::getNodeAspectRatio);
		}

		@Override
		public Shape transform(Node input) {
			Shape shape = input.getShape();
			if (shape != null) {
				return shape;
			}
			return factory.getRectangle(input);
		}
	}
}
