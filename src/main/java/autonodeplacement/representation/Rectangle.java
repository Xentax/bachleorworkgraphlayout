/**
 * Copyright 2020 Jan Juda
 * Created: 15. 3. 2020
 */
package autonodeplacement.representation;

import static autonodeplacement.OutputGenerator.formatDouble;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.text.MessageFormat;

import autonodeplacement.TinyCadConstants;

/**
 * Class representing rectangle for Symbol definition
 * @author Jan Juda (xjudaj00)
 */
public class Rectangle {
	/** Left upper corner of the rectangle */
	Point2D a;
	/** Right bottom corner of the rectangle */
	Point2D b;
	/** Whether this is a boundary rectangle */
	boolean boundary;

	/**
	 * Constructs a rectangle
	 * @param leftUpperCorner left upper corner
	 * @param rightBottomCorner right bottom corner
	 */
	public Rectangle(Point2D leftUpperCorner, Point2D rightBottomCorner) {
		this(leftUpperCorner, rightBottomCorner, false);
	}

	/**
	 * Constructs a rectangle with a possibility to declare it as a subgraph
	 * @param leftUpperCorner left upper corner
	 * @param rightBottomCorner right bottom corner
	 * @param isSubgraph true if this is rectangle for a subgraph
	 */
	public Rectangle(Point2D leftUpperCorner, Point2D rightBottomCorner, boolean isSubgraph) {
		this.a = leftUpperCorner;
		this.b = rightBottomCorner;
		this.boundary = isSubgraph;
	}

	/**
	 * @return copy of this rectangle
	 */
	public Rectangle getCopy() {
		return new Rectangle(a, b);
	}

	/**
	 * @return this rectangle as shape Rectangle2D for graph view, no offset
	 */
	Rectangle2D getShape() {
		double width = (b.getX() - a.getX()) * 5;
		double height = (b.getY() - a.getY()) * 5;
		return new Rectangle2D.Double(0, 0, width, height);
	}

	@Override
	public String toString() {
		if (boundary) {
			return MessageFormat.format(TinyCadConstants.RECTANGLE_FOR_SUBGRAPH, formatDouble(a.getX()), formatDouble(a.getY()), formatDouble(b.getX()),
					formatDouble(b.getY()));
		}
		return MessageFormat.format(TinyCadConstants.RECTANGLE, formatDouble(a.getX()), formatDouble(a.getY()),
				formatDouble(b.getX()), formatDouble(b.getY()));
	}
}
