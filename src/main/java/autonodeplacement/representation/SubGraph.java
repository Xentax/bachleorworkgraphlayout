/**
 * Copyright 2020 Jan Juda
 * Created: 22. 3. 2020
 */
package autonodeplacement.representation;

import java.awt.geom.Point2D;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import autonodeplacement.Preferences;
import javafx.geometry.BoundingBox;

/**
 * Class for representing subgraph and graph hierarchy
 * @author Jan Juda (xjudaj00)
 */
public class SubGraph extends Node {

	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(SubGraph.class.toString());

	/** Compare coefficient for combined area of all nodes and area of graph */
	public static final double AREA_COMPARE_COEF = 1.5;

	/** List of all child nodes of this subgraph */
	private Map<String, Node> childNodes = new LinkedHashMap<>();

	/** Holds parts of this subgraph */
	private Map<BoundingBox, List<Node>> subgraphParts = new LinkedHashMap<>();

	/**
	 * Constructs subgraph
	 * @param id
	 */
	public SubGraph(String id) {
		super(id);
	}

	/**
	 * Constructs subgraph with full parameters, usefull for creating copies
	 * @param id
	 * @param sizeX
	 * @param sizeY
	 * @param positionX
	 * @param positionY
	 */
	public SubGraph(String id, double sizeX, double sizeY, double positionX, double positionY) {
		super(id, sizeX, sizeY, positionX, positionY);
	}

	/**
	 * Adds child node to this subgraph.
	 * @param node
	 */
	public void addChild(Node node) {
		if (node == null) {
			LOGGER.warning("Error while adding child to a SubGraph: Child node cannot be null.");
		} else if (childNodes.containsKey(node.getId())) {
			LOGGER.warning(MessageFormat.format("Error while adding child to a SubGraph: This subgraph {0} already contains node with ID {1}.",
					getId(), node.getId()));
		} else {
			childNodes.put(node.getId(), node);
		}
	}

	/**
	 * @return all children of this subgraph, not recursive = instances of SubGraph can be returned
	 */
	public List<Node> getChildren() {
		return new ArrayList<>(childNodes.values());
	}

	/**
	 * @return all "leaf" nodes of this subgraph, recursive = no instance of "SubGraph" is returned
	 */
	public List<Node> getNodes() {
		List<Node> nodes = new ArrayList<>();
		for (Node child : childNodes.values()) {
			if (child instanceof SubGraph) {
				nodes.addAll(((SubGraph) child).getNodes());
			} else {
				nodes.add(child);
			}
		}
		return nodes;
	}

	/**
	 * @return all "leaf" nodes of this subgraph on this depth level, subgraphs are ignored = no instance of "SubGraph" is returned, not recursive
	 */
	public List<Node> getNodesThisLevel() {
		return childNodes.values().stream().filter(x -> !(x instanceof SubGraph)).collect(Collectors.toList());
	}

	/**
	 * @return all subgraphs of this graph
	 */
	public List<SubGraph> getSubGraphs() {
		return childNodes.values().stream().filter(x -> x instanceof SubGraph).map(x -> (SubGraph) x).collect(Collectors.toList());
	}

	/**
	 * @param id
	 * @return Node matching given ID. Does not perform recursive search.
	 */
	public Node getNodeWithId(String id) {
		return childNodes.get(id);
	}

	/**
	 * @return min area for this subgraph, depending on all children
	 */
	public double getMinArea() {
		List<Node> nodes = getChildren();
		double combinedNodesArea = 0.0;
		for (Node node : nodes) {
			if (node instanceof SubGraph) {
				combinedNodesArea += ((SubGraph) node).getMinArea();
			} else {
				BoundingBox box = node.getBoundingBox(Preferences.getInstance().getNodeBorder(), false);
				combinedNodesArea += box.getWidth() * box.getHeight();
			}
		}
		combinedNodesArea *= AREA_COMPARE_COEF;
		return combinedNodesArea;
	}

	/**
	 * @return min size of dimensions for this subgraph, depending all leaf Nodes
	 */
	public Point2D getMinDimensions() {
		List<Node> nodes = getNodes();
		BoundingBox widest = new BoundingBox(0, 0, 0, 0);
		BoundingBox tallest = new BoundingBox(0, 0, 0, 0);
		for (Node node : nodes) {
			BoundingBox box; //this box is just for width/height purposes only
			if (node instanceof SubGraph) {
				SubGraph childGraph = (SubGraph) node;
				Point2D minSizeOfChild = childGraph.getMinDimensions();
				box = new BoundingBox(0, 0, minSizeOfChild.getX(), minSizeOfChild.getY());
			} else {
				box = node.getBoundingBox(Preferences.getInstance().getNodeBorder(), false);
			}
			if (box.getWidth() > widest.getWidth()) {
				widest = box;
			}
			if (box.getHeight() > widest.getHeight()) {
				tallest = box;
			}
		}
		if (widest != tallest) {
			return new Point2D.Double(widest.getWidth() + tallest.getWidth(), tallest.getHeight() + widest.getWidth());
		} else {
			return new Point2D.Double(widest.getWidth(), tallest.getHeight());
		}
	}

	/**
	 * @return set of all sides of the main graph wanted (preferred) by leaf nodes of this subgraph.
	 */
	@Override
	public Set<Location> getRequiredSides() {
		Set<Location> sides = new HashSet<>();
		for (Node node : getNodes()) {
			if(node.getPreferredLocation() != Location.NONE) {
				sides.add(node.getPreferredLocation());
			}
			if (sides.size() == Location.values().length - 1) {
				break;
			}
		}
		return sides;
	}

	/**
	 * @return map, where key is a child subgraph and value is a set of it's required (preferred) sides
	 */
	public Map<Node, Set<Location>> getRequiredSidesOfChildSubgraphs() {
		Map<Node, Set<Location>> requiredSidesOfSubgraphs = new LinkedHashMap<>();
		for (SubGraph child : this.getSubGraphs()) {
			Set<Location> sides = child.getRequiredSides();
			requiredSidesOfSubgraphs.put(child, sides);
		}
		return requiredSidesOfSubgraphs;
	}

	/**
	 * Sets parts of this subgraph
	 * @param subgraphParts
	 */
	public void setSubgraphParts(Map<BoundingBox, List<Node>> subgraphParts) {
		this.subgraphParts = subgraphParts;
	}

	/**
	 * @return parts of this subgraph or null if not set
	 */
	public Map<BoundingBox, List<Node>> getSubgraphParts() {
		return subgraphParts;
	}

	@Override
	public void shift(Point2D shift) {
		super.shift(shift);
		shiftParts(shift);
		for (Node node : getChildren()) {
			node.shift(shift);
		}
	}

	/**
	 * Shifts only this subgraph. It's children remain intact.
	 * @param shift
	 */
	public void shiftNotRecursive(Point2D shift) {
		super.shift(shift);
	}

	/**
	 * Shifts parts of this subgraph
	 * @param shift
	 */
	private void shiftParts(Point2D shift) {
		if (subgraphParts == null) {
			return;
		}
		Map<BoundingBox, List<Node>> newParts = new LinkedHashMap<>();
		for (Entry<BoundingBox, List<Node>> entry : subgraphParts.entrySet()) {
			BoundingBox oldBox = entry.getKey();
			newParts.put(new BoundingBox(oldBox.getMinX() + shift.getX(), oldBox.getMinY() + shift.getY(), oldBox.getWidth(), oldBox.getHeight()),
					entry.getValue());
		}
		setSubgraphParts(newParts);
	}

	@Override
	public void setPosition(Point2D newPosition) {
		shift(new Point2D.Double(newPosition.getX() - this.position.getX(), newPosition.getY() - position.getY()));
	}

	@Override
	public void setPosition(double x, double y) {
		setPosition(new Point2D.Double(x, y));
	}

	/**
	 * @param nodePath
	 * @return true, if this subgraph contains (even if not directly) node/subgraph with given node path.
	 */
	public boolean contains(String nodePath) {
		return nodePath.startsWith(this.toString());
	}

	/**
	 * Sets size of this subgraph to it's minimal size. <br>
	 * Minimal size is rectangle with matching width/height ratio to getMinDimensions() and area bigger or equal to getMinArea().
	 */
	public void setSizeToMinSize() {
		Point2D dimensions = getMinDimensions();
		double area = getMinArea();
		if (dimensions.getX() * dimensions.getY() > area) {
			setSize(dimensions);
		} else {
			double ratio = area / (dimensions.getX() * dimensions.getY());
			setSize(dimensions.getX() * ratio, dimensions.getY() * ratio);
		}
	}

	/**
	 * Resizes entire subgraph by given aspect
	 * @param aspect
	 */
	public void resize(double aspect) {
		size = new Point2D.Double(size.getX() * aspect, size.getY() * aspect);
		position = new Point2D.Double(position.getX() * aspect, position.getY() * aspect);
		for (Node node : getNodesThisLevel()) {
			node.setPosition(node.getPosition().getX() * aspect, node.getPosition().getY() * aspect);
			node.setSize(node.getSize().getX() * aspect, node.getSize().getY() * aspect);
		}
		Map<BoundingBox, List<Node>> newParts = new LinkedHashMap<>();
		for (Entry<BoundingBox, List<Node>> entry : subgraphParts.entrySet()) {
			BoundingBox box = entry.getKey();
			newParts.put(new BoundingBox(box.getMinX() * aspect, box.getMinY() * aspect, box.getWidth() * aspect, box.getHeight() * aspect), entry.getValue());
		}
		setSubgraphParts(newParts);
		for (SubGraph subgraph : getSubGraphs()) {
			subgraph.resize(aspect);
		}
	}
}
