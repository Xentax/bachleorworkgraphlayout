/**
 * Copyright 2020 Jan Juda
 * Created: 15. 2. 2020
 */
package autonodeplacement.representation;

import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import autonodeplacement.InputParser;
import javafx.geometry.BoundingBox;

/**
 * Internal representation of graph node.
 * @author Jan Juda (xjudaj00)
 */
public class Node {

	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(Node.class.toString());

	/** ID symbolizing root subgraph */
	public static final String ROOT_ID = "";

	/** Default size of a node */
	public static final double DEFAULT_SIZE = 20.0;
	/** Default position of a node */
	public static final double DEFAULT_POSITION = 0.0;
	/** Maximal generated width of node */
	public static final int MAX_WIDTH = 300;
	/** If node is wider than this limit, than increasing width by certain step will be applied */
	public static final int WIDTH_LIMIT_FOR_STEP = 100;
	/** Step for increasing width */
	public static final int WIDTH_STEP = 50;

	/** Id of this node */
	private String id;

	/** Size of this node */
	protected Point2D size;

	/** Position of this node */
	protected Point2D position;

	/** Input edges of this node */
	private List<Edge> inputEdges = new ArrayList<>();

	/** Output edges of this node */
	private List<Edge> outputEdges = new ArrayList<>();

	/** Preferred location of this node. Defaults to none. */
	private Location preferredLocation = Location.NONE;

	/** Symbol definition for this node */
	private SymbolDefinition symbolDef = null;

	/** Position of a name for this node */
	private Point2D namePosition = null;

	/** Parent subgraph node. Null if this is top-level node. */
	protected SubGraph parent = null;

	/**
	 * Default constructor
	 * @param id of this node
	 */
	public Node(String id) {
		this(id, getRecommendedWidth(id), DEFAULT_SIZE, DEFAULT_POSITION, DEFAULT_POSITION);
	}

	/**
	 * Constructor
	 * @param id of this node
	 * @param sizeX size in X axis
	 * @param sizeY size in Y axis
	 * @param positionX position in X axis
	 * @param positionY position in Y axis
	 */
	public Node(String id, double sizeX, double sizeY, double positionX, double positionY) {
		this.id = id;
		this.size = new Point2D.Double(sizeX, sizeY);
		this.position = new Point2D.Double(positionX, positionY);
	}

	/**
	 * @return id of this node
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return size of this node
	 */
	public Point2D getSize() {
		return size;
	}

	/**
	 * @return position of this node
	 */
	public Point2D getPosition() {
		return position;
	}

	/**
	 * Sets node position as x and y
	 * @param x
	 * @param y
	 */
	public void setPosition(double x, double y) {
		this.position = new Point2D.Double(x, y);
	}

	/**
	 * Sets node position
	 * @param position
	 */
	public void setPosition(Point2D position) {
		this.position = position;
	}

	/**
	 * Sets node size as x and y
	 * @param x
	 * @param y
	 */
	public void setSize(double x, double y) {
		this.size = new Point2D.Double(x, y);
	}

	/**
	 * Sets node size
	 * @param size
	 */
	public void setSize(Point2D size) {
		this.size = size;
	}

	/**
	 * @return list of all input edges
	 */
	public List<Edge> getInputEdges() {
		return inputEdges;
	}

	/**
	 * @return list of all output edges
	 */
	public List<Edge> getOutputEdges() {
		return outputEdges;
	}

	/**
	 * Adds input edge to this node
	 * @param edge input edge to add
	 */
	public void addInputEdge(Edge edge) {
		if (edge.getEndNode() == this) {
			inputEdges.add(edge);
		} else {
			LOGGER.warning(MessageFormat.format("Trying to add input edge {0} to node {1}, but this edge doesn''t end in this node.",
					edge.getId(), this.id));
		}
	}

	/**
	 * Adds output edge to this node
	 * @param edge output edge to add
	 */
	public void addOutputEdge(Edge edge) {
		if (edge.getStartNode() == this) {
			outputEdges.add(edge);
		} else {
			LOGGER.warning(
					MessageFormat.format("Trying to add output edge {0} to node {1}, but this edge doesn''t start in this node.",
					edge.getId(), this.id));
		}
	}

	/**
	 * @return the preferredLocation
	 */
	public Location getPreferredLocation() {
		return preferredLocation;
	}

	/**
	 * @param preferredLocation the preferredLocation to set
	 */
	public void setPreferredLocation(Location preferredLocation) {
		if (this.preferredLocation != Location.NONE) {
			LOGGER.warning(MessageFormat.format(InputParser.MESSAGE_TOO_MANY_PREFERRED_LOCS,
					id, this.preferredLocation, preferredLocation));
		}
		this.preferredLocation = preferredLocation;
	}

	/**
	 * @param name ID of a Node
	 * @return recommended width calculated by length of node ID
	 */
	private static double getRecommendedWidth(String name) {
		int width = name.length() * 7 + 10;
		width = Math.min(Math.max((int) DEFAULT_SIZE, width), MAX_WIDTH);
		return width;
	}

	/**
	 * @param border additional border for this node. Should not be negative.<br>
	 *            Ex. if border is 10.0, than both width and height is increased by 20.0 (2x border).
	 * @param shiftFullSize if true, than position is shifted by full size, not just by half a size
	 * @return bounding box of this node with given border
	 */
	public BoundingBox getBoundingBox(double border, boolean shiftFullSize) {
		double divider = shiftFullSize ? 1 : 2;
		double x = position.getX() - size.getX() / divider - border;
		double y = position.getY() - size.getY() / divider - border;
		return new BoundingBox(x, y, size.getX() + border, size.getY() + border);
	}

	/**
	 * Sets new symbol definition for this node.
	 * @param symbolDef
	 */
	public void setSymbolDefinition(SymbolDefinition symbolDef) {
		this.symbolDef = symbolDef;
	}

	/**
	 * @return stored symbol definition for this Node. Can be null if none was set.
	 */
	public SymbolDefinition getSymbolDefinition() {
		return symbolDef;
	}

	/**
	 * Shifts node by given shift.
	 * @param shift
	 */
	public void shift(Point2D shift) {
		position = new Point2D.Double(position.getX() + shift.getX(), position.getY() + shift.getY());
	}

	/**
	 * Sets position of name of this node for TinyCad output
	 * @param namePosition
	 */
	public void setNamePosition(Point2D namePosition) {
		this.namePosition = namePosition;
	}

	/**
	 * @return name position for this node if given
	 */
	public Point2D getNamePosition() {
		return namePosition;
	}

	/**
	 * Sets parent node (subgraph) of this node. Also if parent is not null, then adds this as it's child
	 * @param parentNode
	 */
	public void setParent(SubGraph parentNode) {
		this.parent = parentNode;
		if (parentNode != null) {
			parentNode.addChild(this);
		}
	}

	/**
	 * @return parent node (subgraph) of this node or null, if this is top-level node
	 */
	public SubGraph getParent() {
		return parent;
	}

	/**
	 * @return shape of this node or null, if no symbol definition found
	 */
	public Area getShape() {
		return symbolDef != null ? symbolDef.getShape() : null;
	}

	@Override
	public String toString() {
		String path;
		if (parent != null) {
			path = parent.toString();
			// do not start paths with dot '.node'
			path = path.equals(ROOT_ID) ? id : path + "." + id;
		} else {
			path = id;
		}
		return path;
	}

	/**
	 * @return preferred location wrapped in set, so interface matches with subgraph
	 */
	public Set<Location> getRequiredSides() {
		Set<Location> result = new HashSet<>();
		if (preferredLocation != Location.NONE) {
			result.add(preferredLocation);
		}
		return result;
	}

	/**
	 * @param border
	 * @return area of this node's bounding with given border
	 */
	public double getArea(double border) {
		BoundingBox box = getBoundingBox(border, false);
		return box.getHeight() * box.getWidth();
	}

	/**
	 * Enum describing preferred location of a node
	 * @author Jan Juda (xjudaj00)
	 */
	public enum Location {
		/** Nothing is being parsed */
		NONE,
		/** Left border */
		LEFT,
		/** Right border */
		RIGHT,
		/** Top border */
		TOP,
		/** Bottom border */
		BOTTOM;

		/**
		 * @return complementary location. For TOP returns BOTTOM, for LEFT returns RIGHT...
		 */
		public Location complementary() {
			switch (this) {
				case BOTTOM:
					return TOP;
				case LEFT:
					return RIGHT;
				case RIGHT:
					return LEFT;
				case TOP:
				default:
					return BOTTOM;
			}
		}
	}
}
