/**
 * Copyright 2020 Jan Juda
 * Created: 15. 2. 2020
 */
package autonodeplacement.representation;

/**
 * Internal representation of graph edge.
 * @author Jan Juda (xjudaj00)
 */
public class Edge {

	/** Id of this edge */
	private String id;

	/** Start node of this edge */
	private Node startNode;

	/** End node of this edge */
	private Node endNode;

	/** Start pin of this edge */
	private Pin startPin = null;

	/** End pin of this edge */
	private Pin endPin = null;

	/**
	 * @param id of the edge
	 * @param start from this node
	 * @param end to this node
	 */
	public Edge(String id, Node start, Node end) {
		this.id = id;
		this.startNode = start;
		this.endNode = end;
		start.addOutputEdge(this);
		end.addInputEdge(this);
	}

	/**
	 * @return id of this edge
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return start node of this edge
	 */
	public Node getStartNode() {
		return startNode;
	}

	/**
	 * @return end node of this edge
	 */
	public Node getEndNode() {
		return endNode;
	}

	/**
	 * @param node
	 * @return true whether this edge contains given node (if given node is either start or end of this edge)
	 */
	public boolean contains(Node node) {
		return startNode == node || endNode == node;
	}

	/**
	 * Sets start pin of this edge
	 * @param pin
	 */
	public void setStartPin(Pin pin) {
		startPin = pin;
	}

	/**
	 * Sets end pin of this edge
	 * @param pin
	 */
	public void setEndPin(Pin pin) {
		endPin = pin;
	}

	/**
	 * @return start pin of this edge
	 */
	public Pin getStartPin() {
		return startPin;
	}

	/**
	 * @return end pin of this edge
	 */
	public Pin getEndPin() {
		return endPin;
	}

	@Override
	public String toString() {
		return id;
	}
}
