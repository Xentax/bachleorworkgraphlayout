/**
 * Copyright 2020 Jan Juda
 * Created: 8. 3. 2020
 */
package autonodeplacement;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import autonodeplacement.representation.Node.Location;
import autonodeplacement.representation.SubGraph;
import javafx.geometry.BoundingBox;

/**
 * Class containing all the available post processing
 * @author Jan Juda (xjudaj00)
 */
public class PostProcessing {

	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(PostProcessing.class.toString());

	/** Little addition to shifting to make sure bounding box edges aren't exactly equal */
	private static final double LITTLE_BIT_TO_SHIFT = 0.01;

	/**
	 * Intentionally empty constructor
	 */
	private PostProcessing() {
	}

	/**
	 * Performs complete post-processing on a subgraph part <br>
	 * Includes adjusting subgraph positions and checking node overflow and overlap
	 * @param partSpace space for the node
	 * @param nodesToProcess
	 * @return true if size post-processing was stopped by it's limit, false otherwise
	 */
	public static boolean completePartPostProcess(BoundingBox partSpace, List<Node> nodesToProcess) {
		if (nodesToProcess.isEmpty()) {
			return false;
		}
		adjustSubgraphsPositions(partSpace, nodesToProcess);
		// Apply size post processing
		boolean result = false;
		if (Preferences.getInstance().getSizePostprocessAllowed()) {
			result = PostProcessing.checkOverlapAndOverflow(partSpace, nodesToProcess);
		}
		return result;
	}

	/**
	 * Adjust subgraph position so they fit into their column/row as they should
	 * @param partSpace
	 * @param nodesToProcess
	 */
	private static void adjustSubgraphsPositions(BoundingBox partSpace, List<Node> nodesToProcess) {
		if (nodesToProcess.size() == 1) {
			Node node = nodesToProcess.get(0);
			if (node instanceof SubGraph) {
				node.setPosition(partSpace.getMinX() + partSpace.getWidth() / 2, partSpace.getMinY() + partSpace.getHeight() / 2);
			}
			return;
		}
		Location determinatingLocation = PreProcessing.getDeterminatingLocation(nodesToProcess);
		if (determinatingLocation == Location.NONE) {
			return; // nothing to do here, subgraph in rectangle parts are not anchored to anything
		}
		// for all other cases we adjust subgraph into a row
		boolean leftToRight = determinatingLocation == Location.TOP || determinatingLocation == Location.BOTTOM;
		// order by X or Y
		nodesToProcess.sort((first, second) -> (int) (leftToRight ? first.getPosition().getX() - second.getPosition().getX()
				: first.getPosition().getY() - second.getPosition().getY()));
		double sum = nodesToProcess.stream().mapToDouble(x -> leftToRight ? getBoundingBox(x).getWidth() : getBoundingBox(x).getHeight()).sum();
		double ratio = (leftToRight ? partSpace.getWidth() : partSpace.getHeight()) / sum;
		double fixed = leftToRight ? partSpace.getHeight() / 2 + partSpace.getMinY() : partSpace.getWidth() / 2 + partSpace.getMinX();
		double increasing = leftToRight ? partSpace.getMinX() : partSpace.getMinY();
		for (Node node : nodesToProcess) {
			// Add half size before setting position and half size after
			double toAdd = ratio * (leftToRight ? getBoundingBox(node).getWidth() : getBoundingBox(node).getHeight()) / 2;
			increasing += toAdd;
			if (node instanceof SubGraph) {
				SubGraph subgraph = (SubGraph) node;
				if (leftToRight) {
					subgraph.setPosition(increasing, fixed);
				} else {
					subgraph.setPosition(fixed, increasing);
				}
			}
			increasing += toAdd;
		}
	}

	/**
	 * TinyCad forces, that reference point of any symbol is the bottom right corner. <br>
	 * We use as reference point middle of a node (width/2, height/2), so we need to shift all symbols, so that nothing overflows on top or left side
	 * in the result.
	 * @param graph
	 * @return true if increasing graph size was necessary to fit all nodes with their sizes, false otherwise.
	 */
	public static boolean shiftForOutput(GraphRepresentation graph) {
		BoundingBox graphSpace = new BoundingBox(0, 0, graph.getSize().width, graph.getSize().height);
		double topShift = 0;
		double leftShift = 0;
		// At first, shift the entire graph far away from graph space (above and to the left).
		shiftGraph(graph, new Point2D.Double(graph.getSize().width * -2.0, graph.getSize().height * -2.0));
		// Then fit it exactly to the graph space, so nothing overlaps on top or left side
		List<Node> nodesAndSubgraphs = new ArrayList<>(graph.getNodes());
		nodesAndSubgraphs.addAll(graph.getSubgraphs().values());
		for (Node node : nodesAndSubgraphs) {
			BoundingBox nodeBox = node.getBoundingBox(0, true);
			// Get the biggest shift needed to fit
			double thisNodeTopShift = graphSpace.getMinY() - nodeBox.getMinY();
			double thisNodeLeftShift = graphSpace.getMinX() - nodeBox.getMinX();
			topShift = Math.max(topShift, thisNodeTopShift);
			leftShift = Math.max(leftShift, thisNodeLeftShift);
		}
		if (topShift > 0 || leftShift > 0) {
			Point2D shift = new Point2D.Double(leftShift, topShift);
			shiftGraph(graph, shift);
		}
		// After shifting, if any node sticks out of the graph, than we will have to increase graph size
		Dimension oldSize = graph.getSize();
		if (checkGraphSizeAndIncrease(graph)) {
			LOGGER.info(MessageFormat.format("Graph size was increased from ({0};{1}) to ({2};{3}) during preparation for writing output.",
					Integer.valueOf(oldSize.width), Integer.valueOf(oldSize.height), Integer.valueOf(graph.getSize().width),
					Integer.valueOf(graph.getSize().height)));
			return true;
		}
		return false;
	}

	/**
	 * Method checks for overlapping nodes or nodes overflowing from graph space and solves these issues.
	 * @param partSpace
	 * @param nodesToProcess
	 * @param graph
	 * @return true if iterations were stopped by a limit (so post processing might not be complete), false otherwise
	 */
	public static boolean checkOverlapAndOverflow(BoundingBox partSpace, List<Node> nodesToProcess) {
		boolean result = false;
		int i = 0;
		int maxIterations = Preferences.getInstance().getMaxIterationsPostprocess();
		while (i < maxIterations && checkOverflow(partSpace, nodesToProcess)) {
			i++;
		}
		result = i == maxIterations;
		// Once overflow is done, check overlap
		i = 0;
		while (i < maxIterations && checkOverlap(partSpace, nodesToProcess)) {
			i++;
		}
		result = result || i == maxIterations;
		// Now there are (hopefully) no overlapping nodes and no overflowing nodes
		return result;
	}

	/**
	 * Method checks for overlapping nodes and moves them, so they do not longer overlap. <br>
	 * Node is never moved from partSpace.
	 * @param partSpace space for the node
	 * @param nodesToProcess
	 * @return true if changes were made, false otherwise
	 */
	private static boolean checkOverlap(BoundingBox partSpace, List<Node> nodesToProcess) {
		boolean result = false;
		// Get graph nodes sorted by sum of X and Y of position (from lower)
		Collection<Node> nodes = nodesToProcess.stream()
				.sorted((x, y) -> (int) (x.getPosition().getX() + x.getPosition().getY() - (y.getPosition().getX() + x.getPosition().getY())))
				.collect(Collectors.toList());
		// Check for all overlapping nodes
		for (Node firstNode : nodes) {
			for (Node secondNode : nodes) {
				if (firstNode == secondNode) {
					continue;
				}
				BoundingBox firstBox = firstNode.getBoundingBox(Preferences.getInstance().getNodeBorder(), false);
				BoundingBox secondBox = secondNode.getBoundingBox(Preferences.getInstance().getNodeBorder(), false);
				if (firstBox.intersects(secondBox)) {
					result = true;
					Node nodeToMove = null;
					if (firstBox.contains(secondBox)) { // second node must move
						nodeToMove = secondNode;
					} else if (secondBox.contains(firstBox)) { // first node must move
						nodeToMove = firstNode;
					}
					if (nodeToMove == null) { /// only partial intersect
						List<Direction> possibleDirectionsFirst = canMoveWhichDirections(firstNode, secondNode, partSpace, nodesToProcess);
						List<Direction> possibleDirectionsSecond = canMoveWhichDirections(secondNode, firstNode, partSpace, nodesToProcess);
						if ((possibleDirectionsFirst.isEmpty() && possibleDirectionsSecond.isEmpty()) || // The worst case or...
								(!possibleDirectionsFirst.isEmpty() && !possibleDirectionsSecond.isEmpty())) { // we can move both.
							// This case is handled together, because we can't simply decide which one to move depending that one will have conflict
							// and the other one no (because both will have a conflict or both won't have a conflict).
							// Let's say that smaller node can make less "damage". Meaning there is lesser chance of multiple conflicts
							// and also moving a smaller node will produce smaller changes in relative distances between nodes.
							double areaFirst = firstBox.getHeight() * firstBox.getWidth();
							double areaSecond = secondBox.getHeight() * secondBox.getWidth();
							nodeToMove = areaFirst < areaSecond ? firstNode : secondNode;
						} else if (possibleDirectionsFirst.isEmpty()) { // First is empty, but second not.
							nodeToMove = secondNode;
						} else { // Second is empty, but first not.
							nodeToMove = firstNode;
						}
					}
					Node obstacle = firstNode == nodeToMove ? secondNode : firstNode; // the other node is obstacle
					moveNodeFromObstacle(nodeToMove, obstacle, partSpace, nodesToProcess);
				}
			}
		}
		return result;
	}

	/**
	 * This method moves node from an obstacle node. It chooses the best direction to move the node and only moves it in minimal necessary way.
	 * Precondition is that the nodes are overlapping.
	 * @param nodeToMove
	 * @param obstacle
	 * @param space for the nodes
	 * @param nodes
	 */
	private static void moveNodeFromObstacle(Node nodeToMove, Node obstacle, BoundingBox space, List<Node> nodes) {
		List<Direction> possibleDirections = canMoveWhichDirections(nodeToMove, obstacle, space, nodes);
		Direction directionToMove;
		BoundingBox nodeBox = nodeToMove.getBoundingBox(Preferences.getInstance().getNodeBorder(), false);
		BoundingBox obstacleBox = obstacle.getBoundingBox(Preferences.getInstance().getNodeBorder(), false);
		if (possibleDirections.isEmpty()) {
			// Giving empty list causes it to only check for space boundaries,
			// resulting in possibility, that in next iterations overlap will be solved.
			possibleDirections = canMoveWhichDirections(nodeToMove, obstacle, space, new ArrayList<>());
			// Also possible directions cannot be empty here, because that would mean, that the node is bigger than the assigned space for it.
			if (possibleDirections.isEmpty()) {
				LOGGER.warning(MessageFormat.format("During postprocessing of subgraph part {0} was found, that node {1} is bigger than the assigned space!",
						space, nodeToMove));
			}
		}
		if (possibleDirections.size() == 1) { // Just one possible option
			directionToMove = possibleDirections.get(0);
		} else { // Pick best possibility
			directionToMove = possibleDirections.get(0);
			double smallestShift = Double.POSITIVE_INFINITY;
			for (Direction direction : possibleDirections) {
				double shiftForThisDirection = getBoxShiftInDirection(nodeBox, obstacleBox, direction);
				if (Math.abs(shiftForThisDirection) < Math.abs(smallestShift)) {
					directionToMove = direction;
					smallestShift = shiftForThisDirection;
				}
			}
		}
		Point2D shift = getBoxShiftInDirection2D(nodeBox, obstacleBox, directionToMove);
		nodeToMove.shift(shift);
	}

	/**
	 * @param nodeToMove this node will be moved, must be overlapping with obstacle
	 * @param obstacle this node only acts as obstacle and is used to calculate minimal necessary move
	 * @param space for the nodes
	 * @param nodes
	 * @return List of directions in which this node can move, so that it doesn't overlap with any other
	 */
	private static List<Direction> canMoveWhichDirections(Node nodeToMove, Node obstacle, BoundingBox space, List<Node> nodes) {
		List<Direction> result = new ArrayList<>();
		result.addAll(Arrays.asList(Direction.values()));
		BoundingBox nodeBox = nodeToMove.getBoundingBox(Preferences.getInstance().getNodeBorder(), false);
		BoundingBox obstacleBox = obstacle.getBoundingBox(Preferences.getInstance().getNodeBorder(), false);
		BoundingBox shiftedUp = shiftBox(nodeBox, getBoxShiftInDirection2D(nodeBox, obstacleBox, Direction.UP));
		BoundingBox shiftedDown = shiftBox(nodeBox, getBoxShiftInDirection2D(nodeBox, obstacleBox, Direction.DOWN));
		BoundingBox shiftedLeft = shiftBox(nodeBox, getBoxShiftInDirection2D(nodeBox, obstacleBox, Direction.LEFT));
		BoundingBox shiftedRight = shiftBox(nodeBox, getBoxShiftInDirection2D(nodeBox, obstacleBox, Direction.RIGHT));
		// Check diagram bounds
		if (shiftedUp.getMinY() < space.getMinY()) {
			result.remove(Direction.UP);
		}
		if (shiftedDown.getMaxY() > space.getMaxY()) {
			result.remove(Direction.DOWN);
		}
		if (shiftedLeft.getMinX() < space.getMinX()) {
			result.remove(Direction.LEFT);
		}
		if (shiftedRight.getMaxX() > space.getMaxX()) {
			result.remove(Direction.RIGHT);
		}
		// Check collision with other nodes
		for (Node nodeObstacle : nodes) {
			if (result.isEmpty()) { // short-cut
				break;
			}
			if (nodeObstacle == nodeToMove) {
				continue;
			}
			obstacleBox = nodeObstacle.getBoundingBox(Preferences.getInstance().getNodeBorder(), false);
			if (shiftedUp.intersects(obstacleBox)) {
				result.remove(Direction.UP);
			}
			if (shiftedDown.intersects(obstacleBox)) {
				result.remove(Direction.DOWN);
			}
			if (shiftedLeft.intersects(obstacleBox)) {
				result.remove(Direction.LEFT);
			}
			if (shiftedRight.intersects(obstacleBox)) {
				result.remove(Direction.RIGHT);
			}
		}
		return result;
	}

	/**
	 * @param nodeBox this node will be shifted
	 * @param obstacleBox this an obstacle
	 * @param direction of the shift
	 * @return return minimal shift of nodeBox needed to avoid overlapping of nodeBox and obstacleBox in given direction
	 */
	private static double getBoxShiftInDirection(BoundingBox nodeBox, BoundingBox obstacleBox, Direction direction) {
		double shift;
		switch (direction) {
			case UP:
				shift = obstacleBox.getMinY() - nodeBox.getMaxY();
				break;
			default:
			case DOWN:
				shift = obstacleBox.getMaxY() - nodeBox.getMinY();
				break;
			case LEFT:
				shift = obstacleBox.getMinX() - nodeBox.getMaxX();
				break;
			case RIGHT:
				shift = obstacleBox.getMaxX() - nodeBox.getMinX();
				break;
		}
		// Add just a small little bit to make sure edges of bounding boxes do not equal (that would case the box to still intersect)
		return shift + LITTLE_BIT_TO_SHIFT * Math.signum(shift);
	}

	/**
	 * @param nodeBox this node will be shifted
	 * @param obstacleBox this an obstacle
	 * @param direction of the shift
	 * @return return minimal shift of nodeBox needed to avoid overlapping of nodeBox and obstacleBox in given direction as Point2D
	 */
	private static Point2D getBoxShiftInDirection2D(BoundingBox nodeBox, BoundingBox obstacleBox, Direction direction) {
		switch (direction) {
			default:
			case DOWN:
			case UP:
				return new Point2D.Double(0, getBoxShiftInDirection(nodeBox, obstacleBox, direction));
			case LEFT:
			case RIGHT:
				return new Point2D.Double(getBoxShiftInDirection(nodeBox, obstacleBox, direction), 0);
		}
	}

	/**
	 * Checks if no node overflows from bottom of right side and if any does, then graph size is increased so all nodes fit.
	 * @param graph
	 * @return true if graph size was increased, false otherwise
	 */
	private static boolean checkGraphSizeAndIncrease(GraphRepresentation graph) {
		BoundingBox graphSpace = new BoundingBox(0, 0, graph.getSize().width, graph.getSize().height);
		double bottomMissingSize = 0;
		double rightMissingSize = 0;
		for (Node node : graph.getNodes()) {
			BoundingBox nodeBox = node.getBoundingBox(Preferences.getInstance().getNodeBorder(), true);
			double thisNodeBottomMiss = nodeBox.getMaxY() - graphSpace.getMaxY();
			double thisNodeRightMiss = nodeBox.getMaxX() - graphSpace.getMaxX();
			bottomMissingSize = Math.max(bottomMissingSize, thisNodeBottomMiss);
			rightMissingSize = Math.max(rightMissingSize, thisNodeRightMiss);
		}

		if (bottomMissingSize > 0 || rightMissingSize > 0) {
			graph.setSize((int) Math.ceil(graph.getSize().width + rightMissingSize), (int) Math.ceil(graph.getSize().height + bottomMissingSize));
			return true;
		}
		return false;
	}

	/**
	 * Method checks for nodes overflowing form partSpace and moves them, so they do not longer overflow.
	 * @param partSpace space for the nodes
	 * @param nodesToProcess
	 * @return true if changes were made, false otherwise
	 */
	private static boolean checkOverflow(BoundingBox partSpace, List<Node> nodesToProcess) {
		boolean result = false;
		for (Node node : nodesToProcess) {
			BoundingBox nodeBoxForSpace = getBoundingBox(node);
			Point2D shift = null;
			if (!partSpace.contains(nodeBoxForSpace)) {
				shift = getMinimumShiftForSpace(nodeBoxForSpace, partSpace);
				node.shift(shift);
				result = true;
			}
		}
		return result;
	}

	/**
	 * @param nodeBox bounding box of a node
	 * @param partSpace must completely contain nodeBox
	 * @return minimum shift after which will node fit into graphSpace
	 */
	private static Point2D getMinimumShiftForSpace(BoundingBox nodeBox, BoundingBox partSpace) {
		double xShift = 0;
		double yShift = 0;
		if (partSpace.getMinX() > nodeBox.getMinX()) {
			xShift = partSpace.getMinX() - nodeBox.getMinX();
		} else if (partSpace.getMaxX() < nodeBox.getMaxX()) {
			xShift = partSpace.getMaxX() - nodeBox.getMaxX();
		}
		if (partSpace.getMinY() > nodeBox.getMinY()) {
			yShift = partSpace.getMinY() - nodeBox.getMinY();
		} else if (partSpace.getMaxY() < nodeBox.getMaxY()) {
			yShift = partSpace.getMaxY() - nodeBox.getMaxY();
		}
		return new Point2D.Double(xShift, yShift);
	}

	/**
	 * Shifts every node in graph by given shift
	 * @param graph
	 * @param shift
	 */
	private static void shiftGraph(GraphRepresentation graph, Point2D shift) {
		graph.getRootGraph().shift(shift);
	}

	/**
	 * @param box bounding box to shift, isn't changed
	 * @param shift
	 * @return new bounding box shifted to different position by given shift
	 */
	private static BoundingBox shiftBox(BoundingBox box, Point2D shift) {
		return new BoundingBox(box.getMinX() + shift.getX(), box.getMinY() + shift.getY(), box.getWidth(), box.getHeight());
	}

	/**
	 * @param node
	 * @return bounding box for correct parameters depending on whether the node is SubGraph or simple Node
	 */
	private static BoundingBox getBoundingBox(Node node) {
		if (node instanceof SubGraph) {
			return node.getBoundingBox(Preferences.getInstance().getSubgraphBorder(), false);
		} else {
			return node.getBoundingBox(Preferences.getInstance().getNodeBorder(), false);
		}
	}
}
