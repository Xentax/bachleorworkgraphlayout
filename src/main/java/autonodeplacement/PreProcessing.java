/**
 * Copyright 2020 Jan Juda
 * Created: 8. 3. 2020
 */
package autonodeplacement;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import autonodeplacement.algorithms.AbstractAdapter;
import autonodeplacement.algorithms.Algorithms;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import autonodeplacement.representation.Node.Location;
import autonodeplacement.representation.SubGraph;
import javafx.geometry.BoundingBox;

/**
 * Class containing all available pre-processing
 * @author Jan Juda (xjudaj00)
 */
public class PreProcessing {

	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(PreProcessing.class.toString());

	/** Seed for Math.random if user insists on deterministic algorithm behaviour */
	private static final long SEED = 66627051997666L;

	/**
	 * Intentionally empty constructor
	 */
	private PreProcessing() {
	}

	/**
	 * Checks if it is even possible to fit all nodes into their subgraph and all subgraphs into a graph and if not, then graph size is increased.
	 * <br>
	 * Minimum graph size is considered as following: <br>
	 * - each graph dimension must be bigger or equal to sum of the dimension of boundingBox of largest node in this dimension and the dimension of
	 * boundingBox of largest node in the other dimension <br>
	 * For example we have tallest and widest node, than minimum height = sum of heights of these two nodes.<br>
	 * - graph area must be bigger or equal to 1.5x combined area of boundingBoxes of all nodes. When increasing size, ratio is kept.
	 * @param graph
	 */
	public static void checkGraphSize(GraphRepresentation graph) {
		Dimension oldSize = graph.getSize();
		SubGraph rootGraph = graph.getRootGraph();
		double minRootArea = rootGraph.getMinArea();
		Point2D minRootSize = rootGraph.getMinDimensions();
		Point2D newSize = new Point2D.Double(Math.max(minRootSize.getX(), oldSize.getWidth()), Math.max(minRootSize.getY(), oldSize.getHeight()));
		double graphArea = newSize.getX() * newSize.getY();
		if (minRootArea > graphArea) {
			double ratio = minRootArea / graphArea;
			newSize.setLocation(newSize.getX() * ratio, newSize.getY() * ratio);
		}
		Dimension newDimension = new Dimension((int) Math.ceil(newSize.getX()), (int) Math.ceil(newSize.getY()));
		if (!oldSize.equals(newDimension)) {
			graph.setSize(newDimension);
			LOGGER.info(MessageFormat.format("Graph size was increased from ({0};{1}) to ({2};{3}) during pre-processing in order to fit all nodes in graph space.",
							Integer.valueOf(oldSize.width), Integer.valueOf(oldSize.height), Integer.valueOf(graph.getSize().width),
							Integer.valueOf(graph.getSize().height)));
		}
	}

	/**
	 * Recursively splits each subgraph into parts and layouts the nodes inside these parts.
	 * @param subgraph
	 * @return true if error occured, false if everything is OK
	 */
	public static boolean prepareSubGraphs(SubGraph subgraph) {
		Map<Node, Set<Location>> requiredSidesOfSubgraphs = subgraph.getRequiredSidesOfChildSubgraphs();
		// Test subgraphs for conflicts
		String msgPrefix = (subgraph.getId().equals(Node.ROOT_ID)) ? "Graph" : ("SubGraph " + subgraph);
		if (PreProcessing.hasLocationTypeConflict(requiredSidesOfSubgraphs.values())) {
			LOGGER.severe(msgPrefix + " has conflicts for it's subgraphs with preferred positions. This is fatal, not possible to layout.");
			return true;
		}
		if (PreProcessing.canAddingNodesCauseConflict(requiredSidesOfSubgraphs.values())) {
			// Nodes need to be individually tested
			for (Node childNode : subgraph.getNodesThisLevel()) {
				if (!PreProcessing.willCauseConflict(requiredSidesOfSubgraphs.values(), childNode)) {
					requiredSidesOfSubgraphs.put(childNode, childNode.getRequiredSides());
				} else {
					LOGGER.severe(MessageFormat.format(
							"{0} has conflicts for it''s subgraphs with preferred positions and node {1}. This is fatal, not possible to layout.", msgPrefix,
							childNode));
					return true;
				}
			}
		} else { // Add all nodes, because they can't cause a conflict
			subgraph.getNodesThisLevel().forEach(x -> requiredSidesOfSubgraphs.put(x, x.getRequiredSides()));
		}
		// Divide using wonderful space divider
		SpaceDivider divider = new SpaceDivider(requiredSidesOfSubgraphs);
		Map<BoundingBox, List<Node>> dividedSpace = divider.applyToRealSpace(subgraph.getBoundingBox(0, false));
		// FIXME future version - divide into strongly connected component
		subgraph.setSubgraphParts(dividedSpace);
		// Prepare each child subgraph
		for (SubGraph child : subgraph.getSubGraphs()) {
			if (prepareSubGraphs(child)) {
				return true; // if error occured on child, stop and return
			}
		}
		return false;
	}

	/**
	 * @param locations
	 * @param node
	 * @return true if adding this node (single-required-side) to the locations list will result in location conflict
	 */
	static boolean willCauseConflict(Collection<Set<Location>> locations, Node node) {
		if (locations.stream().anyMatch(x -> x.size() >= 4)) {
			return true; // if there is a subgraph with 4 required sides, adding anything will always cause a conflict
		}
		List<Location> blockedSides = locations.stream().filter(x -> x.size() == 3).map(SpaceDivider::getBlockLocationFromTriple)
				.collect(Collectors.toList());
		return blockedSides.contains(node.getPreferredLocation());
	}

	/**
	 * @param locations
	 * @return true if adding a single-required-side node to the locations list can cause location problem, false otherwise <br>
	 *         In other words, true is returned, if locations contains any set with 3 or more elements.
	 */
	static boolean canAddingNodesCauseConflict(Collection<Set<Location>> locations) {
		return locations.stream().anyMatch(x -> x.size() >= 3);
	}

	/**
	 * @param locationsParam
	 * @return true if given set of location types has conflict
	 */
	static boolean hasLocationTypeConflict(Collection<Set<Location>> locationsParam) {
		List<Set<Location>> locations = new ArrayList<>(locationsParam);
		// Test for all sides
		Set<Location> allSides = locations.stream().filter(x -> x.size() >= 4).findAny().orElse(null);
		if (allSides != null) {
			if (locations.size() > 1) {
				return true;
			}
		} else {
			// Test for triple sides
			locations = locations.stream().filter(x -> !x.isEmpty()).collect(Collectors.toList()); // None can be only in conflict with all sides
			List<Set<Location>> tripleSides = locations.stream().filter(x -> x.size() == 3).collect(Collectors.toList());
			if (!tripleSides.isEmpty()) {
				for (Set<Location> tripleSide : tripleSides) {
					Location blockedLocation = SpaceDivider.getBlockLocationFromTriple(tripleSide);
					long numberOfBlockLocationSets = locations.stream().filter(x -> x.contains(blockedLocation)).count();
					if (numberOfBlockLocationSets > 1) { // 1 is allowed because tripleSide is also part of the locations list
						return true;
					}
				}
			}
			// Test for | vs _
			List<Set<Location>> upToDowns = locations.stream().filter(x -> x.size() == 2 && x.contains(Location.TOP) && x.contains(Location.BOTTOM))
					.collect(Collectors.toList());
			List<Set<Location>> leftToRights = locations.stream().filter(x -> x.size() == 2 && x.contains(Location.LEFT) && x.contains(Location.RIGHT))
					.collect(Collectors.toList());
			if (!upToDowns.isEmpty() && !leftToRights.isEmpty()) {
				return true;
			}
			// Test for double corners
			List<Set<Location>> leftUpper = locations.stream().filter(x -> x.size() == 2 && x.contains(Location.LEFT) && x.contains(Location.TOP))
					.collect(Collectors.toList());
			List<Set<Location>> leftBottom = locations.stream().filter(x -> x.size() == 2 && x.contains(Location.LEFT) && x.contains(Location.BOTTOM))
					.collect(Collectors.toList());
			List<Set<Location>> rightUpper = locations.stream().filter(x -> x.size() == 2 && x.contains(Location.RIGHT) && x.contains(Location.TOP))
					.collect(Collectors.toList());
			List<Set<Location>> rightBottom = locations.stream().filter(x -> x.size() == 2 && x.contains(Location.RIGHT) && x.contains(Location.BOTTOM))
					.collect(Collectors.toList());
			if (leftUpper.size() >= 2 || leftBottom.size() >= 2 || rightUpper.size() >= 2 || rightBottom.size() >= 2) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Performs initial layouting for layout algorithms
	 * @param nodes to layout, {@link Node#getRequiredSides()} must return the same result for all nodes
	 * @param space to layout nodes in
	 */
	public static void initialLayout(BoundingBox space, List<Node> nodes) {
		if (nodes.size() <= 1) { // Lists with only 1 node are already done in SpaceDivider, nothing to do for empty lists
			return;
		}
		Location determinatingLocation = getDeterminatingLocation(nodes);
		if (determinatingLocation == Location.NONE) { // only here we distribute into a rectangle
			if (!Preferences.getInstance().getRandomInit()) {
				distributeDeterministic(nodes, space);
			} else {
				distributeRandom(nodes, space);
			}
			return;
		}
		// for all other cases we distribute into a row
		boolean leftToRight = determinatingLocation == Location.TOP || determinatingLocation == Location.BOTTOM;
		if (!Preferences.getInstance().getRandomInit()) {
			distributeDeterministicLine(nodes, space, leftToRight);
		} else {
			distributeRandomLine(nodes, space, leftToRight);
		}
	}

	/**
	 * @param nodes to get location from, {@link Node#getRequiredSides()} must return the same result for all nodes
	 * @return return determinating location for a nodes from one subgraph part
	 */
	public static Location getDeterminatingLocation(List<Node> nodes) {
		// Doesn't matter which node we pick here, all must return the same
		Set<Location> locations = nodes.get(0).getRequiredSides();
		if (locations.size() == 3) {
			return SpaceDivider.getBlockLocationFromTriple(locations);
		} else if (locations.size() == 2) { // Left-To-Right or Top-to-bottom column, side must be reversed
			return locations.contains(Location.TOP) ? Location.LEFT : Location.TOP;
		} else if (locations.size() == 1) {
			return locations.iterator().next();
		} else { // we have a rectangle
			return Location.NONE;
		}
	}

	/**
	 * Distributes nodes (in their creation order) equally into given space. Result should look like square or rectangle from nodes. <br>
	 * All center positions of nodes will always fit into given space.
	 * @param nodes
	 * @param space Space to distribute nodes in. Upper left corner can be non-zero.
	 */
	private static void distributeDeterministic(Collection<Node> nodes, BoundingBox space) {
		int columns = (int) Math.ceil(Math.sqrt(nodes.size()));
		double xStep = space.getWidth() / (columns + 1);
		double yStep = space.getHeight() / (columns + 1);
		double x = xStep + space.getMinX();
		double y = yStep + space.getMinY();
		Iterator<Node> iterator = nodes.iterator();
		for (int row = 0; row < columns && iterator.hasNext(); row++) {
			for (int column = 0; column < columns && iterator.hasNext(); column++) {
				Node node = iterator.next();
				node.setPosition(x, y);
				x += xStep;
			}
			y += yStep;
			x = xStep + space.getMinX();
		}
	}

	/**
	 * Distributes nodes equally into given space in a line.
	 * @param nodes
	 * @param space Space to distribute nodes in. Upper left corner can be non-zero.
	 * @param leftToRight whether the line should be leftToRight or topToBottom
	 */
	private static void distributeDeterministicLine(Collection<Node> nodes, BoundingBox space, boolean leftToRight) {
		double xStep = leftToRight ? space.getWidth() / (nodes.size() + 1) : 0;
		double yStep = leftToRight ? 0 : space.getHeight() / (nodes.size() + 1);
		double x = (leftToRight ? xStep : space.getWidth() / 2) + space.getMinX();
		double y = (leftToRight ? space.getHeight() / 2 : yStep) + space.getMinY();
		for (Node node : nodes) {
			node.setPosition(x, y);
			x += xStep;
			y += yStep;
		}
	}

	/**
	 * Distributes nodes randomly in a given space.
	 * @param nodes
	 * @param space Space to distribute nodes in. Upper left corner can be non-zero.
	 */
	private static void distributeRandom(Collection<Node> nodes, BoundingBox space) {
		for (Node node : nodes) {
			node.setPosition(Math.random() * space.getWidth() + space.getMinX(), Math.random() * space.getHeight() + space.getMinY());
		}
	}

	/**
	 * Distributes nodes randomly in a given space in a line.
	 * @param nodes
	 * @param space Space to distribute nodes in. Upper left corner can be non-zero.
	 * @param leftToRight whether the line should be leftToRight or topToBottom
	 */
	private static void distributeRandomLine(Collection<Node> nodes, BoundingBox space, boolean leftToRight) {
		double fixed = leftToRight ? space.getHeight() / 2 + space.getMinY() : space.getWidth() / 2 + space.getMinX();
		for (Node node : nodes) {
			if (leftToRight) {
				node.setPosition(Math.random() * space.getWidth() + space.getMinX(), fixed);
			} else {
				node.setPosition(fixed, Math.random() * space.getHeight() + space.getMinY());
			}

		}
	}

	/**
	 * Forces deterministic behaviour on algorithm through setting Math.random seed using reflection.
	 */
	public static void forceDeterministicBehaviour() {
		try {
			Class<?> randomHolder = Class.forName("java.lang.Math$RandomNumberGeneratorHolder");
			Field field = randomHolder.getDeclaredField("randomNumberGenerator");
			field.setAccessible(true);
			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
			field.set(null, new Random(SEED));
		} catch (@SuppressWarnings("unused") NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException
				| ClassNotFoundException e) {
			LOGGER.warning("Forcing deterministic behaviour failed. Reflection generated an exception. Algorithms will use randomized values.");
		}
	}

	/**
	 * Recursively performs action on all subgraph parts starting with given SubGraph.
	 * @param subgraph
	 * @param action to be applied to all parts of given subgraph and all of it's child subgraphs as well
	 */
	private static void recurseGraph(SubGraph subgraph, BiConsumer<? super BoundingBox, ? super List<Node>> action) {
		subgraph.getSubgraphParts().forEach(action);
		for (SubGraph childSubgraph : subgraph.getSubGraphs()) {
			recurseGraph(childSubgraph, action);
		}
	}

	/**
	 * @param graph
	 * @return creates requested number of copies of the graph, initial layout for them and an algorithm adapter for each copy
	 */
	public static List<AbstractAdapter> initialLayoutAndCreateAdapters(GraphRepresentation graph) {
		List<AbstractAdapter> result = new ArrayList<>();
		// Perform initial layout of the original graph
		recurseGraph(graph.getRootGraph(), PreProcessing::initialLayout);
		// Create copies and adapters
		Preferences preferences = Preferences.getInstance();
		Algorithms selectedAlgorithm = preferences.getSelectedAlgorithm();
		for (int i = 0; i < preferences.getStartInitializations(); i++) {
			GraphRepresentation graphCopy = graph.createCopy();
			if (preferences.getRandomInit()) { // If user requests random initialization, then initialize each copy separately
				recurseGraph(graphCopy.getRootGraph(), PreProcessing::initialLayout);
			}
			if (selectedAlgorithm == Algorithms.BEST) {
				// We need to call create copy here once more, because we need different copy for each of the Algorithm adapter,
				// they can have the same initialization, because different algorithms will be used...
				Arrays.stream(Algorithms.values()).filter(x -> x != Algorithms.BEST)
						.forEach(x -> result.add(AbstractAdapter.getAdapterInstance(x, graphCopy.createCopy())));
			} else {
				result.add(AbstractAdapter.getAdapterInstance(selectedAlgorithm, graphCopy));
			}
		}
		return result;
	}

	/**
	 * Runs iterations on all candidates
	 * @param candidates
	 * @return list of best result graphs
	 */
	public static List<GraphRepresentation> runAlgorithmIterations(List<AbstractAdapter> candidates) {
		int counter = 1;
		int numberOfStartInits = Preferences.getInstance().getStartInitializations();
		// Buffer holds given number of best result sorted from worst to best
		PriorityQueue<GraphRepresentation> buffer = new PriorityQueue<>(Preferences.getInstance().getBestBufferSize() + 1,
				(x, y) -> (int) (y.getSavedScore() - x.getSavedScore()));
		boolean postprocesslimitWasReached = false;
		for (AbstractAdapter adapter : candidates) {
			int candidatesCount = Preferences.getInstance().getSelectedAlgorithm().equals(Algorithms.BEST) ?
					(Algorithms.values().length - 1) * numberOfStartInits : numberOfStartInits;
			LOGGER.info(MessageFormat.format("Running iterations for candidate {0} of {1}...", Integer.valueOf(counter), Integer.valueOf(candidatesCount)));
			boolean adapterResult = applyAdapter(adapter, buffer);
			postprocesslimitWasReached = postprocesslimitWasReached || adapterResult;
			counter++;
		}
		if (postprocesslimitWasReached) {
			LOGGER.info("Size post processing (Checking for overflowing and overlapping nodes) reached maximum number of iterations ("
					+ Preferences.getInstance().getMaxIterationsPostprocess()
					+ "). There might be overflowing/overlapping nodes in the result. Try using parameter '--max-iterations-postprocess' to increase this maximum.");
		}
		// We have to re-order the buffer here to the correct output order
		return buffer.stream().sorted((x, y) -> (int) (x.getSavedScore() - y.getSavedScore())).collect(Collectors.toList());
	}

	/**
	 * Runs iterations of given adapter
	 * @param adapter
	 * @param buffer with best results
	 * @return true if size-post processing limit was reached, false otherwise
	 */
	private static boolean applyAdapter(AbstractAdapter adapter, PriorityQueue<GraphRepresentation> buffer) {
		int iterations = Preferences.getInstance().getIterations();
		boolean totalResult = false;
		for (int i = 1; i <= iterations; i++) {
			if (i % 100 == 0) {
				LOGGER.info(MessageFormat.format("Running iteration {0} of {1}",
						Integer.valueOf(i), Integer.valueOf(iterations)));
			}
			boolean iterationResult = adapterOneIteration(adapter.getGraph().getRootGraph(), adapter);
			totalResult = totalResult || iterationResult;
			addToBuffer(adapter.getGraph(), buffer);
			Application.notifyController();
		}
		return totalResult;
	}

	/**
	 * Adds a copy of given graph to buffer, but only if the buffer is not full or if graph has better score than worst one in the buffer
	 * @param graph copy of this graph will be created
	 * @param buffer
	 */
	private static void addToBuffer(GraphRepresentation graph, PriorityQueue<GraphRepresentation> buffer) {
		int bufferSize = Preferences.getInstance().getBestBufferSize();
		long score = Score.evaluate(graph, Preferences.getInstance().getSelectedScores());
		if (buffer.size() < bufferSize || score < buffer.peek().getSavedScore()) {
			GraphRepresentation copy = graph.createCopy();
			copy.setSavedScore(score);
			buffer.add(copy);
		}
		if (buffer.size() > bufferSize) {
			buffer.remove();
		}
	}

	/**
	 * @param currentSubgraph
	 * @param adapter
	 * @return true if size-post processing limit was reached, false otherwise
	 */
	private static boolean adapterOneIteration(SubGraph currentSubgraph, AbstractAdapter adapter) {
		boolean postprocessResult = false;
		for (Entry<BoundingBox, List<Node>> entry : currentSubgraph.getSubgraphParts().entrySet()) {
			adapter.setPartAndLayout(entry.getKey(), entry.getValue());
			boolean localResult = PostProcessing.completePartPostProcess(entry.getKey(), entry.getValue());
			postprocessResult = postprocessResult || localResult;
		}
		for (SubGraph childSubgraph : currentSubgraph.getSubGraphs()) {
			boolean childResult = adapterOneIteration(childSubgraph, adapter);
			postprocessResult = postprocessResult || childResult;
		}
		return postprocessResult;
	}
}
