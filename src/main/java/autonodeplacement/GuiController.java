/**
 * Copyright 2020 Jan Juda
 * Created: 24. 4. 2020
 */
package autonodeplacement;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.logging.Logger;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.JTextComponent;

import autonodeplacement.algorithms.AbstractAdapter;
import autonodeplacement.algorithms.Algorithms;
import autonodeplacement.representation.GraphRepresentation;

/**
 * @author Jan Juda (xjudaj00)
 */
public class GuiController {
	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(GuiController.class.toString());

	/** View type details for file dialog */
	private static final String VIEW_TYPE_DETAIS = "viewTypeDetails";

	/** Preferences instance */
	private Preferences preferences;
	/** Holds gui controlled by this controller */
	private GUI gui;

	/** Listener for GUI actions (for buttons, comboboxes) */
	private ActionListener actionListener;
	/** Listener for GUI item selections (checkboxes) */
	private ItemListener itemListener;
	/** Listener for GUI change property events (text fields) */
	private PropertyChangeListener propertyChangeListener;
	/** Listener for GUI change events (spinners) */
	private ChangeListener changeListener;

	/** Holds number of completed steps (iterations) */
	private long currentProgress = 0;
	/** Holds number of steps (iterations) to completed */
	private long currentNumberOfOperations = 1;
	/** Progress bar of progress dialog. Variable used to set progress. */
	private JProgressBar progressBar;
	/** Progress dialog. Variable used to closed the dialog when operation is done. */
	private JDialog progressDialog;
	/** Hold reference to the layoutThred, when it is running. */
	private Thread layoutThread = null;

	/**
	 * Creates GuiController with an attached GUI.
	 */
	public GuiController() {
		preferences = Preferences.getInstance();
		createListeners();
		gui = new GUI(this);
		preferences.setForceDeterministicBehaviour(false); // This option is not supported with GUI.
		loadFromPreferences();
		progressBar = new JProgressBar(0, 100);
		progressBar.setStringPainted(true);
	}

	/**
	 * Creates controller's listeners
	 */
	private void createListeners() {
		actionListener = createActionListener();
		itemListener = createItemListener();
		propertyChangeListener = createPropertyListener();
		changeListener = createChangeListener();
	}

	/**
	 * @return new action listener for this controller
	 */
	private ActionListener createActionListener() {
		return e -> {
			JFileChooser dialog;
			switch (e.getActionCommand()) {
				case "layout":
					currentProgress = 0;
					currentNumberOfOperations = getCurrentNumberOfIterations();
					gui.getProgressBar().setValue(0);
					gui.getProgressBar().setString("0 %");
					layoutThread = new Thread(Application::applicationRun);
					layoutThread.start();
					showProgressDialog();
					break;
				case "selectMainInput":
					dialog = new JFileChooser(new File("."));
					dialog.setDialogTitle("Select main graph input file");
					// Select details view
					dialog.getActionMap().get(VIEW_TYPE_DETAIS).actionPerformed(null);
					if (dialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
						gui.getMainInputField().setText(dialog.getSelectedFile().getAbsolutePath());
					}
					break;
				case "selectTinycadInput":
					dialog = new JFileChooser(new File("."));
					dialog.setDialogTitle("Select TinyCAD input file");
					dialog.setFileFilter(new FileNameExtensionFilter("TinyCAD file", "dsn"));
					// Select details view
					dialog.getActionMap().get(VIEW_TYPE_DETAIS).actionPerformed(null);
					if (dialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
						gui.getTinycadInputField().setText(dialog.getSelectedFile().getAbsolutePath());
					}
					break;
				case "selectOutputPath":
					dialog = new JFileChooser(new File("."));
					dialog.setDialogTitle("Select output path/directory");
					dialog.setApproveButtonText("Select directory");
					dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					// Select details view
					dialog.getActionMap().get(VIEW_TYPE_DETAIS).actionPerformed(null);
					if (dialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
						gui.getTinycadOutputField().setText(dialog.getSelectedFile().getAbsolutePath());
					}
					break;
				case "algorithmChanged":
					Object source = e.getSource();
					if (source instanceof JComboBox<?>) {
						Object selected = ((JComboBox<?>) source).getSelectedItem();
						if (selected instanceof Algorithms) {
							Algorithms selectedAlgorithm = (Algorithms) selected;
							preferences.setSelectedAlgorithm(selectedAlgorithm);
							AbstractAdapter askAdapter = AbstractAdapter.getAdapterInstance(selectedAlgorithm, new GraphRepresentation());
							gui.getLimitLayoutSpaceCheckBox().setEnabled(askAdapter.isLimitingGraphSpaceSupported());
							if (!askAdapter.isLimitingGraphSpaceSupported()) {
								gui.getLimitLayoutSpaceCheckBox().setSelected(false);
							}
							gui.getLockOtherCheckBox().setEnabled(askAdapter.isLockingOtherNodesSupported());
							updateInfoArea();
						}
					}
					break;
				default:
					LOGGER.info("GuiController's action listener caught unsupported action command: " + e.getActionCommand());
					break;
			}
		};
	}

	/**
	 * @return new item listener for this controller
	 */
	private ItemListener createItemListener() {
		return e -> {
			Object source = e.getSource();
			if (source instanceof JCheckBox) {
				JCheckBox checkbox = (JCheckBox) source;
				boolean enabled = e.getStateChange() == ItemEvent.SELECTED;
				switch (checkbox.getActionCommand()) {
					case "generateOutput":
						preferences.setGenerateOutput(enabled);
						gui.getOutputComponents().forEach(x -> x.setEnabled(enabled));
						break;
					case "arrows":
						preferences.setArrowsAllowed(enabled);
						break;
					case "overridePin":
						preferences.setOverridePinTypes(enabled);
						break;
					case "timestamp":
						preferences.setTimeStamp(enabled);
						break;
					case "randomInit":
						preferences.setRandom(enabled);
						JSpinner startInitSpinner = gui.getInitSpinner();
						if(!enabled) {
							startInitSpinner.setValue(Integer.valueOf(1));
						}
						startInitSpinner.setEnabled(enabled);
						break;
					case "limitLayoutSpace":
						preferences.setLimitLayoutSpace(enabled);
						updateInfoArea();
						break;
					case "lockOtherNodes":
						preferences.setLockOtherNodes(enabled);
						break;
					case "prefPos":
						preferences.setParsePositional(enabled);
						break;
					case "postProcess":
						preferences.setSizePostprocessAllowed(enabled);
						gui.getMaxiterationsSpinner().setEnabled(enabled);
						updateInfoArea();
						break;
					default:
						// Maybe we have a score checkbox
						Score score = Arrays.stream(Score.values()).filter(x -> x.toString().equals(checkbox.getActionCommand())).findAny().orElse(null);
						if (score == null) {
							LOGGER.info("GuiController's item listener caught unsupported item (checkbox): " + checkbox.getActionCommand());
						} else {
							if (enabled) {
								Integer weight = (Integer) gui.getScoreWeightSpinners().get(score).getValue();
								preferences.getSelectedScores().put(score, weight);
								LOGGER.info("Score " + score.toString() + " is enabled and  has weight: " + weight);
							} else {
								preferences.getSelectedScores().remove(score);
								LOGGER.info("Score " + score.toString() + " is disabled");
							}
						}
						break;
				}
			} else {
				LOGGER.info("GuiController's item listener caught unsupported item: " + e.getItemSelectable().toString());
			}
		};
	}

	/**
	 * @return new property change listener for this controller
	 */
	private PropertyChangeListener createPropertyListener() {
		return e -> {
			switch (e.getPropertyName()) {
				case "mainInput":
					preferences.setInputFileName(e.getNewValue().toString());
					break;
				case "tinycadInput":
					preferences.setTinycadInputFileName(e.getNewValue().toString());
					break;
				case "tinycadOutput":
					preferences.setOutputPath(e.getNewValue().toString());
					break;
				case "tinycadOutputPrefix":
					preferences.setOutputFilePrefix(e.getNewValue().toString());
					break;
				default:
					LOGGER.info("GuiController's property change listener caught unsupported property: " + e.getPropertyName());
					break;
			}
		};
	}

	/**
	 * @return new change listener for this controller
	 */
	private ChangeListener createChangeListener() {
		return e -> {
			Object source = e.getSource();
			if (source instanceof JSpinner) {
				JSpinner sourceComponent = (JSpinner) source;
				SpinnerNumberModel model = (SpinnerNumberModel) sourceComponent.getModel();
				int value = model.getNumber().intValue();
				switch (sourceComponent.getName()) {
					case "iterations":
						preferences.setIterations(value);
						updateInfoArea();
						break;
					case "init":
						preferences.setStartInits(value);
						updateInfoArea();
						break;
					case "results":
						preferences.setBestBufferSize(value);
						updateInfoArea();
						break;
					case "maxiterations":
						preferences.setMaxIterationsPostprocess(value);
						break;
					case "width":
						preferences.setSizeX(value);
						break;
					case "height":
						preferences.setSizeY(value);
						break;
					case "nodeborder":
						preferences.setNodeBorder(value);
						break;
					case "subgraphborder":
						preferences.setSubgraphBorder(value);
						break;
					default:
						// Maybe we have a score spinner
						Score score = Arrays.stream(Score.values()).filter(x -> x.toString().equals(sourceComponent.getName())).findAny().orElse(null);
						if (score == null) {
							LOGGER.info("GuiController's change listener caught unsupported source: " + sourceComponent.getName());
						} else {
							// If the score isn't enabled, then changing weight doesn't reflect into model (preferences),
							// because preferences stores only enabled score rules.
							if (preferences.getSelectedScores().containsKey(score)) {
								preferences.getSelectedScores().put(score, Integer.valueOf(value));
								LOGGER.info("Score rule " + score.toString() + " has new weight of " + value);
							}
						}
						break;
				}
			}
		};
	}


	/**
	 * Adds listener for text field, which converts DocumentEvents to PropertyChangeEvents
	 * @param field
	 */
	public void addTextListener(JTextComponent field) {
		DocumentListener listener = new DocumentListener() {
			private int lastChange = 0;
			private int lastNotifiedChange = 0;

			@Override
			public void insertUpdate(DocumentEvent e) {
				changedUpdate(e);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				changedUpdate(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				lastChange++;
				SwingUtilities.invokeLater(() -> {
					if (lastNotifiedChange != lastChange) {
						lastNotifiedChange = lastChange;
						getPropertyChangeListener().propertyChange(new PropertyChangeEvent(field, field.getName(), "", field.getText()));
					}
				});
			}
		};
		field.getDocument().addDocumentListener(listener);
	}

	/**
	 * Updates content of information area and detects errors in configuration.
	 */
	private void updateInfoArea() {
		StringBuilder resultText = new StringBuilder();
		long numberOfIterations = getCurrentNumberOfIterations();
		resultText.append(MessageFormat.format("Total number of iterations: {0}.\n", Long.valueOf(numberOfIterations)));
		boolean configErrorPostProcessOrLimitLayout = !preferences.getSizePostprocessAllowed() && !preferences.getLimitLayoutSpace();
		boolean configErrorPostProcessBest = !preferences.getSizePostprocessAllowed() && preferences.getSelectedAlgorithm().equals(Algorithms.BEST);
		gui.getLayoutButton().setEnabled(!(configErrorPostProcessOrLimitLayout || configErrorPostProcessBest));
		if (configErrorPostProcessOrLimitLayout) {
			resultText.append("Configuration error: Either size post-processing or Limit layout space option must be enabled.\n");
		}
		if (configErrorPostProcessBest) {
			resultText.append("Configuration error: Size post-processing must be enabled when BEST algorithm mode is selected.\n");
		}
		if (preferences.getBestBufferSize() > numberOfIterations) {
			resultText.append("Warning: Number of results should be lower or equal to total number of iterations. Only " + numberOfIterations
					+ " results will be created.\n");
		}
		gui.getInfoTextArea().setText(resultText.toString());
	}

	/**
	 * @return current number of iterations
	 */
	private long getCurrentNumberOfIterations() {
		long iterations = ((long) preferences.getIterations()) * preferences.getStartInitializations();
		if (preferences.getSelectedAlgorithm().equals(Algorithms.BEST)) {
			iterations *= Algorithms.values().length - 1;
		}
		return iterations;
	}

	/**
	 * Updates progress bar by 1 step.
	 */
	public void stepTaken() {
		currentProgress += 1;
		int percent = (int) (currentProgress * 100 / currentNumberOfOperations);
		gui.getProgressBar().setValue(percent);
		gui.getProgressBar().setString(percent + " %");
		progressBar.setValue(percent);
		progressBar.setString(percent + " %");
		if (percent == 100) {
			progressDialog.dispose();
		}
	}

	/**
	 * Notifies GuiController, that error occurred in layout thread.
	 * @param message Message describing the error. Will be shown to user.
	 */
	public void errorOccurred(String message) {
		progressDialog.dispose();
		updateInfoArea();
		StringBuilder errorMsg = new StringBuilder();
		errorMsg.append(gui.getInfoTextArea().getText());
		errorMsg.append("Fatal error occurred in layout thread:\n");
		errorMsg.append(message);
		gui.getInfoTextArea().setText(errorMsg.toString());
	}

	/**
	 * Loads values from preferences and fills the GUI
	 */
	private void loadFromPreferences() {
		gui.getMainInputField().setText(preferences.getInputFileName());
		gui.getTinycadOutputField().setText(preferences.getOutputPath());
		gui.getTinycadInputField().setText(preferences.getTinycadInputFileName());
		gui.getTinycadOutputPrefixField().setText(preferences.getOutputFilePrefix());
		gui.getTinycadCheckbox().setSelected(preferences.getGenerateOutput());
		gui.getAlgorithmCombobox().setSelectedItem(preferences.getSelectedAlgorithm());
		gui.getIterationsSpinner().setValue(Integer.valueOf(preferences.getIterations()));
		gui.getInitSpinner().setValue(Integer.valueOf(preferences.getStartInitializations()));
		gui.getResultsSpinner().setValue(Integer.valueOf(preferences.getBestBufferSize()));
		gui.getMaxiterationsSpinner().setValue(Integer.valueOf(preferences.getMaxIterationsPostprocess()));
		gui.getWidthSpinner().setValue(Integer.valueOf(preferences.getWidth()));
		gui.getHeightSpinner().setValue(Integer.valueOf(preferences.getHeight()));
		gui.getNodeborderSpinner().setValue(Integer.valueOf(preferences.getNodeBorder()));
		gui.getSubgraphborderSpinner().setValue(Integer.valueOf(preferences.getSubgraphBorder()));
		gui.getRandomCheckBox().setSelected(preferences.getRandomInit());
		gui.getLimitLayoutSpaceCheckBox().setSelected(preferences.getLimitLayoutSpace());
		gui.getLockOtherCheckBox().setSelected(preferences.getLockOtherNodes());
		gui.getPrefPosCheckBox().setSelected(preferences.getParsePositional());
		gui.getPostprocessCheckBox().setSelected(preferences.getSizePostprocessAllowed());
		gui.getArrowsCheckBox().setSelected(preferences.getArrowsAllowed());
		gui.getPinOverrideCheckBox().setSelected(preferences.getOverridePinTypes());
		gui.getTimestampCheckbox().setSelected(preferences.timeStamp());
		gui.getShowresultCheckbox().setSelected(preferences.showResult());
		updateInfoArea();
	}

	/**
	 * @return action listener
	 */
	public ActionListener getActionListener() {
		return actionListener;
	}

	/**
	 * @return item listener
	 */
	public ItemListener getItemListener() {
		return itemListener;
	}

	/**
	 * @return property change listener
	 */
	public PropertyChangeListener getPropertyChangeListener() {
		return propertyChangeListener;
	}

	/**
	 * @return change listener
	 */
	public ChangeListener getChangeListener() {
		return changeListener;
	}

	/**
	 * Opens attached GUI
	 */
	public void openGui() {
		gui.setVisible(true);
	}

	/**
	 * Cancels layout operation.
	 */
	void cancelLayout() {
		layoutThread.stop();
		gui.getProgressBar().setValue(0);
		gui.getProgressBar().setString("Operation canceled");
	}

	/**
	 * Shows progress dialog for duration of layout operation.
	 */
	private void showProgressDialog() {
		progressDialog = new JDialog(gui, "Laying out a graph is in progress...", true);
		progressDialog.add(progressBar, BorderLayout.CENTER);
		progressDialog.setAlwaysOnTop(true);
		progressDialog.setBounds(0, 0, 230, 70);
		progressDialog.setLocationRelativeTo(gui);
		progressDialog.setResizable(false);
		progressDialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		progressDialog.addWindowListener(new WindowListener() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				cancelLayout();
				e.getWindow().dispose();
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				// Nothing to do here
			}

			@Override
			public void windowActivated(WindowEvent e) {
				// Nothing to do here
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// Nothing to do here
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// Nothing to do here
			}

			@Override
			public void windowIconified(WindowEvent e) {
				// Nothing to do here
			}

			@Override
			public void windowOpened(WindowEvent e) {
				// Nothing to do here
			}
		});
		progressDialog.setVisible(true);
	}
}
