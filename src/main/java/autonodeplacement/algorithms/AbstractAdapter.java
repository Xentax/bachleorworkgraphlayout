/**
 * Copyright 2020 Jan Juda
 * Created: 23. 2. 2020
 */
package autonodeplacement.algorithms;

import java.util.List;

import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import javafx.geometry.BoundingBox;

/**
 * Abstract class for all layout algorithm adapters
 * @author Jan Juda (xjudaj00)
 */
public abstract class AbstractAdapter {

	/** Graph to layout */
	protected GraphRepresentation graph;

	/** Default timeout of 2000 ms */
	protected int timeout = 2000;
	/** If not null, then allowedNodes will be laid out only in this part of a graph space */
	protected BoundingBox part = null;
	/** Only this nodes will be laid out in the graph space (other nodes should be locked) */
	protected List<Node> allowedNodes = null;

	/**
	 * This constructor loads the diagram into adapted algorithm. Subclasses must call this in their constructor.
	 * @param graph to load and lay out
	 */
	public AbstractAdapter(GraphRepresentation graph) {
		this.graph = graph;
	}

	/**
	 * This method will run the algorithm with already loaded graph. This is useful for multiple iterations. <br>
	 * Method {@link AbstractAdapter#loadAndLayout(GraphRepresentation)} must be run first! <br>
	 * Reaction to {@link Preferences#getLimitLayoutSpace()} should be supported.
	 */
	public abstract void layout();

	/**
	 * Sets timeout of algorithm
	 * @param newTimeout
	 */
	public void setTimeout(int newTimeout) {
		timeout = newTimeout;
	}

	/**
	 * @return graph for this adapter
	 */
	public GraphRepresentation getGraph() {
		return graph;
	}

	/**
	 * Sets part of graph to layout and performs {@link AbstractAdapter#layout()}, if part is null then whole graph will be laid out
	 * @param part of the graph space, nodes will have only assigned positions inside this space
	 * @param allowedNodes only position of these nodes
	 */
	public void setPartAndLayout(BoundingBox part, List<Node> allowedNodes) {
		setPart(part, allowedNodes);
		layout();
	}

	/**
	 * Sets part of graph to layout, if part is null then whole graph will be laid out
	 * @param part of the graph space, nodes will have only assigned positions inside this space
	 * @param allowedNodes only position of these nodes
	 */
	public void setPart(BoundingBox part, List<Node> allowedNodes) {
		this.part = part;
		this.allowedNodes = allowedNodes;
		updateGraph();
	}

	/**
	 * Update the layout algorithm to match the set graph part
	 * @param allowedNodes
	 */
	protected abstract void updateGraph();

	/**
	 * @return true if locking other nodes (nodes not belonging to the currently processing subgraph part) is supported in the
	 *         {@link AbstractAdapter#layout()} method.
	 */
	public abstract boolean isLockingOtherNodesSupported();

	/**
	 * @return true if limiting layout space for the algorithm to the part space (from whole graph space) is supported in the
	 *         {@link AbstractAdapter#layout()} method.
	 */
	public abstract boolean isLimitingGraphSpaceSupported();

	/**
	 * @param algorithm selected algorithm, this does NOT handle {@link Algorithms#BEST}!
	 * @param graph
	 * @return instance of a selected algorithm adapter for given graph
	 */
	public static AbstractAdapter getAdapterInstance(Algorithms algorithm, GraphRepresentation graph) {
		graph.setLaidOutBy(algorithm);
		switch (algorithm) {
			case KK:
				return new AdapterKK(graph);
			case ISOM:
				return new AdapterISOM(graph);
			case FR:
			default:
				return new AdapterFR(graph);
		}
	}
}
