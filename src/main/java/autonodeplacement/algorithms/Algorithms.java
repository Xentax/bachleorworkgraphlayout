/**
 * Copyright 2020 Jan Juda
 * Created: 23. 2. 2020
 */
package autonodeplacement.algorithms;

/**
 * Enumeration of supported algorithms. <br>
 * Each algorithm can have brief description.
 * @author Jan Juda (xjudaj00)
 */
public enum Algorithms {
	/** Fruchterman-Reingold */
	FR("Fruchterman-Reingold force directed algorithm\n      Has best results from all possible options with 'Lock other nodes' and 'Limit layout space' both enabled."),
	/** Kamada-Kawai */
	KK("Kamada-Kawai algorithm\n The slowest algorithm. Has quite pretty results with higher number of iterations."),
	/** ISOM algorithm */
	ISOM("ISOM (Implementation of Self-Organizing Maps) algorithm\n      The fastest algorithm. Good results with 'Limit layout space' enabled."),
	/** Automatically choose algorithm with best result */
	BEST("Automatically choose algorithm with best result.\n      Multiplies number of total iterations.");

	/** Brief description of an algorithm */
	private final String description;
	
	/**
	 * Constructor
	 * @param description of an algorithm
	 */
	private Algorithms(String description) {
		this.description = description;
	}
	
	/**
	 * @return description of this algorithm
	 */
	public final String getDescription() {
		return description;
	}
}
