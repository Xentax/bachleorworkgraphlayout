/**
 * Copyright 2020 Jan Juda
 * Created: 23. 2. 2020
 */
package autonodeplacement.algorithms;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import autonodeplacement.representation.Edge;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import edu.uci.ics.jung.algorithms.layout.KKLayout;

/**
 * Adapter of Kamada-Kawai algorithm
 * @author Jan Juda (xjudaj00)
 */
public class AdapterKK extends AbstractAdapter {

	/** Internal layout, adaptee */
	private KKLayout<Node, Edge> layout;

	/**
	 * Constructs and initializes adapter with given graph
	 * @param graph
	 */
	public AdapterKK(GraphRepresentation graph) {
		super(graph);
		layout = new KKLayout<>(graph.toJUNGGraph());
		layout.setSize(graph.getSize());
		layout.setInitializer(node -> (Point2D) node.getPosition().clone());
		layout.setAdjustForGravity(false);
		layout.initialize();
	}

	@Override
	public void layout() {
		// Prepare for part
		if (allowedNodes == null) {
			allowedNodes = new ArrayList<>(graph.getNodes());
		}
		if (part != null) {
			layout.setInitializer(node -> (Point2D) node.getPosition().clone());
			for (Node node : graph.getNodes()) {
				layout.setLocation(node, node.getPosition());
			}
			layout.initialize();
		}
		layout.reset();
		long timeStart = System.currentTimeMillis();
		while (System.currentTimeMillis() - timeStart < timeout && !layout.done()) {
			layout.step();
		}
		// Copy result
		allowedNodes.forEach(n -> n.setPosition(layout.transform(n)));
	}

	@Override
	protected void updateGraph() {
		layout.setGraph(graph.toJUNGGraph(allowedNodes, true));
	}

	@Override
	public boolean isLimitingGraphSpaceSupported() {
		// Algorithm works on reducing "force" or "tension" between nodes and this feature actually makes
		// that some nodes are outside of the graph space, witch creates too big forces, so it will break
		// iterations of the algorithm (it will tend to diverging, which algorithm does not allow).
		return false;
	}

	@Override
	public boolean isLockingOtherNodesSupported() {
		// Algorithm works on reducing "force" or "tension" between nodes and this feature limits
		// the exchange of the "force" too much, so it will break iterations of the algorithm.
		return false;
	}
}
