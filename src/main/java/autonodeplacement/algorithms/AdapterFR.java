/**
 * Copyright 2020 Jan Juda
 * Created: 23. 2. 2020
 */
package autonodeplacement.algorithms;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import autonodeplacement.Preferences;
import autonodeplacement.representation.Edge;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import edu.uci.ics.jung.algorithms.layout.FRLayout;

/**
 * Adapter of Fruchterman-Reingold force directed algorithm
 * @author Jan Juda (xjudaj00)
 */
public class AdapterFR extends AbstractAdapter {

	/** Internal layout, adaptee */
	private FRLayout<Node, Edge> layout;

	/**
	 * Constructs and initializes adapter with given graph
	 * @param graph
	 */
	public AdapterFR(GraphRepresentation graph) {
		super(graph);
		layout = new FRLayout<>(graph.toJUNGGraph());
		layout.setSize(graph.getSize());
		layout.setInitializer(Node::getPosition);
		layout.initialize();
	}

	@Override
	public void layout() {
		List<Node> lockedNodes = null;
		Point2D shift = null;
		// Prepare for part
		if (allowedNodes == null) {
			allowedNodes = new ArrayList<>(graph.getNodes());
		}
		if (part != null) {
			if (Preferences.getInstance().getLimitLayoutSpace()) {
				shift = new Point2D.Double(-1 * part.getMinX(), -1 * part.getMinY());
				graph.getRootGraph().shift(shift);
				layout.setSize(new Dimension((int) part.getWidth(), (int) part.getHeight()));
			}
			layout.setInitializer(Node::getPosition);
			for (Node node : graph.getNodes()) {
				layout.setLocation(node, node.getPosition());
			}
			if (Preferences.getInstance().getLockOtherNodes()) {
				lockedNodes = new ArrayList<>(graph.getNodes());
				lockedNodes.removeAll(allowedNodes);
				for (Node node : lockedNodes) {
					layout.lock(node, true);
				}
			}
		}
		layout.reset();
		long timeStart = System.currentTimeMillis();
		while (System.currentTimeMillis() - timeStart < timeout && !layout.done()) {
			layout.step();
		}
		// Copy result
		allowedNodes.forEach(n -> n.setPosition(layout.transform(n)));
		// Shift back
		if (shift != null) {
			shift.setLocation(shift.getX() * -1, shift.getY() * -1);
			graph.getRootGraph().shift(shift);
		}
		// Unlock locked nodes again
		if (lockedNodes != null) {
			for (Node node : lockedNodes) {
				layout.lock(node, false);
			}
		}
	}

	@Override
	protected void updateGraph() {
		layout.setGraph(graph.toJUNGGraph(allowedNodes, true));
	}

	@Override
	public boolean isLimitingGraphSpaceSupported() {
		// algorithm fully supports this feature
		return true;
	}

	@Override
	public boolean isLockingOtherNodesSupported() {
		// algorithm fully supports this feature
		return true;
	}
}
