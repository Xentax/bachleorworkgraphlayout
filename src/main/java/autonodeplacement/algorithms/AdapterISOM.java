/**
 * Copyright 2020 Jan Juda
 * Created: 4. 3. 2020
 */
package autonodeplacement.algorithms;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import autonodeplacement.Preferences;
import autonodeplacement.representation.Edge;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import edu.uci.ics.jung.algorithms.layout.ISOMLayout;

/**
 * Adapter of ISOM algorithm
 * @author Jan Juda (xjudaj00)
 */
public class AdapterISOM extends AbstractAdapter {

	/** Internal layout, adaptee */
	private ISOMLayout<Node, Edge> layout;

	/**
	 * Constructs and initializes adapter with given graph
	 * @param graph
	 */
	public AdapterISOM(GraphRepresentation graph) {
		super(graph);
		layout = new ISOMLayout<>(graph.toJUNGGraph());
		layout.setSize(graph.getSize());
		layout.initialize();
		layout.setInitializer(Node::getPosition);
	}

	@Override
	public void layout() {
		List<Node> lockedNodes = null;
		Point2D shift = null;
		// Prepare for part
		if (allowedNodes == null) {
			allowedNodes = new ArrayList<>(graph.getNodes());
		}
		if (part != null) {
			if (Preferences.getInstance().getLimitLayoutSpace()) {
				shift = new Point2D.Double(-1 * part.getMinX(), -1 * part.getMinY());
				graph.getRootGraph().shift(shift);
				layout.setSize(new Dimension((int) part.getWidth(), (int) part.getHeight()));
			}
			layout.setInitializer(Node::getPosition);
			for (Node node : graph.getNodes()) {
				layout.setLocation(node, node.getPosition());
			}
			if (Preferences.getInstance().getLockOtherNodes()) {
				lockedNodes = new ArrayList<>(graph.getNodes());
				lockedNodes.removeAll(allowedNodes);
				for (Node node : lockedNodes) {
					layout.lock(node, true);
				}
			}
		}
		layout.reset();
		long timeStart = System.currentTimeMillis();
		while (System.currentTimeMillis() - timeStart < timeout && !layout.done()) {
			layout.step();
		}
		// Copy result
		allowedNodes.forEach(n -> n.setPosition(layout.transform(n)));
		// Shift back
		if (shift != null) {
			shift.setLocation(shift.getX() * -1, shift.getY() * -1);
			graph.getRootGraph().shift(shift);
		}
		// Unlock locked nodes again
		if (lockedNodes != null) {
			for (Node node : lockedNodes) {
				layout.lock(node, false);
			}
		}
	}

	@Override
	protected void updateGraph() {
		layout.setGraph(graph.toJUNGGraph(allowedNodes, true));
	}

	@Override
	public boolean isLimitingGraphSpaceSupported() {
		// Algorithm fully supports this feature
		return true;
	}

	@Override
	public boolean isLockingOtherNodesSupported() {
		// Algorithm doesn't support locking nodes in general.
		return false;
	}
}
