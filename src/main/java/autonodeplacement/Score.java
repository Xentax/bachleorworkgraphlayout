/**
 * Copyright 2020 Jan Juda
 * Created: 12. 4. 2020
 */
package autonodeplacement;

import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.sun.javafx.geom.Line2D;

import autonodeplacement.representation.Edge;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import javafx.geometry.BoundingBox;

/**
 * Class representing graph score with different score rules. <br>
 * Score is designed as "fault points", meaning each fault in the graph against the score rule is punished by these points.<br>
 * In other words LESS score points is BETTER. <br>
 * Has also static methods to calculate score for a graph.
 * @author Jan Juda (xjudaj00)
 */
public enum Score {
	/** Score rule, that counts edges crossing a node. */
	CEN("Score rule Crossing Edge x Node\n      Crossings edge through a node are counted towards score."),
	/** Score rule, that counts edge crossings. */
	CEE("Score rule Crossing Edge x Edge\n      Crossings of 2 edges are counted towards score."),
	/** Score rule, that all edges must point from left to the right. */
	LtR("Score rule Left to Right\n      This rule demands, that edges point from Left to Right. Edges in opposite direction are counted towards score."),
	/** Score rule, that all edges must point from right to the left. */
	RtL("Score rule Right to Left\n      This rule demands, that edges point from Right to Left. Edges in opposite direction are counted towards score."),
	/** Score rule, that all edges must point from top to bottom */
	TtB("Score rule Top to Bottom\n      This rule demands, that edges point from Top to Bottom. Edges in opposite direction are counted towards score."),
	/** Score rule, that all edges must point from bottom to top */
	BtT("Score rule Bottom to Top\n      This rule demands, that edges point from Bottom to Top. Edges in opposite direction are counted towards score.");



	/** Brief description of the score rule */
	private final String description;

	/**
	 * Creates score rule with description
	 * @param description
	 */
	private Score(String description) {
		this.description = description;
	}

	/**
	 * @return the description of the score rule
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param graph to evaluate
	 * @param scoreRules map of selected score rules with their weights
	 * @return evaluates given graph by selected score rules and returns the score
	 */
	public static long evaluate(GraphRepresentation graph, Map<Score, Integer> scoreRules) {
		long score = 0;
		for (Entry<Score, Integer> entry : scoreRules.entrySet()) {
			Score scoreRule = entry.getKey();
			int weight = entry.getValue().intValue();
			switch(scoreRule) {
				case BtT:
				case LtR:
				case RtL:
				case TtB:
					score += weight * evaluateEdgeDirection(graph, scoreRule);
					break;
				case CEE:
					score += weight * evaluateCrossingsEdges(graph);
					break;
				case CEN:
					score += weight * evaluateCrossingsNode(graph);
					break;
			}
		}
		return score;
	}

	/**
	 * @param graph to evaluate
	 * @return number of edges crossing nodes. Each edge X node crossing is counted.
	 */
	private static long evaluateCrossingsNode(GraphRepresentation graph) {
		long score = 0;
		for (Edge edge : graph.getEdges()) {
			// Edge X Node
			for (Node node : graph.getNodes()) {
				if (node != edge.getStartNode() && node != edge.getEndNode()) {
					if (edgeIntersectsNode(edge, node)) {
						score += 1;
					}
				}
			}
		}
		return score;
	}

	/**
	 * @param edge
	 * @param node
	 * @return true, if edge intersects or is completely inside a rectangular bounds of a node
	 */
	private static boolean edgeIntersectsNode(Edge edge, Node node) {
		Point2D startPos = edge.getStartNode().getPosition();
		Point2D endPos = edge.getEndNode().getPosition();
		BoundingBox box = node.getBoundingBox(0, false);
		// Short check for edge being completely inside a node. It is enough to check only one end,
		// because other cases will be checked further
		if (box.getMinX() < startPos.getX() && startPos.getX() < box.getMaxX() &&
				box.getMinY() < startPos.getY() && startPos.getY() < box.getMaxY()) {
			return true;
		}
		Line2D line = new Line2D((float) startPos.getX(), (float) startPos.getY(), (float) endPos.getX(), (float) endPos.getY());
		// Now check intersections with each side of a rectangle. If any found, return true.
		if (line.intersectsLine((float) box.getMinX(), (float) box.getMinY(), (float) box.getMaxX(), (float) box.getMinY())) {
			return true; // Upper side
		}
		if (line.intersectsLine((float) box.getMinX(), (float) box.getMaxY(), (float) box.getMaxX(), (float) box.getMaxY())) {
			return true; // Lower side
		}
		if (line.intersectsLine((float) box.getMinX(), (float) box.getMinY(), (float) box.getMinX(), (float) box.getMaxY())) {
			return true; // Left side
		}
		if (line.intersectsLine((float) box.getMaxX(), (float) box.getMinY(), (float) box.getMaxX(), (float) box.getMaxY())) {
			return true; // Right side
		}
		return false;
	}

	/**
	 * @param graph to evaluate
	 * @return number of edges crossing each other. Each edge X edge crossing is counted once.
	 */
	private static long evaluateCrossingsEdges(GraphRepresentation graph) {
		long score = 0;
		Set<String> edgeCrossings = new HashSet<>();
		for (Edge edge : graph.getEdges()) {
			Point2D startPos = edge.getStartNode().getPosition();
			Point2D endPos = edge.getEndNode().getPosition();
			// Edge X Edge
			Line2D firstLine = new Line2D((float) startPos.getX(), (float) startPos.getY(), (float) endPos.getX(), (float) endPos.getY());
			for (Edge otherEdge : graph.getEdges()) {
				// We need to somehow check, if we already counted this crossing, because if we didn't, then we would count all crossings twice
				String[] idArray = { edge.toString(), otherEdge.toString() };
				Arrays.sort(idArray);
				String crossId = String.join("_X_", idArray);
				// We create ID for the crossing and check it
				if (edge != otherEdge && !edgeCrossings.contains(crossId) && edge.getStartNode() != otherEdge.getStartNode()
						&& edge.getEndNode() != otherEdge.getEndNode() && edge.getStartNode() != otherEdge.getEndNode()
						&& edge.getEndNode() != otherEdge.getStartNode()) {
					startPos = otherEdge.getStartNode().getPosition();
					endPos = otherEdge.getEndNode().getPosition();
					Line2D otherLine = new Line2D((float) startPos.getX(), (float) startPos.getY(), (float) endPos.getX(), (float) endPos.getY());
					if (firstLine.intersectsLine(otherLine)) {
						score += 1;
						edgeCrossings.add(crossId);
					}
				}
			}
		}
		return score;
	}

	/**
	 * @param graph to evaluate
	 * @param scoreRule this determines the direction of the edges. If rule is LtR than each edge with direction from right to left is counted.
	 * @return number of edges in bad direction according to the scoreRule.
	 */
	private static long evaluateEdgeDirection(GraphRepresentation graph, Score scoreRule) {
		long score = 0;
		boolean horizontal = scoreRule == LtR || scoreRule == RtL;
		boolean endCantBeLess = scoreRule == LtR || scoreRule == TtB;
		for (Edge edge : graph.getEdges()) {
			Point2D position = edge.getStartNode().getPosition();
			double startCoord = horizontal ? position.getX() : position.getY();
			position = edge.getEndNode().getPosition();
			double endCoord = horizontal ? position.getX() : position.getY();
			if (endCantBeLess) {
				if (endCoord < startCoord) {
					score += 1;
				}
			} else if (startCoord < endCoord) {
				score += 1;
			}
		}
		return score;
	}
}
