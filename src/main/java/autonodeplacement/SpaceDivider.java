/**
 * Copyright 2020 Jan Juda
 * Created: 25. 3. 2020
 */
package autonodeplacement;

import static autonodeplacement.OutputGenerator.formatInteger;

import java.awt.geom.Point2D;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import autonodeplacement.representation.Node;
import autonodeplacement.representation.Node.Location;
import autonodeplacement.representation.SubGraph;
import javafx.geometry.BoundingBox;

/**
 * Class for dividing graph space for subgraphs
 * @author Jan Juda (xjudaj00)
 */
public class SpaceDivider {

	/** Class logger */
	private static final Logger LOGGER = Logger.getLogger(SpaceDivider.class.toString());

	/** Default number of rows in space divider */
	private static final int DEFAULT_ROWS = 3;
	/** Default number of columns in space divider */
	private static final int DEFAULT_COLUMNS = 3;
	/** Default index of middle row/column during filling of the space (space cannot shrink during filling) */
	private static final int MIDDLE_INDEX = 1;
	/**
	 * Default index of spread row/column (= whole row left-to-right or whole column top-to-bottom) during filling of the space
	 * (space cannot shrink during filling)
	 */
	private static final int SPREAD_INDEX = 2;

	/** Number of rows in the space */
	private int rows = 0;
	/** Number of columns in the space */
	private int columns = 0;
	/** Space made of Cells. It is 2D map of rows and columns. <br>
	 * First (outer) list contains rows, second (inner) list contains cells. */
	private List<List<Cell>> space = new ArrayList<>();

	/**
	 * Creates default empty 3x3 space divider.
	 */
	private SpaceDivider() {
		columns = DEFAULT_COLUMNS;
		for (int i = 0; i < DEFAULT_ROWS; i++) {
			addRow(0); // Index doesn't matter now
		}
	}

	/**
	 * Creates 3x3 space divider for given nodes and their required sides. <br>
	 * Result is an abstract divided space.
	 * @param nodesToDevideFor cannot contain location conflicts! All nodes must have same parent.
	 */
	public SpaceDivider(Map<Node, Set<Location>> nodesToDevideFor) {
		this();
		if (!Preferences.getInstance().getParsePositional()) {
			// If no positional parsing, then all nodes belong to the middle.
			getCell(MIDDLE_INDEX, MIDDLE_INDEX).nodes.addAll(nodesToDevideFor.keySet());
			return;
		}
		for(Entry<Node, Set<Location>> entry : nodesToDevideFor.entrySet()) {
			Node node = entry.getKey();
			Set<Location> locations = entry.getValue();
			if (locations.size() == 4) {
				getCell(MIDDLE_INDEX, MIDDLE_INDEX).nodes.clear();
				getCell(MIDDLE_INDEX, MIDDLE_INDEX).nodes.add(node);
				return; // If there is an allSider, then there no more nodes.
			} else if (locations.size() == 3) {
				Location blockedLocation = getBlockLocationFromTriple(locations);
				Cell targetCell;
				switch (blockedLocation) {
					case TOP:
						targetCell = getCell(0, MIDDLE_INDEX);
						targetCell.nodes.add(node);
						spreadCellToRow(targetCell, 0);
						break;
					case BOTTOM:
						targetCell = getCell(maxRow(), MIDDLE_INDEX);
						targetCell.nodes.add(node);
						spreadCellToRow(targetCell, 0);
						break;
					case LEFT:
						targetCell = getCell(MIDDLE_INDEX, 0);
						targetCell.nodes.add(node);
						spreadCellToColumn(targetCell, 0);
						break;
					case RIGHT:
						targetCell = getCell(MIDDLE_INDEX, maxColumn());
						targetCell.nodes.add(node);
						spreadCellToColumn(targetCell, 0);
						break;
					default:
					case NONE:
						break;
				}
			} else if (locations.size() == 2) {
				if(locations.contains(locations.iterator().next().complementary())) {
					// Do we have left & right or top & bottom?
					if(locations.contains(Location.TOP)) {
						getSpreadColumnCell().nodes.add(node);
					} else {
						getSpreadRowCell().nodes.add(node);
					}
				} else {
					// Else we have an corner!
					int rowIndex = locations.contains(Location.TOP) ? 0 : maxRow();
					int columnIndex = locations.contains(Location.LEFT) ? 0 : maxRow();
					getCell(rowIndex, columnIndex).nodes.clear(); // because corner can have only 1
					getCell(rowIndex, columnIndex).nodes.add(node);
				}
			} else if (locations.size() == 1) {
				Location location = locations.iterator().next();
				switch (location) {
					case TOP:
						getCell(0, MIDDLE_INDEX).nodes.add(node);
						break;
					case BOTTOM:
						getCell(maxRow(), MIDDLE_INDEX).nodes.add(node);
						break;
					case LEFT:
						getCell(MIDDLE_INDEX, 0).nodes.add(node);
						break;
					case RIGHT:
						getCell(MIDDLE_INDEX, maxColumn()).nodes.add(node);
						break;
					default:
					case NONE:
						break;
				}
			} else {
				getCell(MIDDLE_INDEX, MIDDLE_INDEX).nodes.add(node);
			}
		}
	}

	/**
	 * Takes prepared abstract space and applies it on the given space with exact size and position. <br>
	 * SubGraphs will have assigned a size.
	 * @param spaceBox space (size and position) for nodes
	 * @return map consisting of a bounding boxes defining a space and theirs lists of nodes to be layouted in them
	 */
	public Map<BoundingBox, List<Node>> applyToRealSpace(BoundingBox spaceBox) {
		reduceSpace(spaceBox);
		// Let's divide the space and assign cell sizes
		double heightRatio = spaceBox.getHeight() / Math.max(getTotalRowRequiredHeight(), 1);
		double widthRatio = spaceBox.getWidth() / Math.max(getTotalColumnRequiredWidth(), 1);
		double totalRequiredArea = Math.max(getTotalRequiredArea(), 1);
		double heightAreaRatio = spaceBox.getHeight() / totalRequiredArea;
		double widthAreaRatio = spaceBox.getWidth() / totalRequiredArea;
		if (heightRatio < 1) {
			LOGGER.warning(MessageFormat.format("Height of box {0} is too small for it''s content, content will be shrunk!", spaceBox));
		}
		if (widthRatio < 1) {
			LOGGER.warning(MessageFormat.format("Width of box {0} is too small for it''s content, content will be shrunk!", spaceBox));
		}
		if (spaceBox.getHeight() * spaceBox.getWidth() < totalRequiredArea) {
			LOGGER.warning(MessageFormat.format("Area of box {0} is too small for it''s content, content will be shrunk!", spaceBox));
		}
		for (int row = 0; row < rows; row++) {
			double rowHeight = (getRowMinHeight(row) * heightRatio + (getRowMinArea(row) * heightAreaRatio)) / 2;
			for (int column = 0; column < columns; column++) {
				Cell cell = getCell(row, column);
				if (cell.wholeColumn && row != 0) {
					continue; // this column is already processed, so skip to the next one
				}
				cell.height = rowHeight;
				cell.width = (getColumnMinWidth(column) * widthRatio + (getColumnMinArea(column) * widthAreaRatio)) / 2;
				if (cell.wholeRow) {
					cell.width = spaceBox.getWidth();
					break; // this row is complete processed by this cell
				} else if (cell.wholeColumn) {
					cell.height = spaceBox.getHeight();
				}
			}
		}
		// Now we can perform initial layouting of the content of the cells
		assignSpace(spaceBox);
		// After reduction of space, there can still be empty cells, so we need to fill them.
		if (fillEmptyCells()) {
			// Now, if any cell changed, we have to re-assign space
			assignSpace(spaceBox);
		}
		// Finally get the result
		Map<BoundingBox, List<Node>> resultSpaceMap = new LinkedHashMap<>();
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				Cell cell = getCell(row, column);
				if (cell.wholeRow && column != 0) {
					break; // this row is processed in it's first cell
				}
				if (cell.wholeColumn && row != 0) {
					continue; // this column is proccessed in it's first cell
				}
				if (!cell.isEmpty()) {
					Point2D position = getCellPosition(row, column, spaceBox);
					resultSpaceMap.put(new BoundingBox(position.getX(), position.getY(), cell.width, cell.height), cell.nodes);
				}
			}
		}
		return resultSpaceMap;
	}


	/**
	 * Fills (distributes) space of empty cells to their neighbor cells.
	 * @return true, if size of any cell was changed
	 */
	private boolean fillEmptyCells() {
		// Works in 3 steps:
		// 1st assign needed width or height
		boolean result = false;
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				Cell cell = getCell(row, column);
				if (!cell.isEmpty()) {
					continue;
				}
				List<Cell> neighbors = getCellNeighbors(row, column, false);
				neighbors.removeIf(x -> x.heightNeeded == 0 || x.widthNeeded == 0);
				neighbors.removeIf(x -> (x.heightNeeded > 0 && x.width != cell.width) || (x.widthNeeded > 0 && x.height != 0));
				List<Cell> neighborsWithOnlyChance = new ArrayList<>(neighbors);
				List<Cell> savedNeighbors = null;
				neighborsWithOnlyChance.removeIf(this::hasMoreThanOneEmptyNeighbor);
				if (!neighborsWithOnlyChance.isEmpty()) {
					savedNeighbors = neighbors;
					neighbors = neighborsWithOnlyChance;
				}
				double topDifference = 0;
				boolean width = true;
				Cell selectedNeighbor = null;
				for (Cell neighbor : neighbors) {
					if (neighbor.heightNeeded > topDifference) {
						topDifference = neighbor.heightNeeded;
						selectedNeighbor = neighbor;
						width = false;
					}
					if (neighbor.widthNeeded > topDifference) {
						topDifference = neighbor.widthNeeded;
						selectedNeighbor = neighbor;
						width = true;
					}
				}
				if (selectedNeighbor == null) {
					continue;
				}
				result = true;
				cell.resolved = true;
				double rest = increaseCell(selectedNeighbor, width, width ? cell.width : cell.height, false);
				finishTheSecondNeighbor(neighbors, savedNeighbors, cell, selectedNeighbor, width, rest, false);
			}
		}
		// 2nd assign needed area
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				Cell cell = getCell(row, column);
				if (!cell.isEmpty() || cell.resolved) {
					continue;
				}
				List<Cell> neighbors = getCellNeighbors(row, column, false);
				final int finalRow = row;
				final int finalColumn = column;
				// Remove neighbors with no needed area or neighbors with not matching sizes
				neighbors.removeIf(x -> x.areaNeeded == 0 || (getCellRow(x) == finalRow && cell.height != x.height)
						|| (getCellColumn(x) == finalColumn && cell.width != x.width));
				List<Cell> neighborsWithOnlyChance = new ArrayList<>(neighbors);
				List<Cell> savedNeighbors = null;
				neighborsWithOnlyChance.removeIf(this::hasMoreThanOneEmptyNeighbor);
				if (!neighborsWithOnlyChance.isEmpty()) {
					savedNeighbors = neighbors;
					neighbors = neighborsWithOnlyChance;
				}
				Cell selectedNeighbor = null;
				for (Cell neighbor : neighbors) {
					if (selectedNeighbor == null || neighbor.areaNeeded > selectedNeighbor.areaNeeded) {
						selectedNeighbor = neighbor;
					}
				}
				if (selectedNeighbor == null) {
					continue;
				}
				result = true;
				cell.resolved = true;
				boolean width = getCellRow(selectedNeighbor) == row;
				double rest = increaseCell(selectedNeighbor, width, width ? cell.width : cell.height, true);
				finishTheSecondNeighbor(neighbors, savedNeighbors, cell, selectedNeighbor, width, rest, true);
			}
		}
		// 3rd assign the rest, to the biggest (area of cell's nodes) cell
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				Cell cell = getCell(row, column);
				if (!cell.isEmpty() || cell.resolved) {
					continue;
				}
				List<Cell> neighbors = getCellNeighbors(row, column, false);
				// Remove neighbors with not matching sizes
				final int finalRow = row;
				final int finalColumn = column;
				neighbors.removeIf(x -> (getCellRow(x) == finalRow && cell.height != x.height) || (getCellColumn(x) == finalColumn && cell.width != x.width));
				Cell selectedNeighbor = neighbors.stream().sorted((x, y) -> (int) (y.getAllNodesArea() - x.getAllNodesArea())).findFirst().orElse(null);
				if (selectedNeighbor == null) {
					LOGGER.info(MessageFormat.format(
							"Filling empty cell during space division failed: No matching neighbor found. This will result in block of unused empty space (size {0}x{1}px) in the output.",
							formatInteger((int) cell.width), formatInteger((int) cell.height)));
					continue;
				}
				result = true;
				cell.resolved = true;
				if (getCellRow(selectedNeighbor) == row) {
					selectedNeighbor.width += cell.width;
					cell.width = 0;
				} else {
					selectedNeighbor.height += cell.height;
					cell.height = 0;
				}
			}
		}
		return result;
	}

	/**
	 * Uses rest size for the second neighbor on the selected side. This is part of filling empty Cells.
	 * @param neighbors
	 * @param savedNeighbors
	 * @param cell
	 * @param selectedNeighbor
	 * @param width
	 * @param rest
	 * @param area
	 */
	private static void finishTheSecondNeighbor(List<Cell> neighbors, List<Cell> savedNeighbors, Cell cell, Cell selectedNeighbor, boolean width, double rest,
			boolean area) {
		neighbors.remove(selectedNeighbor);
		if (width) {
			cell.width = 0;
		} else {
			cell.height = 0;
		}
		final boolean widthFinal = width;
		// If we got some rest width/height to spend, there can be another neighbor on the other side!
		Cell secondNeighbor = neighbors.stream().filter(x -> widthFinal ? x.height == cell.height : x.width == cell.width).findAny().orElse(null);
		if (secondNeighbor == null && savedNeighbors != null) {
			savedNeighbors.remove(selectedNeighbor);
			secondNeighbor = savedNeighbors.stream().filter(x -> widthFinal ? x.height == cell.height : x.width == cell.width).findAny().orElse(null);
		}
		if (secondNeighbor != null) {
			double newRest = increaseCell(secondNeighbor, width, rest, area);
			if (newRest > 0) {
				if (width) {
					double total = selectedNeighbor.width + secondNeighbor.width;
					selectedNeighbor.width += newRest * (selectedNeighbor.width / total);
				} else {
					double total = selectedNeighbor.height + secondNeighbor.height;
					selectedNeighbor.height += newRest * (selectedNeighbor.height / total);
				}
			}
		} else {
			if (width) {
				selectedNeighbor.width += rest;
			} else {
				selectedNeighbor.height += rest;
			}
		}
	}

	/**
	 * Increases cell width/height by smallest possible part of available
	 * @param cell
	 * @param width if true, width (otherwise height) will be increased
	 * @param available the amount of available space
	 * @param area if true, than area computation will be used, otherwise length computation will be used
	 * @return the rest of available space (the unused part)
	 */
	private static double increaseCell(Cell cell, boolean width, double available, boolean area) {
		double canAffort;
		if (area) {
			double needed;
			if (width) {
				needed = cell.areaNeeded / cell.height;
				canAffort = Math.min(available, needed);
				cell.width += canAffort;
				cell.areaNeeded -= canAffort * cell.height;
			} else {
				needed = cell.areaNeeded / cell.width;
				canAffort = Math.min(available, needed);
				cell.height += canAffort;
				cell.areaNeeded -= canAffort * cell.width;
			}
		} else {
			if (width) {
				canAffort = Math.min(available, cell.widthNeeded);
				cell.width += canAffort;
				cell.widthNeeded -= canAffort;
			} else {
				canAffort = Math.min(available, cell.heightNeeded);
				cell.height += canAffort;
				cell.heightNeeded -= canAffort;
			}
		}
		return available - canAffort;
	}

	/**
	 * @param cell
	 * @return true if a cell has at least 2 empty non-resolved neighbors
	 */
	private boolean hasMoreThanOneEmptyNeighbor(Cell cell) {
		int row = getCellRow(cell);
		int column = getCellColumn(cell);
		return row < rows && column < columns && getCellNeighbors(row, column, true).size() > 1;
	}

	/**
	 * @param cell
	 * @return (lowest) row index of a cell
	 */
	private int getCellRow(Cell cell) {
		int row = 0;
		while (row < rows) {
			if (getRow(row).contains(cell)) {
				break;
			}
			row++;
		}
		return row;
	}

	/**
	 * @param cell
	 * @return (lowest) column index of a cell
	 */
	private int getCellColumn(Cell cell) {
		int column = 0;
		while (column < columns) {
			if (getColumn(column).contains(cell)) {
				break;
			}
			column++;
		}
		return column;
	}

	/**
	 * @param row
	 * @param column
	 * @param empty if true, then only empty neighbors are returned, if false only non-empty neightbors are returned
	 * @return neighbor cells of the given cell
	 */
	private List<Cell> getCellNeighbors(int row, int column, boolean empty) {
		List<Cell> neighbors = new ArrayList<>();
		if (row - 1 >= 0) {
			neighbors.add(getCell(row - 1, column));
		}
		if (column - 1 >= 0) {
			neighbors.add(getCell(row, column - 1));
		}
		if (column + 1 < columns) {
			neighbors.add(getCell(row, column + 1));
		}
		if (row + 1 < rows) {
			neighbors.add(getCell(row + 1, column));
		}
		neighbors.removeIf(x -> x.isEmpty() != empty && !x.resolved);
		return neighbors;
	}

	/**
	 * Performs assigning space to every cell of the space
	 * @param spaceBox
	 */
	private void assignSpace(BoundingBox spaceBox) {
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				Cell cell = getCell(row, column);
				if (cell.wholeRow && column != 0) {
					break; // this row is processed in it's first cell
				}
				if (cell.wholeColumn && row != 0) {
					continue; // this column is proccessed in it's first cell
				}
				assignSpaceInsideCell(row, column, spaceBox);
			}
		}
	}

	/**
	 * Checks if all cells fit into width / height / area and prepares sizes of subgraphs (with exception of the middle no-side cell). <br>
	 * Space in the cell is divided equally according to min sizes and min areas.
	 * @param row of a cell
	 * @param column of a cell
	 * @param spaceBox space of the subgraph
	 */
	private void assignSpaceInsideCell(int row, int column, BoundingBox spaceBox) {
		Cell cell = getCell(row, column);
		Point2D position = getCellPosition(row, column, spaceBox);
		int subgraphBorder = Preferences.getInstance().getSubgraphBorder();
		if (cell.isEmpty()) {
			return; // nothing to do here with empty cell
		}
		if (cell.nodes.size() == 1) { // Corners etc.
			Node node = cell.nodes.get(0);
			Point2D nodePosition = new Point2D.Double(position.getX() + cell.width / 2, position.getY() + cell.height / 2);
			node.setPosition(nodePosition);
			if (node instanceof SubGraph) {
				node.setSize(cell.width - subgraphBorder * 2, cell.height - subgraphBorder * 2);
			}
		} else {
			int border = Preferences.getInstance().getNodeBorder();
			double sumOfMinSubGraphsArea = cell.getSubGraphArea();
			double sumOfNodesArea = cell.getNodesOnlyArea();
			double cellArea = cell.width * cell.height;
			if (cellArea < (sumOfMinSubGraphsArea + sumOfNodesArea)) {
				cell.areaNeeded = (sumOfMinSubGraphsArea + sumOfNodesArea) - cellArea;
			}
			double sumOfMinHeightSubGraphs = cell.nodes.stream().filter(node -> node instanceof SubGraph)
					.mapToDouble(node -> ((SubGraph) node).getMinDimensions().getY()).sum();
			double sumOfHeightNodes = cell.nodes.stream().filter(node -> !(node instanceof SubGraph))
					.mapToDouble(node -> node.getBoundingBox(border, false).getHeight()).sum();

			double sumOfMinWidthSubGraphs = cell.nodes.stream().filter(node -> node instanceof SubGraph)
					.mapToDouble(node -> ((SubGraph) node).getMinDimensions().getX()).sum();
			double sumOfWidthNodes = cell.nodes.stream().filter(node -> !(node instanceof SubGraph))
					.mapToDouble(node -> node.getBoundingBox(border, false).getWidth()).sum();
			// Nodes that fill whole width
			if (cell.wholeRow || cell.nodes.stream().anyMatch(node -> node.getRequiredSides().contains(Location.LEFT)
					|| node.getRequiredSides().contains(Location.RIGHT))) {
				double ratio = (sumOfMinHeightSubGraphs != 0) ? (cell.height - sumOfHeightNodes) / sumOfMinHeightSubGraphs : 1;
				double areaRatio = (sumOfMinSubGraphsArea != 0) ? (cell.height - sumOfHeightNodes) / sumOfMinSubGraphsArea : 1;
				if (cell.height < (sumOfMinHeightSubGraphs + sumOfHeightNodes)) {
					cell.heightNeeded = (sumOfMinHeightSubGraphs + sumOfHeightNodes) - cell.height;
				}
				for (SubGraph subgraph : cell.nodes.stream().filter(node -> node instanceof SubGraph).map(x -> (SubGraph) x).collect(Collectors.toList())) {
					double height = (subgraph.getMinDimensions().getY() * ratio + subgraph.getMinArea() * areaRatio) / 2 - subgraphBorder * 2;
					subgraph.setSize(cell.width - subgraphBorder * 2, height);
				}
			} else if (cell.wholeColumn) { // Nodes that fill whole height
				double ratio = (sumOfMinWidthSubGraphs != 0) ? (cell.width - sumOfWidthNodes) / sumOfMinWidthSubGraphs : 1;
				double areaRatio = (sumOfMinSubGraphsArea != 0) ? (cell.width - sumOfWidthNodes) / sumOfMinSubGraphsArea : 1;
				if (cell.width < (sumOfMinWidthSubGraphs + sumOfWidthNodes)) {
					cell.widthNeeded = (sumOfMinWidthSubGraphs + sumOfWidthNodes) - cell.width;
				}
				for (SubGraph subgraph : cell.nodes.stream().filter(node -> node instanceof SubGraph).map(x -> (SubGraph) x).collect(Collectors.toList())) {
					double width = (subgraph.getMinDimensions().getX() * ratio + subgraph.getMinArea() * areaRatio) / 2 - subgraphBorder * 2;
					subgraph.setSize(width, cell.height - subgraphBorder * 2);
				}
			} else {
				// The middle
				// Not much to do here, just assign min size to subgraphs
				for (SubGraph subgraph : cell.nodes.stream().filter(node -> node instanceof SubGraph).map(x -> (SubGraph) x).collect(Collectors.toList())) {
					subgraph.setSizeToMinSize();
				}
			}
		}
	}

	/**
	 * Reduces the space by removing empty rows & columns and merging partially full rows & columns
	 * @param spaceBox
	 */
	private void reduceSpace(BoundingBox spaceBox) {
		// remove empty rows and columns
		for (List<Cell> row : new ArrayList<>(space)) {
			if (row.stream().allMatch(Cell::isEmpty)) {
				removeRow(space.indexOf(row));
			}
		}
		int column = 0;
		while (column < columns) {
			if (getColumn(column).stream().allMatch(Cell::isEmpty)) {
				removeColumn(column);
			} else {
				column++; // Only increase index for iteration, when we HAVEN'T removed a column!
			}
		}
		// merge partially full rows & columns
		int first = 0;
		int second = 1;
		while (second <= maxRow() || second <= maxColumn()) {
			boolean canMergeRows = canMerge(getRow(first), getRow(second));
			boolean canMergeColumns = canMerge(getColumn(first), getColumn(second));
			if (canMergeRows && canMergeColumns) { // If both rows and columns can be merge, we need to decide which to merge
				double widthPart = spaceBox.getWidth() / Math.max(getTotalColumnRequiredWidth(), 1);
				double heightPart = spaceBox.getHeight() / Math.max(getTotalRowRequiredHeight(), 1);
				canMergeRows = heightPart < widthPart;
				canMergeColumns = !canMergeRows;
			}
			// Now is decided, so merge
			if (canMergeRows) {
				mergeRows(first, second);
			} else if (canMergeColumns) {
				mergeColumns(first, second);
			} else {
				first++;
				second++;
			}
		}
	}

	/**
	 * @param tripleSide
	 * @return The completely block location from set of 3 locations. Ex: For TOP, LEFT and RIGHT it is TOP.
	 */
	public static Location getBlockLocationFromTriple(Set<Location> tripleSide) {
		return (Location) tripleSide.stream().filter(x -> !tripleSide.contains(x.complementary())).toArray()[0];
	}

	/**
	 * @param where merging destination
	 * @param what merge to the destination
	 * @return true, if second list of Cells can be merged into the first one without overwriting any non-empty cell
	 */
	private static boolean canMerge(List<Cell> where, List<Cell> what) {
		if (where.isEmpty() || what.isEmpty()) {
			return false;
		}
		for (int i = 0; i < what.size(); i++) {
			if (!where.get(i).isEmpty() && !what.get(i).isEmpty()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Merges second (what) row into the first one (where) and deletes the second row afterwards. <br>
	 * Method canMerge should be used to check if merging wouldn't overwrite any non-empty cells.
	 * @param where merging destination
	 * @param what this will be merged into destination and deleted afterwards.
	 */
	private void mergeRows(int where, int what) {
		List<Cell> rowWhere = getRow(where);
		List<Cell> rowWhat = getRow(what);
		for (int i = 0; i < rowWhat.size(); i++) {
			if (rowWhere.get(i).isEmpty()) {
				rowWhere.set(i, rowWhat.get(i));
			}
		}
		removeRow(what);
	}

	/**
	 * Merges second (what) column into the first one (where) and deletes the second column afterwards. <br>
	 * Method canMerge should be used to check if merging wouldn't overwrite any non-empty cells.
	 * @param where merging destination
	 * @param what this will be merged into destination and deleted afterwards.
	 */
	private void mergeColumns(int where, int what) {
		List<Cell> columnWhat = getColumn(what);
		for (int i = 0; i < columnWhat.size(); i++) {
			if (getCell(i, where).isEmpty()) {
				space.get(i).set(where, columnWhat.get(i));
			}
		}
		removeColumn(what);
	}
	
	/**
	 * @param row index of row of the cell
	 * @param column index of column of the cell
	 * @param spaceBox
	 * @return exact cell position (upper-left corner) in the spaceBox
	 */
	private Point2D getCellPosition(int row, int column, BoundingBox spaceBox) {
		Cell cell = getCell(row, column);
		double y = spaceBox.getMinY();
		double x = spaceBox.getMinX();
		if (!cell.wholeColumn) {
			for (int i = 0; i < row; i++) {
				y += getCell(i, column).height;
			}
		}
		if (!cell.wholeRow) {
			for (int i = 0; i < column; i++) {
				x += getCell(row, i).width;
			}
		}
		return new Point2D.Double(x, y);
	}

	/**
	 * @param index of a row
	 * @return min height of a row
	 */
	private double getRowMinHeight(int index) {
		return new HashSet<>(getRow(index)).stream().mapToDouble(cell -> cell.nodes.stream()
				.mapToDouble(node -> (node instanceof SubGraph ? ((SubGraph) node).getMinDimensions().getY()
						: node.getBoundingBox(Preferences.getInstance().getNodeBorder(), false).getHeight()))
				.max().orElse(0)) // from each cell take maximum
				.max().orElse(0);
	}
	
	/**
	 * @param index of a column
	 * @return min width of a column
	 */
	private double getColumnMinWidth(int index) {
		return new HashSet<>(getColumn(index)).stream().mapToDouble(cell -> cell.nodes.stream()
				.mapToDouble(node -> (node instanceof SubGraph ? ((SubGraph) node).getMinDimensions().getX()
						: node.getBoundingBox(Preferences.getInstance().getNodeBorder(), false).getWidth()))
				.max().orElse(0)) // from each cell take maximum
				.max().orElse(0);
	}

	/**
	 * @param index of a row
	 * @return sum of minimum area of cells in a row
	 */
	private double getRowMinArea(int index) {
		return new HashSet<>(getRow(index)).stream().mapToDouble(Cell::getAllNodesArea).sum();
	}

	/**
	 * @param index of a column
	 * @return sum of minimum area of cells in a column
	 */
	private double getColumnMinArea(int index) {
		return new HashSet<>(getColumn(index)).stream().mapToDouble(Cell::getAllNodesArea).sum();
	}

	/**
	 * @return sum of all column widths
	 */
	private double getTotalColumnRequiredWidth() {
		double totalColumnRequiredWidth = 0;
		for (int i = 0; i < columns; i++) {
			totalColumnRequiredWidth += getColumnMinWidth(i);
		}
		return totalColumnRequiredWidth;
	}

	/**
	 * @return sum of all column heights
	 */
	private double getTotalRowRequiredHeight() {
		double totalRowRequiredHeight = 0;
		for (int i = 0; i < rows; i++) {
			totalRowRequiredHeight += getRowMinHeight(i);
		}
		return totalRowRequiredHeight;
	}

	/**
	 * @return sum of all cell minimum required areas
	 */
	private double getTotalRequiredArea() {
		double area = 0;
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				area += getCell(row, column).getAllNodesArea();
			}
		}
		return area;
	}

	/**
	 * @param index of a row
	 * @return row from the space or empty list if index is out of bounds, can be directly modified/written to
	 */
	private List<Cell> getRow(int index) {
		if (index > maxRow()) {
			return new ArrayList<>(0);
		}
		return space.get(index);
	}

	/**
	 * @param index of a column
	 * @return column from the space or empty list if index is out of bounds <br>
	 *         FOR READING ONLY, writing to this list will NOT be reflected in the space
	 */
	private List<Cell> getColumn(int index) {
		if (index > maxColumn()) {
			return new ArrayList<>(0);
		}
		return space.stream().map(x -> x.get(index)).collect(Collectors.toList());
	}

	/**
	 * @return index of last row
	 */
	private int maxRow() {
		return rows - 1;
	}

	/**
	 * @return index of last column
	 */
	private int maxColumn() {
		return columns - 1;
	}

	/**
	 * Adds new row to the space on the given index (all further rows will be shifted).
	 * @param index of the new row
	 */
	private void addRow(int index) {
		List<Cell> newRow = new ArrayList<>();
		for (int i = 0; i < columns; i++) {
			Cell firstCellInColumn = null;
			if (rows != 0 && columns != 0) {
				firstCellInColumn = space.get(0).get(i);
			}
			newRow.add(firstCellInColumn != null && firstCellInColumn.wholeColumn ? firstCellInColumn : new Cell());
		}
		space.add(index, newRow);
		rows++;
	}

	/**
	 * Adds new column to the space on the given index (all further columns will be shifted).
	 * @param index of the new column
	 */
	private void addColumn(int index) {
		for (int i = 0; i < rows; i++) {
			List<Cell> row = space.get(i);
			Cell firstCellInRow = row.get(0);
			row.add(index, firstCellInRow.wholeRow ? firstCellInRow : new Cell());
		}
		columns++;
	}

	/**
	 * Removes a row
	 * @param index of a row
	 */
	private void removeRow(int index) {
		space.remove(index);
		rows--;
	}

	/**
	 * Removes a column
	 * @param index of a row
	 */
	private void removeColumn(int index) {
		for (int i = 0; i < rows; i++) {
			space.get(i).remove(index); // Removing cells from rows, not rows from space. Iterating over space.
		}
		columns--;
	}

	/**
	 * @param row
	 * @param column
	 * @return returns Cell at given position
	 */
	private Cell getCell(int row, int column) {
		return space.get(row).get(column);
	}

	/**
	 * Spreads the cell to the whole row
	 * @param cell
	 * @param index of the row
	 */
	private void spreadCellToRow(Cell cell, int index) {
		List<Cell> row = space.get(index);
		if (!row.get(0).wholeRow) {
			cell.wholeRow = true;
			for (int i = 0; i < columns; i++) {
				row.set(0, cell);
			}
		}
	}

	/**
	 * Spreads the cell to the whole column
	 * @param cell
	 * @param index of the column
	 */
	private void spreadCellToColumn(Cell cell, int index) {
		if (!getCell(0, index).wholeColumn) {
			cell.wholeColumn = true;
			for (int i = 0; i < rows; i++) {
				space.get(i).set(index, cell);
			}
		}
	}

	/**
	 * @return whole-column-cell of the SPREAD_INDEX column
	 */
	private Cell getSpreadColumnCell() {
		if (SPREAD_INDEX == maxColumn()) { // If doesn't exist, then create one
			addColumn(SPREAD_INDEX);
			Cell wantedCell = getCell(MIDDLE_INDEX, SPREAD_INDEX);
			spreadCellToColumn(wantedCell, SPREAD_INDEX);
		}
		return getCell(0, SPREAD_INDEX);
	}

	/**
	 * @return whole-row-cell of the SPREAD_INDEX row
	 */
	private Cell getSpreadRowCell() {
		if (SPREAD_INDEX == maxRow()) { // If doesn't exist, then create one
			addRow(SPREAD_INDEX);
			Cell wantedCell = getCell(SPREAD_INDEX, MIDDLE_INDEX);
			spreadCellToRow(wantedCell, SPREAD_INDEX);
		}
		return getCell(SPREAD_INDEX, 0);
	}

	/**
	 * One cell of a space. <br>
	 * All attributes can be directly accessed for easy manipulation. This is private class.
	 * @author Jan Juda (xjudaj00)
	 */
	private static class Cell {
		/** Nodes in this cell */
		List<Node> nodes = new ArrayList<>();
		/** Does this cell fill entire row? */
		boolean wholeRow = false;
		/** Does this cell fill entire column? */
		boolean wholeColumn = false;
		/** Width of this cell, should be assigned in the process */
		double width = 0;
		/** Height of this cell, should be assigned in the process */
		double height = 0;
		/** Min width needed to satisfy inner nodes requirements */
		double widthNeeded = 0;
		/** Min height needed to satisfy inner nodes requirements */
		double heightNeeded = 0;
		/** Min area needed to satisfy inner nodes requirements */
		double areaNeeded = 0;
		/** Marks empty already resolved cells */
		boolean resolved = false;

		/**
		 * Constructs empty cell.
		 */
		public Cell() {
			// Nothing to do here.
		}

		/**
		 * @return true if there are no nodes in this cell
		 */
		public boolean isEmpty() {
			return nodes.isEmpty();
		}

		/**
		 * @return area of all subgraphs inside this cell
		 */
		public double getSubGraphArea() {
			return nodes.stream().filter(node -> node instanceof SubGraph).mapToDouble(node -> ((SubGraph) node).getMinArea()).sum();
		}

		/**
		 * @return area of all simple Nodes (not subgraphs) inside this cell
		 */
		public double getNodesOnlyArea() {
			int border = Preferences.getInstance().getNodeBorder();
			return nodes.stream().filter(node -> !(node instanceof SubGraph)).mapToDouble(node -> node.getArea(border)).sum();
		}

		/**
		 * @return area of all nodes (both Nodes and Subgraphs) inside this cell
		 */
		public double getAllNodesArea() {
			return getSubGraphArea() + getNodesOnlyArea();
		}

		@Override
		public String toString() {
			return "[" + nodes.size() + "]";
		}
	}
}
