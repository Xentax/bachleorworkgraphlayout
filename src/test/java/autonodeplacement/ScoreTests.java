/**
 * Copyright 2020 Jan Juda
 * Created: 2. 5. 2020
 */
package autonodeplacement;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import autonodeplacement.representation.Edge;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import autonodeplacement.representation.SubGraph;

/**
 * Tests for Score class
 * @author Jan Juda (xjudaj00)
 */
public class ScoreTests {

	/** Default size for nodes in test graph */
	private static final double NODE_SIZE = 40;
	/** Default Y position for nodes in test graph */
	private static final double NODE_POS_Y = 100;

	/**
	 * Test graph with 3 nodes, 2 edges (Left to Right and Top to bottom),
	 * 1 Edge x Edge crossing and 1 Edge x Node crossing.<br>
	 * 
	 *      |4|
	 * 		 |
	 * |1|---|---|3|----->|2|
	 *       V
	 * 		|5|
	 */
	private static GraphRepresentation testGraph;
	/** All score rules enabled with weight 1 */
	private static Map<Score, Integer> allRulesWeightOne = new LinkedHashMap<>();
	/** All score rules enabled with weight 0 */
	private static Map<Score, Integer> allRulesWeightZero = new LinkedHashMap<>();
	/** All score rules enabled, scores has weights in multiples of ten */
	private static Map<Score, Integer> allRulesMultiplesOfTen = new LinkedHashMap<>();

	/**
	 * Sets up variables used in multiple tests
	 */
	@BeforeClass
	public static void setUpClass() {
		int i = 1;
		for (Score score : Score.values()) {
			allRulesWeightOne.put(score, Integer.valueOf(1));
			allRulesWeightZero.put(score, Integer.valueOf(0));
			allRulesMultiplesOfTen.put(score, Integer.valueOf(i));
			i *= 10;
		}
		SubGraph rootGraph = new SubGraph("");
		Map<String, SubGraph> subgraphs = new LinkedHashMap<>();
		subgraphs.put(rootGraph.getId(), rootGraph);
		/**
		 *      |4|
		 * 		 |
		 * |1|---|---|3|----->|2|
		 *       V
		 * 		|5|
		 */
		Node firstNode = new Node("firstNode", NODE_SIZE, NODE_SIZE, 50, NODE_POS_Y);
		Node secondNode = new Node("secondNode", NODE_SIZE, NODE_SIZE, 400, NODE_POS_Y);
		Node thirdNode = new Node("thirdNode", NODE_SIZE, NODE_SIZE, 200, NODE_POS_Y);
		Node fourthNode = new Node("fourthNode", NODE_SIZE, NODE_SIZE, 100, 50);
		Node fifthNode = new Node("fifthNode", NODE_SIZE, NODE_SIZE, 100, 150);
		firstNode.setParent(rootGraph);
		secondNode.setParent(rootGraph);
		thirdNode.setParent(rootGraph);
		fourthNode.setParent(rootGraph);
		fifthNode.setParent(rootGraph);
		Map<String, Node> nodes = new LinkedHashMap<>();
		nodes.put(firstNode.toString(), firstNode);
		nodes.put(secondNode.toString(), secondNode);
		nodes.put(thirdNode.toString(), thirdNode);
		nodes.put(fourthNode.toString(), fourthNode);
		nodes.put(fifthNode.toString(), fifthNode);
		Edge firstToSecond = new Edge("firstNode->secondNode", firstNode, secondNode);
		Edge fourthTofifth = new Edge("fourthNode->fifthNode", fourthNode, fifthNode);
		Map<String, Edge> edges = new LinkedHashMap<>();
		edges.put(firstToSecond.getId(), firstToSecond);
		edges.put(fourthTofifth.getId(), fourthTofifth);
		testGraph = new GraphRepresentation(nodes, edges, subgraphs, 600, 600);
	}

	/**
	 * Tests that evaluating an empty graph returns zero for all of the score rules.
	 */
	@Test
	public void evaluateEmpty_Zero() {
		assertEquals(0, Score.evaluate(new GraphRepresentation(), allRulesWeightOne));
	}

	/**
	 * Tests that evaluating a graph with empty set of rules returns zero
	 */
	@Test
	public void evaluateWithNoRules_Zero() {
		assertEquals(0, Score.evaluate(testGraph, new LinkedHashMap<>()));
	}

	/**
	 * Tests that evaluating a graph with empty set of rules returns zero
	 */
	@Test
	public void evaluateWithZeroWeight_Zero() {
		assertEquals(0, Score.evaluate(testGraph, allRulesWeightZero));
	}

	/**
	 * Tests that evaluating the test graph will result in correct score with all score rules given same weights
	 */
	@Test
	public void evaluateNonEmptyWithAllRulesSameWeight_NonZero() {
		assertEquals(4, Score.evaluate(testGraph, allRulesWeightOne));
	}

	/**
	 * Tests that evaluating the test graph will result in correct score with all score rules given different weights
	 */
	@Test
	public void evaluateNonEmptyWithAllRulesDifferentWeight_NonZero() {
		assertEquals(101011, Score.evaluate(testGraph, allRulesMultiplesOfTen));
	}

	/**
	 * Tests CEN rule
	 */
	@Test
	public void evaluateRuleCEN_One() {
		assertEquals(1, Score.evaluate(testGraph, Collections.singletonMap(Score.CEN, Integer.valueOf(1))));
	}

	/**
	 * Tests CEE rule
	 */
	@Test
	public void evaluateRuleCEE_One() {
		assertEquals(1, Score.evaluate(testGraph, Collections.singletonMap(Score.CEE, Integer.valueOf(1))));
	}

	/**
	 * Tests LtR rule
	 */
	@Test
	public void evaluateRuleLtR_Zero() {
		assertEquals(0, Score.evaluate(testGraph, Collections.singletonMap(Score.LtR, Integer.valueOf(1))));
	}

	/**
	 * Tests RtL rule
	 */
	@Test
	public void evaluateRuleRtL_One() {
		assertEquals(1, Score.evaluate(testGraph, Collections.singletonMap(Score.RtL, Integer.valueOf(1))));
	}

	/**
	 * Tests TtB rule
	 */
	@Test
	public void evaluateRuleTtB_Zero() {
		assertEquals(0, Score.evaluate(testGraph, Collections.singletonMap(Score.TtB, Integer.valueOf(1))));
	}

	/**
	 * Tests BtT rule
	 */
	@Test
	public void evaluateRuleBtT_One() {
		assertEquals(1, Score.evaluate(testGraph, Collections.singletonMap(Score.BtT, Integer.valueOf(1))));
	}
}
