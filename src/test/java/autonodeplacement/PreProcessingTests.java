/**
 * Copyright 2020 Jan Juda
 * Created: 2. 5. 2020
 */
package autonodeplacement;

import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import autonodeplacement.representation.Edge;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import autonodeplacement.representation.SubGraph;

/**
 * Tests for PreProcessing
 * @author Jan Juda (xjudaj00)
 */
public class PreProcessingTests {


	/** Default size for nodes in test graph */
	private static final double NODE_SIZE = 40;

	/** Tiny graph with only one node */
	private static GraphRepresentation testGraph;

	/**
	 * Sets up variables used in multiple tests
	 */
	@BeforeClass
	public static void setUpBeforeClass() {
		SubGraph rootGraph = new SubGraph("");
		Map<String, SubGraph> subgraphs = new LinkedHashMap<>();
		subgraphs.put(rootGraph.getId(), rootGraph);
		/**
		 * 		    ________________
		 *         |                |
		 *         |                |
		 * |1,2,3| |                |
		 *         |                |
		 *         |________________|
		 */
		Node firstNode = new Node("firstNode", NODE_SIZE, NODE_SIZE, NODE_SIZE, NODE_SIZE);
		firstNode.setParent(rootGraph);
		Map<String, Node> nodes = new LinkedHashMap<>();
		nodes.put(firstNode.getId(), firstNode);
		Map<String, Edge> edges = new LinkedHashMap<>();
		testGraph = new GraphRepresentation(nodes, edges, subgraphs, 1, 1);
	}

	/**
	 * Tests that tiny graph size will increase by calculation of minimum size
	 */
	@Test
	public void graphSizeWillIncrease() {
		Point2D tinyInitialSize = testGraph.getRootGraph().getSize();
		PreProcessing.checkGraphSize(testGraph);
		Point2D newSize = testGraph.getRootGraph().getSize();
		assertTrue(newSize.getX() > tinyInitialSize.getX());
		assertTrue(newSize.getY() > tinyInitialSize.getY());
	}

}
