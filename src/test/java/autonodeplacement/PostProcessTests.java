/**
 * Copyright 2020 Jan Juda
 * Created: 2. 5. 2020
 */
package autonodeplacement;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import autonodeplacement.representation.Edge;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import autonodeplacement.representation.SubGraph;
import javafx.geometry.BoundingBox;

/**
 * Tests for PostProcessing class
 * @author Jan Juda (xjudaj00)
 */
public class PostProcessTests {

	/** Default size for nodes in test graph */
	private static final double NODE_SIZE = 40;
	
	/**
	 * Test graph with 3 nodes, 0 edges, and 1 subgraph.
	 * Node 3 belong to the subgraph.
	 * 		    ________________
	 *         |                |
	 *         |                |
	 * |1,2,3| |                |
	 *         |                |
	 *         |________________|
	 */
	private static GraphRepresentation testGraph;
	/** Third node of the graph */
	private static Node thirdNode;
	/** One subgraph of the graph */
	private static SubGraph subgraph;
	
	/**
	 * Sets up variables used in multiple tests
	 */
	@BeforeClass
	public static void setUpClass() {
		// Set default value of max iterations of postprocessing
		Preferences.getInstance().setMaxIterationsPostprocess(100);
		SubGraph rootGraph = new SubGraph("");
		subgraph = new SubGraph("Subgraph", 300, 600, 450, 300);
		Map<String, SubGraph> subgraphs = new LinkedHashMap<>();
		subgraphs.put(rootGraph.getId(), rootGraph);
		subgraphs.put(subgraph.getId(), subgraph);
		/**
		 * 		    ________________
		 *         |                |
		 *         |                |
		 * |1,2,3| |                |
		 *         |                |
		 *         |________________|
		 */
		Node firstNode = new Node("firstNode", NODE_SIZE, NODE_SIZE, 50, 300);
		Node secondNode = new Node("secondNode", NODE_SIZE, NODE_SIZE, 50, 300);
		thirdNode = new Node("thirdNode", NODE_SIZE, NODE_SIZE, 50, 300);
		firstNode.setParent(rootGraph);
		secondNode.setParent(rootGraph);
		thirdNode.setParent(subgraph);
		Map<String, Node> nodes = new LinkedHashMap<>();
		nodes.put(firstNode.getId(), firstNode);
		nodes.put(secondNode.getId(), secondNode);
		nodes.put(thirdNode.getId(), thirdNode);
		Map<String, Edge> edges = new LinkedHashMap<>();
		testGraph = new GraphRepresentation(nodes, edges, subgraphs, 600, 600);
	}
	
	/**
	 * Tests that overflow from subgraph can be solved without reaching default iterations limit.
	 */
	@Test
	public void overflowFromSubgraphResolved() {
		BoundingBox subgraphBox = subgraph.getBoundingBox(0, false);
		assertFalse(PostProcessing.checkOverlapAndOverflow(subgraphBox, Arrays.asList(thirdNode)));
		assertTrue(subgraphBox.contains(thirdNode.getBoundingBox(0, false)));
	}

	/**
	 * Tests that overlap can be solved without reaching default iterations limit.
	 * The graph has 3 overlapping nodes. The third one can be moved by previous test, but there will still be an overlap.
	 * The order of execution of these tests shouldn't matter.
	 */
	@Test
	public void overlapResolved() {
		BoundingBox subgraphBox = testGraph.getRootGraph().getBoundingBox(0, false);
		assertFalse(PostProcessing.checkOverlapAndOverflow(subgraphBox, new ArrayList<>(testGraph.getNodes())));
		for (Node first : testGraph.getNodes()) {
			for (Node second : testGraph.getNodes()) {
				if (first != second) {
					assertFalse(first.getBoundingBox(0, false).intersects(second.getBoundingBox(0, false)));
				}
			}
		}
	}
}
