/**
 * Copyright 2020 Jan Juda
 * Created: 2. 5. 2020
 */
package autonodeplacement;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * All Tests suite
 * @author Jan Juda (xjudaj00)
 */
@RunWith(Suite.class)
@SuiteClasses({ PostProcessTests.class, PreProcessingTests.class, RepresentationTests.class, ScoreTests.class })
public class AllTests {
	// Nothing to do here
}
