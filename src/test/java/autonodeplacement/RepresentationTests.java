/**
 * Copyright 2020 Jan Juda
 * Created: 2. 5. 2020
 */
package autonodeplacement;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.BeforeClass;
import org.junit.Test;

import autonodeplacement.representation.Edge;
import autonodeplacement.representation.GraphRepresentation;
import autonodeplacement.representation.Node;
import autonodeplacement.representation.Node.Location;
import autonodeplacement.representation.SubGraph;

/**
 * Tests for internal representation
 * @author Jan Juda (xjudaj00)
 */
public class RepresentationTests {


	/** Default size for nodes in test graph */
	private static final double NODE_SIZE = 40;
	
	/**
	 * Test graph with 3 nodes, 0 edges, and 1 subgraph.
	 * Node 3 belong to the subgraph.
	 * 		    ________________
	 *         |                |
	 * |1|     |                |
	 *         |   |3|          |
	 *     |2| |                |
	 *         |________________|
	 */
	private static GraphRepresentation testGraph;
	/** First node of the graph */
	private static Node firstNode;
	/** Second node of the graph */
	private static Node secondNode;
	/** Third node of the graph */
	private static Node thirdNode;
	/** Rootsubgraph of the graph */
	private static SubGraph rootGraph;
	/** One subgraph of the graph */
	private static SubGraph subgraph;
	
	/**
	 * Sets up variables used in multiple tests
	 */
	@BeforeClass
	public static void setUpClass() {
		rootGraph = new SubGraph("");
		subgraph = new SubGraph("Subgraph", 300, 600, 450, 300);
		subgraph.setParent(rootGraph);
		Map<String, SubGraph> subgraphs = new LinkedHashMap<>();
		subgraphs.put(rootGraph.getId(), rootGraph);
		subgraphs.put(subgraph.getId(), subgraph);
		/**
		 * 		    ________________
		 *         |                |
		 * |1|     |                |
		 *         |   |3|          |
		 *     |2| |                |
		 *         |________________|
		 */
		firstNode = new Node("firstNode", NODE_SIZE, NODE_SIZE, 50, 100);
		secondNode = new Node("secondNode", NODE_SIZE, NODE_SIZE, 200, 400);
		thirdNode = new Node("thirdNode", NODE_SIZE, NODE_SIZE, 400, 300);
		firstNode.setParent(rootGraph);
		secondNode.setParent(rootGraph);
		thirdNode.setParent(subgraph);
		Map<String, Node> nodes = new LinkedHashMap<>();
		nodes.put(firstNode.toString(), firstNode);
		nodes.put(secondNode.toString(), secondNode);
		nodes.put(thirdNode.toString(), thirdNode);
		Map<String, Edge> edges = new LinkedHashMap<>();
		testGraph = new GraphRepresentation(nodes, edges, subgraphs, 600, 600);
	}
	
	/**
	 * Tests if shifting graph works and that the changes are propagated recursively
	 */
	@Test
	public void shiftingGraphWorks() {
		List<Point2D> initialPositions = Stream.concat(testGraph.getNodes().stream(), testGraph.getSubgraphs().values().stream())
				.map(Node::getPosition).collect(Collectors.toList());
		Point2D shift = new Point2D.Double(20, -5);
		testGraph.getRootGraph().shift(shift);
		List<Point2D> shiftedPositions = Stream.concat(testGraph.getNodes().stream(), testGraph.getSubgraphs().values().stream())
				.map(Node::getPosition).collect(Collectors.toList());
		for (int i = 0; i < initialPositions.size(); i++) {
			Point2D initialPos = initialPositions.get(i);
			Point2D shiftedPos = shiftedPositions.get(i);
			assertEquals(initialPos.getX() + shift.getX(), shiftedPos.getX(), 0.0000001);
			assertEquals(initialPos.getY() + shift.getY(), shiftedPos.getY(), 0.0000001);
		}
	}

	/**
	 * Tests that graph contains correct nodes
	 */
	@Test
	public void graphContainsCorrectNodes() {
		Collection<Node> nodes = testGraph.getNodes();
		assertTrue(nodes.contains(firstNode));
		assertTrue(nodes.contains(secondNode));
		assertTrue(nodes.contains(thirdNode));
		assertEquals(3, nodes.size());
	}

	/**
	 * Tests that there are no edges in the graph
	 */
	@Test
	public void graphNoEdges() {
		assertTrue(testGraph.getEdges().isEmpty());
	}

	/**
	 * Tests that the graph contains given rootgraph and the subgraph
	 */
	@Test
	public void graphContainsRootAndSecondSubgraph() {
		Collection<SubGraph> subgraphs = testGraph.getSubgraphs().values();
		assertTrue(subgraphs.contains(rootGraph));
		assertTrue(subgraphs.contains(subgraph));
		assertEquals(2, subgraphs.size());
		assertEquals(rootGraph, testGraph.getRootGraph());

	}

	/**
	 * Tests getting existing nodes by path
	 */
	@Test
	public void getNodeByPathExisting() {
		assertEquals(firstNode, testGraph.getNode(firstNode.toString()));
		assertEquals(secondNode, testGraph.getNode(secondNode.toString()));
		assertEquals(thirdNode, testGraph.getNode(thirdNode.toString()));
	}

	/**
	 * Tests getting non-existing nodes by path
	 */
	@Test
	public void getNodeByPathNotExisting() {
		assertEquals(null, testGraph.getNode("not_existing_path"));
	}

	/**
	 * Tests getting existing nodes by ID
	 */
	@Test
	public void getNodeByIDExisting() {
		assertEquals(firstNode, testGraph.getNodeByID(firstNode.getId()));
		assertEquals(secondNode, testGraph.getNodeByID(secondNode.getId()));
		assertEquals(thirdNode, testGraph.getNodeByID(thirdNode.getId()));
		assertEquals(thirdNode, testGraph.getNodeByID(thirdNode.toString()));
	}

	/**
	 * Tests getting non-existing nodes by ID
	 */
	@Test
	public void getNodeByIDNotExisting() {
		assertEquals(null, testGraph.getNodeByID("not_existing_ID"));
	}

	/**
	 * Tests that resizing of the graph correctly changes sizes of all nodes and subgraph
	 */
	@Test
	public void resizingTest() {
		List<Point2D> initialSizes = Stream.concat(testGraph.getNodes().stream(), testGraph.getSubgraphs().values().stream()).map(Node::getPosition)
				.collect(Collectors.toList());
		testGraph.resize(0.5);
		List<Point2D> shrunkenSizes = Stream.concat(testGraph.getNodes().stream(), testGraph.getSubgraphs().values().stream()).map(Node::getPosition)
				.collect(Collectors.toList());
		for (int i = 0; i < initialSizes.size(); i++) {
			Point2D initialSize = initialSizes.get(i);
			Point2D shrunkenSize = shrunkenSizes.get(i);
			assertEquals(initialSize.getX() * 0.5, shrunkenSize.getX(), 0.0000001);
			assertEquals(initialSize.getY() * 0.5, shrunkenSize.getY(), 0.0000001);
		}
		testGraph.resize(2);
		List<Point2D> enlargedSizes = Stream.concat(testGraph.getNodes().stream(), testGraph.getSubgraphs().values().stream()).map(Node::getPosition)
				.collect(Collectors.toList());
		for (int i = 0; i < initialSizes.size(); i++) {
			Point2D initialSize = initialSizes.get(i);
			Point2D enlargedSize = enlargedSizes.get(i);
			assertEquals(initialSize.getX(), enlargedSize.getX(), 0.0000001);
			assertEquals(initialSize.getY(), enlargedSize.getY(), 0.0000001);
		}
	}

	/**
	 * Tests that hasPositionalNodes method return false for no positional nodes
	 */
	@Test
	public void hasPositionalNodes_False() {
		thirdNode.setPreferredLocation(Location.NONE);
		assertFalse(testGraph.hasPositionalNodes());
	}

	/**
	 * Tests that hasPositionalNodes method return true for positional node
	 */
	@Test
	public void hasPositionalNodes_True() {
		thirdNode.setPreferredLocation(Location.RIGHT);
		assertTrue(testGraph.hasPositionalNodes());
	}
}
